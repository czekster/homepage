<?php
include_once('session_valid.php');
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="generator" content="GitLab Pages">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Ricardo M. Czekster's homepage</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="syntax.css">
	<link rel="stylesheet" href="main.css">
   <link rel="stylesheet" href="style.css">
<style>
.button {
  background-color: #04AA6D;
  border: none;
  color: white;
  padding: 15px 32px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
  margin: 4px 2px;
  cursor: pointer;
}
</style>
  </head>

<body class="vsc-initialized">
  <div class="wrapper">
	<div class="columns">
<?php
include("sidebar.php");
?>
	  <div class="main">
<h1>FYP - Projects listing</h1>

<?php
   if (isset($_SESSION['user']) && $_SESSION['user'] != "nok") {
?>
<!-- main block-->
<div align="right">
<form method="POST" action="logout.php">
 <button class="button">Logout</button>
</form>
</div>

<p>Here are some ideas for FYP projects. Use it as something to give you insights to <b>your own idea</b>.
</p>

<p>Feel free to contact me after you know (more or less) what you would like to pursuit.
</p>

<p>All projects here involve <b>building a tool</b> (yes, implementing a useful tool) and follow the phases of Software Development Life-Cycle (SDLC).
</p>

<hr width="100%" align="left" color="#91A3AB">

<a name="top"></a>
<h2>Projects</h2>
&bull;&nbsp;<a href="#RC01"><b>RC01:</b> Declarative OO Modelling</a><br/>
&bull;&nbsp;<a href="#RC02"><b>RC02:</b> Tool to automate Threat Modelling</a><br/>
&bull;&nbsp;<a href="#RC03"><b>RC03:</b> Tool to generate Domain Driven Design artefacts</a><br/>
&bull;&nbsp;<a href="#RC04"><b>RC04:</b> Tool to create Attack/Threat Trees from files</a><br/>
&bull;&nbsp;<a href="#RC05"><b>RC05:</b> Tool to create visual front-ends to CLI</a><br/>
&bull;&nbsp;<a href="#RC06"><b>RC06:</b> Tool to map level of coupling/cohesion in FOSS</a><br/>
&bull;&nbsp;<a href="#RC07"><b>RC07:</b> Tool to tag methods to automate JUnit tests</a><br/>
&bull;&nbsp;<a href="#RC08"><b>RC08:</b> Tool to automatically hash on-line texts</a><br/>
&bull;&nbsp;<a href="#RC09"><b>RC09:</b> Tool to map in JSON for graph analysis</a><br/>
&bull;&nbsp;<a href="#RC10"><b>RC10:</b> Tool to execute scientific computing using WebAssembly</a><br/>
&bull;&nbsp;<a href="#RC11"><b>RC11:</b> Tool to map the software supply chain of features</a><br/>
&bull;&nbsp;<a href="#RC12"><b>RC12:</b> Tool to understand static analysis in Spring Boot</a><br/>
&bull;&nbsp;<a href="#RC13"><b>RC13:</b> Prediction of Football Outcomes</a><br/>
&bull;&nbsp;<a href="#RC14"><b>RC14:</b> Tool to enact Threat Modelling principles</a><br/>
&bull;&nbsp;<a href="#RC15"><b>RC15:</b> Tool for adding static analysis frameworks for security</a><br/>
&bull;&nbsp;<a href="#RC16"><b>RC16:</b> Tool to visualise data races using Java Pathfinder (JPF)</a><br/>
&bull;&nbsp;<a href="#RC17"><b>RC17:</b> Tool to generate scripts for OWASP's Zed Attack Proxy</a><br/>
&bull;&nbsp;<a href="#RC18"><b>RC18:</b> Tool for visualising Software Supply Chains</a><br/>
&bull;&nbsp;<a href="#RC19"><b>RC19:</b> Tool for ensuring software features are securely deployed</a><br/>
&bull;&nbsp;<a href="#RC20"><b>RC20:</b> Tool for creating a "Secure Auto-Pilot" and code suggestions</a><br/>
&bull;&nbsp;<a href="#RC21"><b>RC21:</b> Tool for selecting key cyber security related data in datasets</a><br/>
&bull;&nbsp;<a href="#RC22"><b>RC22:</b> Tool called "extraCTIng" for retrieving CTI from Reddit & PRAW</a><br/>
&bull;&nbsp;<a href="#RC23"><b>RC23:</b> Tool for querying and alerting on the CVE database</a><br/>

<br><br>
<hr width="100%" align="left" color="#91A3AB">

<!-- begin of projects -->

<!--  ###### -->

<a name="RC01"></a>
<p><b>RC01: Tool for declarative OO modelling when building on-line systems</b></p>

<p>The idea is to take a structured textual definition (you define) and generate some Object Oriented (OO) code (you can choose the programming language).</p>

<p>This model could employ for instance Markdown for the required constructs, ie:</p>
<pre style="overflow-x:hidden;">
A [*user*] has [*one-to-one*] [unique, id] 
A [*user*] has [*one-to-one*] [string, name]
A [*user*] has [*one-to-one*] [encrypted, password]
A [*client*] has [*one-to-one*] [*user*]
A [*store*] has [*one-to-many*] [*client*]
...
</pre>

<p>Modelling decisions: for instance, a [*user] could be final class.</p>

<p>Example: automatically generate code for the Spring Boot framework.</p>

<a href="#top">top</a><br/>

<br/>
<hr width="100%" align="left" color="#91A3AB">

<!--  ###### -->

<a name="RC02"></a>
<p><b>RC02: Tool to automate Threat Modelling using Data Flow Diagrams (DFD) and OWASP pytm</b></p>

<p>OWASP pytm is a tool that creates a DFD model from textual specifications. More info on <a href="https://owasp.org/www-project-pytm/" target="_blank">its website</a>&#x2197;.</p>

<p>The idea of this project will be to create a tool that parses code and generates the format required by pytm so it outputs its models. 
</p>

<a href="#top">top</a><br/>

<br/>
<hr width="100%" align="left" color="#91A3AB">

<!--  ###### -->

<a name="RC03"></a>
<p><b>RC03 - Tool to generate Domain Driven Design artefacts automatically</b></p>

<p>The use of Domain Driven Design (DDD) permeates software development. In this project you will create a textual specification (that you design) comprising a software project (classes, etc) and it will generate the Domain Primitives, Entities, Value Objects (POJO/Bean, etc).</p>

<p>It aims to help developers to create more secure software by using higher-level constructs.</p>

<a href="#top">top</a><br/>

<br/>
<hr width="100%" align="left" color="#91A3AB">

<!--  ###### -->

<a name="RC04"></a>
<p><b>RC04 - Tool to create visual Attack/Threat Trees from textual files</b></p>

<p>In this project you will develop software that takes security models written as textual Attack/Threat Trees and create a visual representation (eg, a Java canvas) that helps analysis and better risk communication.
</p>

<a href="#top">top</a><br/>

<br/>
<hr width="100%" align="left" color="#91A3AB">

<!--  ###### -->

<a name="RC05"></a>
<p><b>RC05 - Tool to create visual front-ends to command-line interfaces</b></p>

<p>The idea is to take a tool that has a command-line interface (CLI) such as NMAP and maps all required parameters and types (using the manual that is provided) to create a visual front-end counterpart.
</p>

<a href="#top">top</a><br/>

<br/>
<hr width="100%" align="left" color="#91A3AB">

<!--  ###### -->

<a name="RC06"></a>
<p><b>RC06 - Tool to map level of coupling/cohesion in FOSS projects</b></p>

<p>This project will take Free and Open Source Software (OSS) projects as secondary datasets and analyse them for measuring the level of coupling and cohesion to guide better designs.
</p>

<a href="#top">top</a><br/>

<br/>
<hr width="100%" align="left" color="#91A3AB">

<!--  ###### -->

<a name="RC07"></a>
<p><b>RC07 - Tool to tag methods to automate JUnit tests</b></p>

<p>In this project, you will code a solution that takes a software project as input (OO based with classes, attributes, and methods) and you will create a tagging system on all methods that will support the generation of automated JUnit tests.
</p>

<a href="#top">top</a><br/>

<br/>
<hr width="100%" align="left" color="#91A3AB">

<!--  ###### -->

<a name="RC08"></a>
<p><b>RC08 -  Tool to automatically hash on-line texts for improved search</b></p>

<p>The idea is to take any on-line text (eg, a book) and hash it (varying string length) to support improved (perhaps faster) searches.
</p>

<a href="#top">top</a><br/>

<br/>
<hr width="100%" align="left" color="#91A3AB">

<!--  ###### -->

<a name="RC09"></a>
<p><b>RC09 - Tool to map distributed assets written in JSON for multi-path graph analysis</b></p>

<p>As input this software will create a textual representation of assets and then it will convert to a graph amenable for multi-path analysis, ie, the myriad possibilities for abusing the underlying systems.
</p>

<a href="#top">top</a><br/>

<br/>
<hr width="100%" align="left" color="#91A3AB">

<!--  ###### -->

<a name="RC10"></a>
<p><b>RC10 - Tool to execute scientific computing using WebAssembly</b></p>

<p>This project will choose a scientific computing application and run in a compatible browser using <a href="https://webassembly.org" target="_blank">WebAssembly</a>&#x2197;.
</p>

<a href="#top">top</a><br/>

<br/>
<hr width="100%" align="left" color="#91A3AB">

<!--  ###### -->

<a name="RC11"></a>
<p><b>RC11 - Tool to map the software supply chain of multiple deployed features</b></p>

<p>In this project you will implement a tool that traverses the required APIs for the set of deployed (in production) features of a project and you will compute the effort to patch/update vulnerabilities per feature.
</p>

<a href="#top">top</a><br/>

<br/>
<hr width="100%" align="left" color="#91A3AB">

<!--  ###### -->

<a name="RC12"></a>
<p><b>RC12 -  Tool to understand static analysis of Spring Boot projects</b></p>

<p>In this project you will run tools for static analysis and subject it to Spring Boot projects to investigate how developers could improve their skills whilst implementing secure software.
</p>

<a href="#top">top</a><br/>

<br/>
<hr width="100%" align="left" color="#91A3AB">

<!--  ###### -->

<a name="RC13"></a>
<p><b>RC13 - Prediction of Football Outcomes</b></p>

<p>This project will employ the BCAS dataset (link: https://www.kaggle.com/datasets/rczekster/matches-brazilian-football-from-2003-to-2019) to predict next champion early in the season (starting at the middle, ie, the 19th round out of 38). This will be an exploratory exercise to compare among different techniques (eg, IA/ML, Markov Chains, simulation, etc.).
</p>

<a href="#top">top</a><br/>

<br/>
<hr width="100%" align="left" color="#91A3AB">

<!--  ###### -->

<a name="RC14"></a>
<p><b>RC14 - Tool to enact Threat Modellling principles through tool translation</b></p>

<p>It will build on the notion of <i>"From high-level descriptions of systems to threat models"</i>. The idea is to take a <a href="https://github.com/mingrammer/diagrams" target="_blank">Diagrams</a>&#x2197; model and convert to its <a href="https://github.com/izar/pytm" target="_blank">pytm</a>&#x2197; counterpart (both written in Python) so end-users may add threat modelling to their security analysis.
</p>

<a href="#top">top</a><br/>

<br/>
<hr width="100%" align="left" color="#91A3AB">

<!--  ###### -->

<a name="RC15"></a>
<p><b>RC15 - Tool for adding static analysis frameworks into projects and processing outputs to find security vulnerabilities</b></p>

<p>This project will take as input a Java project and will do two things: 1. incorporate static analysis frameworks in the project; and 2. analyse generated output to point out security vulnerabilities. Relevant research: <a href="https://akjournals.com/view/journals/606/17/2/article-p1.xml" target="_blank">Click here</a>&#x2197;
</p>

<a href="#top">top</a><br/>

<br/>
<hr width="100%" align="left" color="#91A3AB">

<!--  ###### -->

<a name="RC16"></a>
<p><b>RC16 - Tool to visualise data races in Java-based distributed computing using Java Pathfinder (JPF)</b></p>

<p>This project will take JPF's output and will generate a visual counterpart to understand data races in distributed applications. <a href="https://github.com/javapathfinder/jpf-core" target="_blank">Link to tool</a>&#x2197;.
</p>

<a href="#top">top</a><br/>

<br/>
<hr width="100%" align="left" color="#91A3AB">

<!--  ###### -->

<a name="RC17"></a>
<p><b>RC17 - Tool to generate scripts for OWASP's Zed Attack Proxy (ZAP) tool</b></p>

<p>This project will take a target website (provided) and will automatically generate scripts to simulate attacks into the platform. <a href="https://www.zaproxy.org" target="_blank">More information here</a>&#x2197;.
</p>

<a href="#top">top</a><br/>

<br/>
<hr width="100%" align="left" color="#91A3AB">

<!--  ###### -->

<a name="RC18"></a>
<p><b>RC18 - Tool for visualising the Software Supply Chain of Open Source Software to decide feature toggling</b></p>

<p>This project will take a given Open Source Software (OSS) project and traverse the features to understand all the API dependencies (and versions) to understand the Software Supply Chain and make timely decisions on feature toggling.
</p>

<a href="#top">top</a><br/>

<br/>
<hr width="100%" align="left" color="#91A3AB">

<!--  ###### -->

<a name="RC19"></a>
<p><b>RC19 - Tool for ensuring software features are securely deployed</b></p>

<p>The idea is to traverse the project's source code in search for i) comments that leak information; ii) secrets written statically in code; iii) error messages with sensitive information. Then, it shows a report to the end-user with security-based recommendations on what to do before deploying in production.
</p>

<a href="#top">top</a><br/>

<br/>
<hr width="100%" align="left" color="#91A3AB">

<!--  ###### -->

<a name="RC20"></a>
<p><b>RC20 - Tool for creating a "Secure Auto-Pilot" and devising code suggestions adhering to security requirements</b></p>

<p>The idea is to help programmers insert or review code in projects by suggesting software that are secure by design.
</p>

<a href="#top">top</a><br/>

<br/>
<hr width="100%" align="left" color="#91A3AB">

<!--  ###### -->

<a name="RC21"></a>
<p><b>RC21 - Tool for selecting key cyber security related data in datasets</b></p>

<p>The idea is to employ a cyber security related dataset, for instance, the <a href="https://www.kaggle.com/datasets/katehighnam/beth-dataset" target="_blank">BETH dataset</a>&#x2197;, and develop a tool (Python script) to filter out 'noise' that tries to reduce the dataset with most key data elements to perform timely analysis afterwards. The method shall be generic enough to be applied to similar cyber security datasets.
</p>

<a href="#top">top</a><br/>

<br/>
<hr width="100%" align="left" color="#91A3AB">

<!--  ###### -->

<a name="RC22"></a>
<p><b>RC22 - Tool called "extraCTIng" for retrieving threat intelligence from Reddit using PRAW</b></p>

<p>The idea is to use <a href="https://github.com/praw-dev/praw" target="_blank">PRAW: The Python Reddit API Wrapper</a>&#x2197; to extract actionable intelligence from the Reddit platform and generate STIX models for sharing.
</p>

<a href="#top">top</a><br/>

<br/>
<hr width="100%" align="left" color="#91A3AB">

<!--  ###### -->

<a name="RC23"></a>
<p><b>RC23 - Tool for querying and alerting on the Common Vulnerabilities and Exposures (CVE) database</b></p>

<p>You will implement a tool that takes as input a list of search strings (eg, "apache", "linux", "php", and so on), and queries the <a href="https://cve.mitre.org/cve/search_cve_list.html" target="_blank">CVE repository</a>&#x2197;, takes the output, process it, and compare all attack vectors, listing them to users. It can be visual (web based) or command-line (a script). Before you embark on this project, search for those strings at that website, see what it returns as result, see the attack vector, and think about ways of "studying" those attack vectors altogether. For this project, you will need to study about scripting, if you want to create a web page, about dynamic web page creation (using php, or even putting things in a database, using mysql), and about the internals of CVEs, what they represent, CVSS, attack vectors, and so on. (mind that I will mention this later on at the CS3SP module).
</p>

<a href="#top">top</a><br/>

<br/>
<hr width="100%" align="left" color="#91A3AB">

<!--  ###### -->

<!-- new  one!
<a name="RC0X"></a>
<p><b>RC0X</b></p>

<p>
</p>

<a href="#top">top</a><br/>

<br/>
<hr width="100%" align="left" color="#91A3AB">
-->

<br><br><br><br><br><br><br><br><br><br><br><br>
<br><br><br><br><br><br><br>

<!--  ###### -->

<!-- end main block-->
<?php
   }
?>




<!-- end of projects -->


<?php
include("postfooter.php");
?>

            </div> <!-- div main -->

          </div> <!-- div columns -->

        </div> <!-- div wrapper -->

<?php
include("footer.php");
?>

</div>
</body>
</html>