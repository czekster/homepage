<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="generator" content="GitLab Pages">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Ricardo M. Czekster's homepage</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="syntax.css">
	<link rel="stylesheet" href="main.css">
    <link rel="stylesheet" href="style.css">
  </head>

<body class="vsc-initialized">
  <div class="wrapper">
	<div class="columns">
<?php
include("sidebar.php");
?>
	  <div class="main">
		<h1>Rants & Musings</h1>
<p>These are my personal views, not related in any way to my employer's. Take it with a <i>kilo of salt</i> before judging anything that has been written here.</p>

<p>I present you next some of my favorite <i>rants</i> & <i>musings</i> for everyone's delight (I hope...).</p>

<a name="top"></a>
<b>Index</b> <font size="-1" face="Courier">(datetime format: <b>[DD/MM/YYYY]</b>)</font><br/>
<ol reversed>
 <li><a href="#thoughts-programming">General thoughts on programming</a>, 23/11/2024</li>
 <li><a href="#spinningwheel">The perpetual spinning wheel of modern software development</a>, 03/10/2023</li>
 <li><a href="#ecree">Claims & Evidence</a>, 11/06/2023</li>
 <li><a href="#dependability-taxonomy">On dependability taxonomy</a>, 09/04/2023</li>
 <li><a href="#difficulties-cybersec">Difficulties posed by cyber security</a>, 03/04/2022</li>
 <li><a href="#fearmongering">Fearmongering</a>, 03/11/2021</li>
 <li><a href="#science-as-the-answer">Science is the most likely answer</a>, 22/06/2020</li>
 <li><a href="#clever-name">Clever title</a>, 01/01/1970</li>
</ol>


<br>
<hr size="1" align="left" height="100" color="#aabbcc">

<!-- ########################################### -->
<a name="thoughts-programming"></a>
<h2>"General thoughts on programming"</h2>
<p class="first-word-uppercase-p">Today I noticed that I have been programming since 1996 and since then, I have been thinking about how to be a better programmer. Over these 28 years I have had some up and downs, some happy and <font color="#555544">(<i>leider</i>)</font> a lot of sad moments. I cannot say I have chosen my career poorly, as I do enjoy programming a lot.
</p>

<p>And every day I still think about that.
</p>

<hr color="#00aa11" width="10%" size="1" align="left">

<p>I started programming in PASCAL, then C, then Java, C++, Perl, and recently (as of 2022), Python.
</p>

<p>I touched BASIC (Beginner's All-Purpose Symbolic Instruction Code), invented by John Kemeny. It is indeed a very odd "language", for example, to print some Fibonacci numbers one would "code" the following:
</p>
<pre style="overflow-x:hidden;">
05 HOME : TEXT : REM Fibonacci numbers 
10 LET MAX = 5000
20 LET X = 1 : LET Y = 1
30 IF (X > MAX) GOTO 100
40 PRINT X
50 X = X + Y
60 IF (Y > MAX) GOTO 100
70 PRINT Y
80 Y = X + Y
90 GOTO 30
100 END
</pre>
Source: <a href="https://www.hoist-point.com/applesoft_basic_tutorial.htm" target="_blank">tutorial on BASIC</a>.<br>

<p>Remember that quote from Edsger Wybe Dijkstra: <i><b>"GOTO Statement Considered Harmful"</b></i>? It stems from that code ;).</p>

<p>To add instructions between 10 and 20 one would create a new line and add a "15" with the new instruction. Can you see the host of issues that this "design" causes? <b>Thank you, programming leaders of the world, for creating more useful programming languages over the years!!</b></p>

<table border="0" cellpadding="10" width="100%">
 <tr><td colspan="2" align="left"><b><font size="-1">Quote:</font></td></tr>
 <tr>
  <td width="5%" bgcolor="#AABBCC">&nbsp;</td>
  <td width="90%" style = "border: 2px solid gray;" bgcolor="#EEEEEE"><p>
"(in programming) there are basically three main (or meta) principles above all: KISS, DRY and No-Principle-is-Sacred."<br>
- Internet random anonymous user
</p>
</td>
  <td width="5%" bgcolor="#AABBCC">&nbsp;</td>
 </tr>
</table>
<p align="right"><font size="-1">KISS=Keep It Simple, Stupid<br>DRY=Don't Repeat Yourself<br></font></p>

<p>I remember (vividly) coding in <a href="https://sourceforge.net/projects/turbopascal-wdb/" target="_blank">Turbo Pascal</a>, then in Turbo C. What a joyful experience. Debugging. Understanding how things work.</p>

<hr color="#00aa11" width="10%" size="1" align="left">

<p>Nowadays, programmming is a BEAST of its own: a plethora of libraries to attach to projects, several layers upon layers of things to worry, infrastructure issues (DevOps), embedding security early on, etc., etc. I wonder if as a field we will stabilise eventually into "something" that is prone from defects from the get-go, whatever language one uses to solve a problem. Only the future will tell, I guess.</p>

<p align="right">Birmingham, 23/11/2024</p>
<p align="left"><font color="dddddd"><a href="#top">top</a></font></p>

<br>
<hr size="1" align="left" height="100" color="#aabbcc">
<!-- #### -->

<!-- ########################################### -->
<a name="spinningwheel"></a>
<h2>"The perpetual spinning wheel of modern software development"</h2>
<p class="first-word-uppercase-p">A recurring problem surfacing in today's society given its high reliance on software for performing a myriad of tasks is due to persistent defects finding its way into production environments.
</p>

<p>
Modern deployment practices forcibly push whole systems or sets of features into production with insufficient (sometimes minimal) observance to its non-functional properties, namely, performance, usability, or cyber security (<font size="-1" color="#0aaa55">Note: one may exhaustively debate whether naming this 'non-functional' is useful or totally useless; for the sake of this post, this is how the field calls these practices, the discussion can be reserved to other time. See <a href="https://www.youtube.com/watch?v=4aHKsolzCv4" target="_blank">"Non-Functional Requirements Are STUPID"</a>&#x2197; on YouTube by Farley</font>).
</p>

<p>
On top of that, software usually depends on a significant amount of third-party API that are also amenable to failures, that were poorly tested, or that are shipped with (sometimes unwarranted/hidden) vulnerabilities.
</p>

<p>
These unwanted situations are complex to map and address with testing, as it involves a non-trivial amount  of resources to complete in time.
</p>

<p>
Different software quality assurance tools are employed by organisations to prevent weaknesses reaching end-users and customers.
</p>

<p>
It is a fact that software development has always been difficult, permeated with hard-to-solve all-encompassing issues like heterogeneous teams skills levels, problems understanding requirements or persistent defects plaguing systems altogether, to mention a few concerns.
</p>

<p>
In terms of process, the sad truth is that however you call it (waterfall, agile, etc.), it was difficult to address and effectively tackle those impending issues.
One difference though is that now, the <i><b>cycles are way shorter</b></i>. Let me explain. It is usual to go through all SDLC phases in a one or two weeks sprint. 
</p>

<p>
In the early days one could see the sprint as being organised as a 6-months to 1-year long endeavour and, by the end, we would have a product with many features.
</p>

<hr color="#00aa11" width="10%" size="1" align="left">

<p>
We decompose large systems into smaller <i>chunks</i> that one might call a <b>feature</b> and we think about all these individual features (trying to) operating together, orchestraded.
And we shortened the sprint to one to two weeks (or days or even hours, in more performant teams, perhaps?) and we go through all the phases to ensure each feature is OK for execution whilst in production.
</p>

<p>
It's like we are changing the radius of the spinning wheel to move faster (more deliverables, however, it sacrifises overall software quality).
</p>

<p>
Let's suppose we're interested in securing the feature, so the next analysis is biased towards this attribute.
</p>

<p>
The steps in modern software development/delivery/deployment or DevOps (<a href="https://www.jedi.be/blog/2022/02/11/shades-of-devops-roles/" target="_blank">in all its shades</a>&#x2197;) and assuming a sizeable Java-based project:
<ol>
 <li>Client has come up with (yet another) bright idea and he/she wants to become a feature in the system at hand. The teams try to understant what it is needed and start drafting some ideas and high-level requirements.
 <ul>
  <li>Some back and forth between stakeholders adjust things here and there and all is ready to 'move on' forward</li>
 </ul>
 <li>Write code; integrate; test (a lot, across the board) feature, automated tests (white box, removing naive mistakes and perhaps more serious issues)
 <ul>
  <li>Note: this is highly dependent on developer skills to ensure the code is according to specification/requirements and follow recommendations and best practices</li>
 </ul>
 <li>Then basic syntax check for dumb mistakes (eg, CheckStyle, finding dead methods, etc., this kind of stuff), basic SAST (sifting through the overwhelming number of false positives), using SpotBugs, and then some SCA (third party API and libraries analysis given software is more complex nowadays given the multitude of external things to be linked)</li>
 <ul>
  <li>You can start working on some threat models and abuse cases for your feature (or whole application) at this point, depending on your level of experience and familiarity with methodologies; some basic risk assessment might go together</li>
 </ul>
 <li>Then code review, perhaps commit to (main?) repository/branch/whatever (single? multiple?)</li>
 <li>Then more testing, quality assurance testing (secure testing/pentesting, performance/load testing, usability testing), another round of SAST+SCA, then DAST+IAST+WAF (if you can, or have the resources)</li>
 <li>Finally, you might have something releasable (a deliverable?), then deployment, canary release, rollback if needed, rinse and repeat</li>
</ol>
</p>

<p>
Feedback (and its frequency) have changed. Teams have changed. Technologies have changed.
Sadly, <i><b>humans have not changed</b></i> (nor they will), and as the time of writing this post, AI has not replaced humans out of the mix (yet).
</p>

<p>
The field is ripe with fast-paced time-to-market demands meeting ever stricter deadlines.
Short cuts. 'Clever' (to whom?) work arounds, bypassing (usually security) controls. Scripting. JavaScript (is this still a 'thing' in 2023?). Quick and dirty alternatives (those things that E. W. Dijkstra would not vouch over one's shoulder [1]).
</p>

<p>
If teams are conscious and independent/responsible, the fact that adding some security champions in the mix might improve quality.
They could consider adhering to security standards and best practices, useful/maintainable design patterns, secure by design approaches, and accounting/balancing security, performance over usability.
However, let's not bury our heads in the sand because in reality, across organisations, the ratio between soft. devs. to appsec professionals is 100 to 1.
</p>

<p>
I'm sorry, but I don't have an easy answer to these problems addressed here, because it takes time, organisational culture change, and budgetary decisions that make managers twitch in their chairs in discomfort every time they are mentioned.
</p>

<br>
<hr color="#008811" width="25%" size="1" align="left">
[1] Quote: <i>"I mean, if 10 years from now, when you are doing something quick and dirty, you suddenly visualize that I am looking over your shoulders and say to yourself 'Dijkstra would not have liked this', well, that would be enough immortality for me."</i>, by Edsger W. Dijkstra
<br><br>

<p align="right">Birmingham, 03/10/2023</p>
<p align="left"><font color="dddddd"><a href="#top">top</a></font></p>

<br>
<hr size="1" align="left" height="100" color="#aabbcc">
<!-- #### -->


<!-- ########################################### -->
<a name="ecree"></a>
<h2>"Claims & Evidence"</h2>
<p class="first-word-uppercase-p">The <a href="https://en.wikipedia.org/wiki/Sagan_standard" target="_blank"><b><i>Sagan Standard</i></b></a>&#x2197; state that <i><b>"extraordinary claims require extraordinary evidence"</b></i> (usually mentioned as ECREE). It was not originally created by him as other prominent scientific minds such as <a href="https://en.wikipedia.org/wiki/Pierre-Simon_Laplace" target="_blank">Laplace</a>&#x2197; employed earlier (1814).</p>

<p>
ECREE is related to <a href="https://en.wikipedia.org/wiki/Occam%27s_razor" target="_blank">Occam's Razor</a>&#x2197; in the sense that according to such a heuristic, <b><i>simpler explanations are preferred to more complicated ones</i></b>. Was that a UFO or just an oddly shaped flying <i>thingy</i>? You tell me.
</p>

<p>
It's good to keep an open mind about things, however, to convince ME (aamof, anyone) of your extraordinary (oftentimes <i>absurd</i> and <i>highly improbable</i>) claim, please come with scientifically sound experiments and evidences to substantiate those ideas altogether, otherwise, it'll be dismissed as pure <i>speculation</i> or <i>anecdote</i> (ie, accounts regarded as unreliable or hearsay).
</p>

<p>Nowadays, we are plagued with "flat Earthers", "homeopathy", "zen cures", "astrology" (this one is old though) and "reptilian lizard people", but let's stop now. I may be mistaken, but I've never seen any <i>hard evidence</i> substantiating <i>any</i> of these claims. Science, on the other hand, has provided many answers to our deep questions throughout our very own existence. 
</p>

<p>Claims backed up by science are simply 'out there', in the open, to be put to tests. If they fail, they are discarded and we as scientists start all over again with different assumptions to prove or refute any idea. And that's how I like my facts: scientifically sound, amenable to proof, and substantiated by other scientists.
</p>

<p align="right">Birmingham, 11/06/2023</p>
<p align="left"><font color="dddddd"><a href="#top">top</a></font></p>

<br>
<hr size="1" align="left" height="100" color="#aabbcc">

<!-- ########################################### -->
<a name="dependability-taxonomy"></a>
<h2>"On dependability taxonomy"</h2>
<p class="first-word-uppercase-p">I don't wish to start finger pointing anything, however, it frustrates me the overspread use of dependability concepts interchangeably, without any <i>thinking</i>.</p>

<p>It is not my intention to mandate the proper use of concepts, however, a basic understanding would suffice for explaining to broader audiences about overall <i>system's quality</i>.</p>

<p>An example is <i>reliability</i>. Because the word "reliable" conjures the sense that something is trustworthy or dependable (now it's even worst!), people use it interchangeably with <i>availability</i>.
</p>

<p>In (formal) dependability, one can think of those desired system properties as one word each (you shall find more than one definition for these properties):
<ul>
 <li>Reliability: <b>continuity</b></li>
 <li>Availability: <b>readiness</b></li>
</ul>
</p>

<table border="0" cellpadding="10">
 <tr>
  <td width="1%" bgcolor="#eeffee">&nbsp;</td>
  <td style = "border: 1px solid green;" bgcolor="#eeeeff">
<i>"A fundamental difference between reliability and availability is that reliability refers to failure-free operation during an interval, while availability refers to failure-free operation at a given instant of time, usually the time when a device or system is accessed to provide a required function or service. Reliability is a measure that characterizes the failure process of an item, while availability combines the failure process with the restoration or repair process and looks at the probability that at a given time instant the item is operational independently of the number of failure/repair cycles already undergone by the item."</i>
<br/>
Trivedi, K. S., & Bobbio, A. (2017). <b><i>Reliability and availability engineering: modeling, analysis, and applications.</i></b> Cambridge University Press.
</td>
 </tr>
</table>

<p><b>Example with time</b>: in this case, the reliability level is usually computed in terms of the amount of time it didn't work throughout a duration, eg, a year. We have <b>525,600</b> minutes in one year. Look at this list next:</p>
<ul>
 <li>Not working 10 seconds per year: reliability is 99.999968% (6 nine's)</li>
 <li>Not working 60 seconds per year (1 min): reliability is 99.999810% (5 nine's)</li>
 <li>Not working 3600 seconds per year (1 hour): reliability is 99.988584% (3 nine's)</li>
</ul> 

<p>Vendors try to push reliability to customers, stating that they offer high-reliability. I guess they mean 6 nine's by that, ie, the system works almost all the time per year, admitting failures that take up to 10 seconds to resume to operational status.</p>

<p>Now think: can you say that your system provides reliability? If yes, how much? 6 nine's? 3 nine's? Are you really off-line for just one hour per year? Can you support this claim?</p>

<p><b>Example with other elements</b>: you could employ the same reasoning for any other element, eg, number of yearly flights with no incidents, number of odd-shaped nails in a batch, and so on.</b>

<hr color="#ee8811" width="10%" size="1" align="left">

<p>Availability and reliability both tackle <b>failure-free operation</b> of systems; the former on a <i>given instant of time</i> and the latter during an <i>interval</i>. Let's think about the system and the states it can assume: <font face="Courier">[Working, Repairing, Failure]</font>. The <b><i>Availability</i></b> (A) is:</p>

<ul>
 <li>A = MTTF / (MTTF + MTTR)</li>
 <ul>
  <li>where</li>
  <ul>
   <li>MTTF = Mean Time To Failure</li>
   <li>MTTR = Mean Time To Repair</li>
  </ul>
 </ul>
</ul>

<p>Thus, availability is about computing the level of working time a system has. How to do this? You take a bunch of system's logs with time data and failure events (analogously the amount of time it remained working).</p>

<p>Then, proceed discovering where and when any failure events (transitions to the <font face="Courier">Failure</font> state) happened, mark the current time (that it worked, eg, MTTF) and the time it took to come back to <font face="Courier">Working</font> state (eg, you will be computing the amount of time to repair the system - MTBF).</p>

<p>When you do this process for <b><i>all your failure events</i></b> present in that log batch, you can compute your overall system's availability and tell your boss how you could work to improve it.</p>

<p>Not surprisingly, if we plug to the equation one year operation (<b>525,600</b> minutes in one year) as MTTF (one failure per year), and it took 10 seconds (1/6 of one minute) to repair (MTTR), we will come up with the same value as before (A=99.999968%). What we usually have is not a single failure, but multiple failures/repairs pairs throughout one year. That's the reasoning on why it is more realist to compute availability, without loss of generality.</p>

<p>My point here is: whenever people refers to <i>reliability</i> perhaps what they <b><i>really</i></b> mean is <i>availability</i>. But again, this is just another rant.

<p align="right">Birmingham, 09/04/2023</p>
<p align="left"><font color="dddddd"><a href="#top">top</a></font></p>


<br>
<hr size="1" align="left" height="100" color="#aabbcc">


<!-- ########################################### -->
<a name="difficulties-cybersec"></a>
<h2>"Difficulties posed by cyber security"</h2>
<p class="first-word-uppercase-p">Reading more about the difficulties behind (almost any) cyber security effort, in this paper: <i>"Seeking Foundations for the Science of Cyber Security"</i> [1].</p>

<p>This is not at all surprising because cyber security turns out to be an extremely difficult problem, owing to many factors, such as:</p>
<ul type="I">
 <li>The <b>scale of the cyberspace</b>, where (potentially) billions of devices are interconnected and must be considered holistically;</li>
 <ul>
  <li>massive (in breadth/length) attack surfaces to observe</li>
  <li>on the TTP-based threat hunting white paper by MITRE [2] they discuss the <i>Terrain</i>: a triad with Time x Technology x TTPs</li>
 </ul>
 <li>The <b>complexity of cyberspace</b> in terms of the interdependence and interactions among its cyber-physical-human components;</li>
 <ul>
  <li>complex interconnections among elements</li>
 </ul>
 <li>The <b>adversarial intelligence</b> that attackers possess as skilled human beings which is on par with defenders;</li>
 <ul>
  <li>dynamic strategies to consider <i>next attacks</i> and matching <i>mitigations</i></li>
 </ul>
 <li>The <b>asymmetry</b> between attackers who only need to identify and exploit a single vulnerability, and defenders who must seek to prevent or block each and every vulnerability;</li>
 <ul>
  <li>imbalanced <i>attack-defense game</i>, where attackers have sensible advantages over defenders</li>
  <li>automatic tracking, surveying, monitoring networks and systems for vulnerabilities (scanning ports, sniffing traversing packets, potential OS discovery through observed patterns, and so on)</li>
 </ul>
 <li>The <b>difficulty in quantifying cyber security</b> from a whole-system (rather than building-blocks) perspective;</li>
 <ul>
  <li>monitoring, identifying, confirming, etc. <b><i>notable cyber security events</i></b> are not a trivial task to engage</li>
  <li>incorporating dependability models is thus not trivial, also when modelling the effects of cyber-attacks across systems is difficult to map and to identify relationships</li>
 </ul>
 <li>The <b>multidisciplinary nature of cyber security</b>, technologically speaking, and especially the involvement of human beings as users, attackers, and defenders;</li>
 <ul>
  <li>the overall cyber security effort and posture involves a high number of stakeholders</li>
  <li><i>classic dependability</i> deals with systems failures or defects inadvertently introduced by programmers or people working on top of the infrastructure whereas cyber security tackles issues arising in deliberately issues such as cyber-attacks or overwhelming the infrastructure to stretch resources to the limit to gain access and control and then installing malware or other footholds for later exploration</li>
  <li>so called <i>repair models</i> employed to tackle cyber security issues underpinning massive attack surfaces could be modelled as systems behaving in simple states, namely operating or failure</li>
 </ul>
</ul>

<hr color="#ee8811" width="10%" size="1" align="left">

<p>Let's not forget that current cyber-attacks do not necessarily follow a straightforward CKC set of <i>ordered</i> (or organised) actions/tasks; threat actors would try different Tactics, Techniques & Procedures (TTP) over time.</p>

<p>The observed time to compromise elements varies, as new footholds are established and then exploited, to circumvent detection or other protective measures.</p>

<p>In terms of quantifying cyber security, and modelling the <i>consequences of attacks</i> [3], one could ponder that understanding and keeping the infrastructure updated are hard tasks to achieve.</p>

<p>On top of that, systems are organised in multiple layers, there is a fundamental lack of institutional knowledge being passed along (making simple configurations problems more frequent), cyber-attackers exploring Zero-Day attacks by understanding patching events from vendors, multiple potential points of access across the infrastructure (huge attack surfaces, as mentioned), sheer device heterogeneity (actually, this helps cyber security, only difficulties are coping, updating and patching a wide range of systems with different requirements).</p>

<p><b>References</b></p>
[1] Xu, S., Yung, M., & Wang, J. (2021). <b><i>Seeking Foundations for the Science of Cyber Security: Editorial for Special Issue of Information Systems Frontiers.</i></b> Information Systems Frontiers, 23(2), 263-267.<br>
[2] Daszczyszak, R., Ellis, D., Luke, S., & Whitley, S. (2019). <b><i>TTP-based hunting.</i></b> MITRE CORP MCLEAN VA.<br>
[3] Nicol, D. M., Sanders, W. H., & Trivedi, K. S. (2004). <b><i>Model-based evaluation: from dependability to security.</i></b> IEEE Transactions on dependable and secure computing, 1(1), 48-65.<br>


<p align="right">Birmingham, 03/04/2022</p>
<p align="left"><font color="dddddd"><a href="#top">top</a></font></p>

<br>
<hr size="1" align="left" height="100" color="#aabbcc">


<!-- ########################################### -->
<a name="fearmongering"></a>
<h2>"Fearmongering"</h2>
<p class="first-word-uppercase-p">I was once very young (in the 80's early 90's) and I still remember people and the media going crazy and talking about the "hole on the ozone layer".
That's how all started for me.
We were suppose to protect ourselves specially around 11am to 14pm, and they would track the hole in (near) real-time and report it constantly over the tele.
</p>

<p>What is <b><i>fearmongering</i></b>?</p>

<table border="0" cellpadding="10">
 <tr>
  <td width="1%" bgcolor="#eeffee">&nbsp;</td>
  <td style = "border: 1px solid green;" bgcolor="#eeeeff">
<i>"Fearmongering, or scaremongering, is the action of intentionally trying to make people afraid of something when this is not necessary or reasonable."</i>
<br/>
<a href="https://dictionary.cambridge.org/dictionary/english/fearmongering" target="_blank">Cambridge Dictionary</a>&#x2197; (on-line)
</td>
 </tr>
</table>

<p>From that onwards, I heard about the disastrous consequences of the "Millenium Bug" (or Year 2000 Bug, or Y2K) that, let's take a moment to digest, was widely discussed in both 1999 AND 2000!
Nowadays people talk about the "Climate Emergence", and the nefarious consequences to future generations and so on.</p>

<p>That crisis was obviously averted.</p>

<p>Look, I am a scientist, I believe in science (yes, yes, I know this is not a widely accepted stance), and I trust science in my daily life encounters with almost everything I interact in my daily routine.
I understand the difficulties of learning complex phenomena and provide evidence and facts, however, I think that this 'catastrophe mode' will take us nowhere.</p>

<p>Over the years, we came to understand that scientists tackled the Ozone Layer problem through sensible reductions of chlorofluorocarbons (CFC) gases in the atmosphere, among other actions.
And for the Y2K bug, computing scientists worked head on to modify systems to cope with it, with no massive problems to users.
If we, as humanity, have a problem, we should try to first understand it (really understand it), reason about it, devise experiments to confirm/refute our hypothesis, and act on it, diminishing its impact to the populace.
Let's wait and see.</p>

<hr color="#ee8811" width="10%" size="1" align="left">

<p>Now, let's do some <b><i>fearmongering of our own.</i></b></p>
<p>Our next computing related crisis is set to happen on Tuesday, 19 January 2038, by 03:14:07, where the latest time since 1 January 1970 systems may store using a signed 32-bit integer will be set, in what it's called the <a href="https://en.wikipedia.org/wiki/Year_2038_problem" target="_blank">"Year 2038 Problem" (or "the epochalypse"</a>&#x2197;).
Well, we have 17 years (as of 2021) to either stop using this outdated 'technology' altogether or update systems (somehow) so they don't stop working when that fatidic timestamp arrives.</p>

<p align="right">Birmingham, 03/11/2021</p>
<p align="left"><font color="dddddd"><a href="#top">top</a></font></p>


<br>
<hr size="1" align="left" height="100" color="#aabbcc">

<!-- ########################################### -->
<a name="science-as-the-answer"></a>
<h2>"Science is the most likely answer"</h2>
<p class="first-word-uppercase-p">The collapse of Arecibo’s dish and the institutional neglect over a wide timeframe has got me thinking about science.</p>

<p>About its advances in the past 20 to 30 years. About our lack of understanding on how the brain or consciousness work, whether we are alone in the vast infinite universe or if other species live and breathe in Earth-like (or not) planets.</p>

<p>Another example concerns the ocean’s floor; I learned that it is not thoroughly mapped as of today. We have gaps in knowledge about Mars, and Venus is like a mythical place where odd things happen.</p>

<p>Space in general is a huge (infinite) unknown variable.</p>

<p>Sadly, to this day, many people somehow still think Earth is flat, the horoscope is somehow based on facts for a lot of people, socialism is communism, centre is left, left is alt-left, and viruses are secretly bio-engineered to harm specific populace or track the movement of 90 years old ladies from Coventry.</p>

<p>In times of uncertainty and difficult to grasp science facts, the easiest, superficial, not based on evidence explanations are accepted and embraced as the only truth and if you don’t go along, then you’re not my friend or like-mind belonging to my hive.</p>

<p>God did this, the oracle predicted that, a divine intervention occurred there, a miracle, unexplained activities, unknown flying objects (regardless of improving resolution, bystanders insist on using outdated cameras and shimmering hands), and so on.</p>

<p>For our sake, in many situations Occam’s Razor provides the perfect explanation. Sadly, this is not true in our confirmation biased new world order.</p>

<p>Although it is often boring, people seem to prefer more ‘colourful’ explanations with no basis in reality. Those ‘explanations’ are more like hallucinations or weird dreamlike situations.</p>

<hr color="#ee8811" width="10%" size="1" align="left">
<p>To my complete dismay, relentlessness and sleepless nights, a lot of today’s efforts in Research & Development in big companies are directed to product advertisements (not surprisingly the F and G in FAANG+M are fundamentally ad-oriented enterprises).</p>

<p>Buzz Aldrin professed <i>"You promised me Mars colonies. Instead I got Facebook."</i> How sad. That (weird) social network owner and <i>visionaire extraordinaire</i> is now a <i>billionaire</i>.</p>

<p>Some Internet based companies main focus is to ‘discover’ and ‘research’ new ways of targeting products to consumers, tailored to their posts and attached to their personal world views and moral standpoints.</p>

<p>Does society need that? Maybe the capitalist society thrives and feeds on exactly that.
The result is a large amount of distracted and unproductive people always needing the next gadget to fulfill their frivolous desires.</p>

<p>Social networks are ripe of posts of successful stories only. Not many of those are depicting stressful situations, humiliating stepping stones, people bullying you around, or any difficulties throughout harsh life paths. Only good things, people laughing and having a good time abroad or in restaurants - these things have been debated to exhaustion, so I won’t bother.</p>

<hr color="#ee8811" width="10%" size="1" align="left">
<p>Searching stuff around it’s the same. It is a version of what is expected, curated by some tailored algorithm that knows everything about you. It’s the same for the news, it shows and presents world views of someone else’s, based on things someone wants you to watch.</p>

<p>In the current Internet you can’t exactly find what you are looking for, just a glimpse of some company sponsored data piece a piece of software puts in front of you to digest.</p>

<p>The difficulty is and has always been about asking the right questions. Especially in a world plagued by ChatGPT <font color="aabbaa">(added excerpt on 9/4/23)</font>. Remember that we, as researchers, should embrace difficult to grasp ideas and try to explain and justify those to broader audiences with abstractions and useful artefacts that convey ideas and outcomes.</p>

<p>Scientific results with the wrong questions end up at the Limbo of Oblivious Papers - those that will be certainly retrieved in searches but never actually read or referenced by others.</p>

<hr color="#ee8811" width="10%" size="1" align="left">
<p>How to escape such inevitability? How to make relevant remarkable things? How to build novelty on top of other people’s work? How to actually capture the interest of audiences? How to influence other people? How to impact and change prevalent conceptions?</p>

<p>There are no easy answers to any of these questions. There is no overarching framework, mindset or model that fits all. And that is what science is all about. The systematic pursuit of knowledge and the truth. Diffusing ideas to broader audiences. Asking and trying to answer good relevant research questions that impact human’s livelihoods.</p>

<p>This process though must be conducted with sound and solid research combined to firm ethical grounds.</p>

<p>Paying close attention to all research phases, hypotheses, designing experiments, choosing metrics, measuring, collecting, and commenting results and observations.</p>

<p>Never disregarding your scientific integrity, since, at the end of the day, it is your reputation on the line.</p>

<p>Whenever you are pleased with what you achieved, then it's time to stop your contribution and suggest improvements so other researchers may use your work to advance other techniques and ideas.</p>

<p align="right">Birmingham, 22/06/2020</p>
<p align="left"><font color="dddddd"><a href="#top">top</a></font></p>

<br>
<hr size="1" align="left" height="100" color="#aabbcc">

<!-- ##copy from here######################################### -->
<a name="clever-name"></a>
<h2>"Clever Title"</h2>
<p class="first-word-uppercase-p">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
</p>
<p>More content and ranting away.</p>

<hr color="#ee8811" width="10%" size="1" align="left">
<p>Dividing content with horizontal rules.</p>

<p>Finally, ending it and adding a date.</p>
<p align="right">Birmingham, 01/01/1970</p>
<p align="left"><font color="dddddd"><a href="#top">top</a></font></p>

<br>
<hr size="1" align="left" height="100" color="#aabbcc">
<!-- ##until here## -->

<hr size="500" color="#ffffff">

<?php
include("postfooter.php");
?>

            </div> <!-- div main -->

          </div> <!-- div columns -->

        </div> <!-- div wrapper -->

<?php
include("footer.php");
?>

</div>
</body>
</html>