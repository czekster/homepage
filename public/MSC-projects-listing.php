<?php
include_once('session_valid.php');
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="generator" content="GitLab Pages">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Ricardo M. Czekster's homepage</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="syntax.css">
	<link rel="stylesheet" href="main.css">
   <link rel="stylesheet" href="style.css">
<style>
.button {
  background-color: #04AA6D;
  border: none;
  color: white;
  padding: 15px 32px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
  margin: 4px 2px;
  cursor: pointer;
}
</style>
  </head>

<body class="vsc-initialized">
  <div class="wrapper">
	<div class="columns">
<?php
include("sidebar.php");
?>
	  <div class="main">
<h1>MSc - Projects listing</h1>

<?php
   if (isset($_SESSION['user']) && $_SESSION['user'] != "nok") {
?>
<!-- main block-->
<div align="right">
<form method="POST" action="logout.php">
 <button class="button">Logout</button>
</form>
</div>

<p>Here are some ideas for MSc projects. Use it as something to give you insights to <b>your own idea</b>.
</p>

<p>Feel free to contact me after you know (more or less) what you would like to pursuit.
</p>

<hr width="100%" align="left" color="#91A3AB">

<a name="top"></a>
<h2>Projects</h2>
&bull;&nbsp;Which LLM tool creates the highest coverage cybersecurity application testing?<br/>
&bull;&nbsp;Is it possible to create useful threat models out of LLM?<br/>
&bull;&nbsp;How different LLM analyses cybersecurity related data?<br/>
&bull;&nbsp;Is it possible to teach an LLM to advise security testing in organisations?<br/>
&bull;&nbsp;How LLM perform risk assessments tailored for cybersecurity?<br/>

<br><br>
<hr width="100%" align="left" color="#91A3AB">

<!--  ###### -->

<!-- end main block-->
<?php
   }
?>


<?php
include("postfooter.php");
?>

            </div> <!-- div main -->

          </div> <!-- div columns -->

        </div> <!-- div wrapper -->

<?php
include("footer.php");
?>

</div>
</body>
</html>