<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="generator" content="GitLab Pages">
    <meta name="keywords" content="ricardo melo czekster, czekster, cyber security"/>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Ricardo M. Czekster's homepage</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="syntax.css">
	<link rel="stylesheet" href="main.css">
    <link rel="stylesheet" href="style.css">
  </head>

<body class="vsc-initialized">
  <div class="wrapper">
	<div class="columns">
<?php
include("sidebar.php");
?>
	  <div class="main">
		<h1>Research</h1>
<p>I am an active researcher in dependability plus cyber security, according to my <a href="https://dblp.uni-trier.de/pid/56/500.html" target="_blank">DBLP profile</a>&#x2197; or my <a href="https://scholar.google.com.br/citations?user=M4SZcHsAAAAJ" target="_blank">Google Profile</a>&#x2197;.</p>

<p>First and foremost, check out my <a href="research-faq.php">Frequently Asked Questions</a> (FAQ) about <b>Research</b> or read some <a href="research-highlights.php">highlights</a> about this topic.
</p>

<br><hr style="width:50%;text-align:left;margin-left:0;color:#ddeeff">
<a name="top"></a>
<h2>Index</h2>
&bull;&nbsp;<a href="#focus">Focus</a><br>
&bull;&nbsp;<a href="#datasets">Datasets</a><br>
&bull;&nbsp;<a href="#concepts">Key concepts in research</a><br>
&bull;&nbsp;<a href="#reproducibility">On reproducibility in computing</a><br>
&bull;&nbsp;<a href="#other-interests">Other interests</a><br>

<br><hr style="width:50%;text-align:left;margin-left:0;color:#ddeeff">
<br>

<a name="focus"></a>
<h2 id="beginning">Focus</h2>

<p>My research interest lies in building <b>secure software</b>.</p>

<p>I'm interested in the following research topics:</p>
<ul>
  <li>Secure Testing</li>
   <ul>
    <li>Automated test generation</li>
    <li>Approaches like fault injection, mutation testing, fuzzing, <i>etc.</i></li>
	<li>Tools to incorporate into CI/CD pipelines for increasing software quality</li>
   </ul>
   <li>Software and Application Security (AppSec)</li>
   <ul>
    <li>Cyber security in Software Development Life-Cycle (SDLC)</li>
    <li>Domain Driven Design (DDD) applied to cyber security</li>
    <li>Vulnerability life-cycle and impact on software quality</li>
   </ul>
  <li>Attack Modelling Techniques (AMT)</li>
   <ul>
    <li>Reasoning and modelling adversarial behaviours</li>
    <li>Cyber Threat Intelligence (CTI) and modelling (eg, <a href="http://docs.oasis-open.org/cti/stix/v2.0/stix-v2.0-part1-stix-core.html" target="_blank">STIX&trade;</a>&#x2197;)</li>
    <li>Threat Hunting</li>
     <ul><li>Methods and tools for intrusion detection</li></ul>
   </ul>  
  <li>Tackling large longitudinal (near) realtime datasets for cyber security</li>
   <ul>
    <li>Excess alerting problem and solutions, reporting and false positives analysis</li>
    <li>Time series data obtained from sensing/monitoring infrastructure, <i>etc.</i></li>
   </ul>
</ul>
</p>

<table border="0" cellpadding="10">
 <tr>
  <td width="1%" bgcolor="#eeffee">&nbsp;</td>
  <td style = "border: 1px solid green;" bgcolor="#eeeeff"><p>The Cybersecurity and Infrastructure Security Agency (CISA/US) states that <i>"Cyber security is the art of protecting networks, devices, and data from unauthorized access or criminal use and the practice of ensuring confidentiality, integrity, and availability of information"</i>.
</p></td>
 </tr>
</table>
<p>I would like to stress out that if one wants to be successful in cybersecurity, one must understand and appreciate <i>technology</i>, which is at the base of everything we want to protect.</p>
<a href="#top">top</a><br/><br/>

<hr style="width:50%;text-align:left;margin-left:0;color:#ddeeff">

<a name="datasets"></a>
<h2>Datasets</h2>
<p>You will find next a non-comprehensive list of datasets for research (or other purposes, like learning how to tackle large datasets, and so on).</p>
<ul>
 <li>General purpose</li>
 <ul>
  <li><a href="https://www.kaggle.com/datasets" target="_blank">Kaggle</a>&#x2197; (general link for datasets)</li>
  <li><a href="https://www.data.gov.uk/" target="_blank">Find open data</a>&#x2197; (UK)</li>
  <li><a href="https://data.worldbank.org/" target="_blank">World Bank Open Data</a>&#x2197;</li>
  <li><a href="https://registry.opendata.aws/" target="_blank">Registry of Open Data on AWS</a>&#x2197; (Amazon Web Services)</li>
 </ul>
 <li>Cyber security related</li>
 <ul>
  <li><a href="https://github.com/gfek/Real-CyberSecurity-Datasets" target="_blank">Real CyberSecurity-Datasets</a>&#x2197;</li>
  <li><a href="https://github.com/shramos/Awesome-Cybersecurity-Datasets" target="_blank">Awesome-Cybersecurity-Datasets</a>&#x2197;</li>
  <li><a href="https://github.com/rshipp/awesome-malware-analysis" target="blank">Awesome Malware Analysis</a>&#x2197;</li>
  <li><a href="http://www.sysnet.ucsd.edu/projects/url/" target="_blank">Detecting Malicious URLs</a>&#x2197;</li>
  <li><a href="https://csr.lanl.gov/data/cyber1/" target="_blank">Comprehensive, Multi-Source Cyber-Security Events</a>&#x2197; (requires explanations on how it will be used)</li>
 </ul>
</ul>
<a href="#top">top</a><br/><br/>

<hr style="width:50%;text-align:left;margin-left:0;color:#ddeeff">

<a name="concepts"></a>
<h2>Key concepts in research</h2>
<p>Get familiarised with the following concepts:
<ul>
 <li><b>Research ethics</b></li>
 <li>Methodogy (and following one)</li>
 <li>Impact, finding good venues, etc.</li>
 <li>Repeatability* (Same team, same experimental setup)</li>
 <li>Reproducibility (Different team, different experimental setup)</li>
 <li>Replicability (Different team, same experimental setup)</li>
</ul>
*<a href="https://www.acm.org/publications/policies/artifact-review-and-badging-current" target="_target">Link at ACM</a>
</p>
<p><b><i>Reproducibility</i></b> refers to the ability of different experts to produce the same results from the same data.</p>
<p><b><i>Repeatability</i></b> refers to the ability to repeat the assessment in the future, in a manner that is consistent with and hence comparable to prior assessments—enabling the organization to identify trends.</p>
<a href="#top">top</a><br/><br/>

<hr style="width:50%;text-align:left;margin-left:0;color:#ddeeff">

<a name="reproducibility"></a>
<h2>On reproducibility in computing</h2>

<a href="https://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1003285" target="_blank">Ten Simple Rules for Reproducible Computational Research</a>&#x2197;:
<ul>
 <li>Rule 1: For Every Result, Keep Track of How It Was Produced</li>
 <li>Rule 2: Avoid Manual Data Manipulation Steps</li>
 <li>Rule 3: Archive the Exact Versions of All External Programs Used</li>
 <li>Rule 4: Version Control All Custom Scripts</li>
 <li>Rule 5: Record All Intermediate Results, When Possible in Standardized Formats</li>
 <li>Rule 6: For Analyses That Include Randomness, Note Underlying Random Seeds</li>
 <li>Rule 7: Always Store Raw Data behind Plots</li>
 <li>Rule 8: Generate Hierarchical Analysis Output, Allowing Layers of Increasing Detail to Be Inspected</li>
 <li>Rule 9: Connect Textual Statements to Underlying Results</li>
 <li>Rule 10: Provide Public Access to Scripts, Runs, and Results</li>
</ul>
<p>Look at <a href="https://blog.padhye.org/Artifact-Evaluation-Tips-for-Authors/" target="_blank">Artifact Evaluation</a>&#x2197; (by Rohan Padhye) webpage for some tips.</p>

<a href="#top">top</a><br/><br/>

<hr style="width:50%;text-align:left;margin-left:0;color:#ddeeff">

<a name="other-interests"></a>
<h2 id="now-this">Other interests</h2>

<p>I'm interested in a plethora of topics, ranging from:
 <ul>
   <li><b>Non-Functional Properties (NFP) of Systems</b></li>
   <ul>
    <li>Major areas of interest: dependability plus cyber-security, performance, fault-tolerance, usability</li>
   </ul>
  </ul>

  <ul>
   <li><b>Quantitative Performance Evaluation of Systems</b></li>
   <ul>
    <li>Modelling &amp; Simulation (M&amp;S): abstractions, mapping, parametrisation, accreditation, multiple scenario "what-if" analysis, statistical analysis</li>
    <li>Analytic Modelling: Markov processes and structured Markov Chains</li>
    <li>Monitoring: instrumentation and impact on performance, novel approaches, techniques, and methods</li>
   </ul>
  </ul>
</ul>

</p>
<a href="#top">top</a><br/><br/>


<br><br><br><br><br><br><br><br><br><br><br><br><br>

<?php
include("postfooter.php");
?>

            </div> <!-- div main -->

          </div> <!-- div columns -->

        </div> <!-- div wrapper -->

<?php
include("footer.php");
?>

</div>
</body>
</html>
