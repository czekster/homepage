<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="generator" content="GitLab Pages">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Ricardo M. Czekster's homepage</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="syntax.css">
	<link rel="stylesheet" href="main.css">
    <link rel="stylesheet" href="style.css">
  </head>

<body class="vsc-initialized">
  <div class="wrapper">
	<div class="columns">
<?php
include("sidebar.php");
?>
	  <div class="main">
		<h1>Tips for writing better</h1>
<table border="0" align="right"><tr><td><img align="right" width="200" src="images/writing.png"><font size="-1"><br/>Source: <a href="https://commons.wikimedia.org/wiki/File:Fountain_pen_writing_(literacy).jpg"> (CC BY-SA 4.0)</a></font></td></tr></table>
<p>What are the desired characteristics of good scientific texts?</p>
<p>I can think of conciseness, precision, ordered 'stream of ideas/concepts/notions'.</p>
<p>For starters, I recommend the following link: <a href="http://www.plainenglish.co.uk/how-to-write-in-plain-english.html" target="_blank">How to write in plain English</a>.&#x2197;</p>

<br><hr style="width:50%;text-align:left;margin-left:0;color:#ddeeff"><br>

<p><b>TIP#1</b>: You want other researchers to understand what the work is about, what has been done, in an unambiguously fashion. And then, you want them to cite your work because what you did was not only relevant but understandable and reproducible.</p>
<p><font size="+2">&rarr;&nbsp;</font>
<b><i>"Keep It Simple, Stupid (KISS)."</i></b> Write concisely, with meaning, in a simple fashion. Your <i>writing panache</i> is not on judgement here.
</p>

<br><hr style="width:50%;text-align:left;margin-left:0;color:#ddeeff"><br>
<p><b>TIP#2</b>: I recommend avoiding passive voice sentences like the plague. Example: "it is shown by the example", "tests were conducted", "a wealth of research was proposed".
</p>
<p><font size="+2">&rarr;&nbsp;</font>
Who's doing what? That's unclear, because the author has (<font color="#1199ff">mistakenly?</font>) used passive voice to convey that idea. Who's the agent of the action?
You will notice that, starting now, when YOU read a paper and the author uses passive voice all over the text, you'll know his mistakes and his failed approach to convey research.</p>

<p>Read some <a href="https://advice.writing.utoronto.ca/revising/passive-voice/" target="_blank">tips on how you should avoid using the passive voice</a>&#x2197; on scientific literature.</p>

<br><hr style="width:50%;text-align:left;margin-left:0;color:#ddeeff"><br>
<p><b>TIP#3</b>: You should not use double negatives for stating a positive: "The above definitions are not incompatible."</p>
<p><font size="+2">&rarr;&nbsp;</font>
See TIP#1 again</p>

<br><hr style="width:50%;text-align:left;margin-left:0;color:#ddeeff"><br>
<p><b>TIP#4</b>: You must employ <i>clarity</i> over <i>complexity</i> whilst crafting sentences.</p>

<br><hr style="width:50%;text-align:left;margin-left:0;color:#ddeeff"><br>
<p><b>TIP#5</b>: I suggest employing <i>Multiple Iterative Editing&reg;</i>: weaving papers and scientific communications, working, editing, re-reading, improving sentences.
You should re-read all over again multiple times.</p>

<br><hr style="width:50%;text-align:left;margin-left:0;color:#ddeeff"><br>
<p><b>TIP#6</b>: <i>"Give it a rest"</i>, i.e., "sleep on it" for two or three weeks. Clearly after a while you have become 'addicted' to your own (beloved) phrases and sentences.</p>

<p>You might even think you have a high quality piece in your hands, however, you probably (still) <i>do not</i>.
This time off the text will help you to regroup your reasoning and rethink what you have up to a point, hinting you with ideas on how to improve the text further.
</p>

<br><hr style="width:50%;text-align:left;margin-left:0;color:#ddeeff"><br>
<p><b>TIP#7</b>: Find more tips and effectively improve your <b><i>own</i></b> (not ChatGPT's) writing.
</p>

<!--<br><hr style="width:50%;text-align:left;margin-left:0;color:#ddeeff"><br>-->
<!-- new tip -->

<?php
include("postfooter.php");
?>

            </div> <!-- div main -->

          </div> <!-- div columns -->

        </div> <!-- div wrapper -->

<?php
include("footer.php");
?>

</div>
</body>
</html>