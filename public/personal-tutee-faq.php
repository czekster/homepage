<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="generator" content="GitLab Pages">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Ricardo M. Czekster's homepage</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="syntax.css">
	<link rel="stylesheet" href="main.css">
    <link rel="stylesheet" href="style.css">
  </head>

<body class="vsc-initialized">
  <div class="wrapper">
	<div class="columns">
<?php
include("sidebar.php");
?>
	  <div class="main">
		<h1>Personal Tutee FAQ</h1>

<a name="top"></a>
<h2>Questions</h2>
&bull;&nbsp;<a href="#religious-pilgrimage">Could I submit Extended Leave or Exceptional Circumstance (EC) for religious pilgrimage?</a><br>
&bull;&nbsp;<a href="#programming-resources">What programming resources could I use to learn?</a><br>
&bull;&nbsp;<a href="#cert-cyber">How to engage with certifications in cybersecurity?</a><br>
<!--
&bull;&nbsp;<a href="#meetings">How to prepare for meetings?</a><br>
-->
<!--&bull;&nbsp;<a href="#"></a><br>-->


<!-- question -->

<br><br><hr style="width:50%;text-align:left;margin-left:0;color:#ddeeff">
<a name="religious-pilgrimage"></a>
<h2>Could I submit Extended Leave or Exceptional Circumstance (EC) for religious pilgrimage?</h2>
<p>
According to this <a href="https://www.aston.ac.uk/sites/default/files/2022-09/273788-Religion%20and%20Belief%20Guidance%20Notes.pdf">Aston Document dated Sep/2022</a>:
<table border="0">
 <tr>
  <td rowspan="2" bgcolor="#AAFFBB" width="10%">&nbsp;</td>
  <td bgcolor="#EEFFEE"><i>9. Student Issues<br>
    9.1 Leave during Assessment or Examination periods - Students have a responsibility to fulfil the requirements of their course of study in order to achieve the relevant award. Students should complete a Religious Commitments, Examinations and Assessment form at the beginning of the Academic year, stating any religious festivals during which they would be unable to undertake examinations or assessments for religious reasons and return this to the relevant School’s Director of Programmes by the date stated on the form. The University’s Examination’s Officer will then be advised of requests affecting formal examinations. In consultation with the appropriate faith leaders, the University will endeavour, to a proportionate and reasonable extent, to make arrangements for students whose faith requires them to miss assessments for reasons of religious observance to take assessments at a different time. Where there is no written notice of the appropriate dates, the University will normally treat the delayed assessment as a referral.</i>
   </td>
 </tr>
 <tr>
  <td bgcolor="#EEFFEE"><i>
  9.2 Extended Leave - Students are expected to make a commitment to the full Academic Year of Study. However, in exceptional circumstances a student may request extended leave at a particular time for the purpose of going on religious pilgrimage. Such requests should be made in writing to the relevant School’s Director of programmes at the earliest opportunity, along with a letter of support from a member of the appropriate faith group. Students will be responsible for making good any missed learning opportunities. If the extended leave involves missing significant periods of study and/or assessments the student may be required to repeat relevant modules and assessments or the whole of the academic year.</i>
  </td>
 </tr>
</table>
</p>


<a href="#top">Go back to questions</a><br>


<!-- question -->

<br><br><hr style="width:50%;text-align:left;margin-left:0;color:#ddeeff">
<a name="programming-resources"></a>
<h2>What programming resources could I use to learn?</h2>
<p>
<ul>
 <li><a href="https://github.com/Dylan-Israel/ultimate-coding-resources" target="_blank">Ultimate coding resources - curated list</a>&#x2197;</li>
 <li><a href="https://www.codecademy.com/" target="_blank">Code Academy</a>&#x2197;</li>
 <li><a href="https://www.w3schools.com/" target="_blank">W3C School</a>&#x2197;</li>
 <li>YouTube resources</li>
 <ul>
  <li><a href="https://www.youtube.com/@freecodecamp" target="_blank">FreeCodeCamp YT Channel</a></li>
 </ul>
</ul>
</p>


<a href="#top">Go back to questions</a><br>


<!-- question -->

<br><br><hr style="width:50%;text-align:left;margin-left:0;color:#ddeeff">
<a name="cert-cyber"></a>
<h2>How to engage with certifications in cybersecurity?</h2>
<p>
<ul>
 <li>Check out <a href="cyber-cert/index.html">my webpage about certifications in cybersecurity</a></li>
</ul>
</p>


<a href="#top">Go back to questions</a><br>





<!-- question -->

<!--
<br><br><hr style="width:50%;text-align:left;margin-left:0;color:#ddeeff">
<a name="name"></a>
<h2>question?</h2>
<p>
</p>

<a href="#top">Go back to questions</a><br>

-->

<br><br><br><br><br><br><br><br><br><br><br><br><br>
<?php
include("postfooter.php");
?>

            </div> <!-- div main -->

          </div> <!-- div columns -->

        </div> <!-- div wrapper -->

<?php
include("footer.php");
?>

</div>
</body>
</html>
