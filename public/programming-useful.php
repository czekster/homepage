<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="generator" content="GitLab Pages">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Ricardo M. Czekster's homepage</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="syntax.css">
	<link rel="stylesheet" href="main.css">
    <link rel="stylesheet" href="style.css">
  </head>

<body class="vsc-initialized">
  <div class="wrapper">
	<div class="columns">
<?php
include("sidebar.php");
?>
	  <div class="main">
<h1>Programming - useful things</h1>

<p>This is more for me than to anyone else, really. Oftentimes I forget important commands, which I will try to put them here. Enjoy.
</p>

<hr>
<h2>Git</h2>

<p>1. Working with Git:</p>
<p>Before I start putting some Git commands here, I invite you to access <a href="https://beej.us/guide/bggit/" target="_blank">Beej's Guide to Git</a>. It is an invaluable resource.</p>
<hr>

<p>To use Git you can use a GUI or the command-line. Next I'll show some commands, but if you prefer a GUI, for MS-Windows, download <a href="https://desktop.github.com/download/" target="_blank">GitHub Desktop</a>&#x2197; (suggestion).</p>

&bull;&nbsp;Some basic commands.
<pre style="overflow-x:hidden;">
# observe the '.' by the end!!
git clone git@gitlab.com:czekster/coolproject.git .
# note that I clone using ssh - you need to add key at gitlab! (you could clone with https as well)
# see note below: How do I add an SSH key in gitlab? - Stack Overflow

# then, modify a file on that folder, and add it to the repository:
git add .
# then
git commit -m "Message."
# then
git push
# and to get modifications on this repo, use
git pull
</pre>

<p>Check this: <a href="https://stackoverflow.com/questions/35901982/how-do-i-add-an-ssh-key-in-gitlab" target="_blank">How to add an SSH key in GitLab</a>&#x2197;.</p>

<p>&bull;&nbsp;Case you deleted a file, and immediately realised it was a mistake?</p>
<pre style="overflow-x:hidden;">
#This one is easy, just do:
git checkout HEAD <filename>
# or just git checkout <filename>
</pre>

<p>&bull;&nbsp;Remember that <font face="Courier">.gitignore</font> will only ignore files *NOT* yet on repository. To remove those files from your repository (but not from your file system) run <font face="Courier">git rm --cached</font> on them. <a href="https://stackoverflow.com/questions/45400361/why-is-gitignore-not-ignoring-my-files" target="_blank">Also read this thread</a>&#x2197;.</p>

<p>
&bull;&nbsp;More useful commands:</p>
<ul>
 <li>Learn Git! Learn the difference between <b>Clone</b> and <b>Fork</b></li>
 <li>Create a branch in the Command Line Interface (CLI): <font face="Courier">git checkout -b <BRANCH></font></li>
 <li>List remotes: <font face="Courier">git remove -v</font></li>
 <li>Check changes (useful to check before commit!): <font face="Courier">git status</font></li>
</ul>





<!--
<a href="" target="_blank"></a>&#x2197;
-->

<hr>
<h2>GNU/Linux</h2>

<p>
&bull;&nbsp;Basic commands:</p>
<ul>
 <li>Adding users: <font face="Courier">adduser <USER></font> (this will create 'proper' users, with /home/user folders, etc.)</li>
 <li>How to use scp without password: useful for scripting backups, etc. <a href="https://alvinalexander.com/linux-unix/how-use-scp-without-password-backups-copy/" target="_blank">Check this link first</a>&#x2197;</li>
 <li>Adding user to sudoers group: (If you want it to be root... careful): <font face="Courier">usermod -aG sudo <USER></font></li>
<br>
 <li>Host name: <font face="Courier">hostname</font></li>
 <li>Kernel info: <font face="Courier">lsb_release -a</font></li>
 <li>OS-version: <font face="Courier">cat /etc/os-release</font></li>
 <li>CPU info: <font face="Courier">lscpu</font></li>
 <li>Memory info: <font face="Courier">free -h</font></li>
 <li>Disks: <font face="Courier">df -h</font></li>
 <li>Installing things: <font face="Courier">apt install htop gcc g++ make nmap unzip sysstat plocate net-tools</font></li>
 <ul>
  <li><font face="Courier">ncdu</font>: disk usage (interesting and quick)</li>
  <li><font face="Courier">btop</font>: better than htop, same feature</li>
 </ul>
</ul>



<?php
include("postfooter.php");
?>

            </div> <!-- div main -->

          </div> <!-- div columns -->

        </div> <!-- div wrapper -->

<?php
include("footer.php");
?>

</div>
</body>
</html>