<?php
session_start();
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="generator" content="GitLab Pages">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Ricardo M. Czekster's homepage</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="syntax.css">
	<link rel="stylesheet" href="main.css">
   <link rel="stylesheet" href="style.css">
<style>
.button {
  background-color: #04AA6D;
  border: none;
  color: white;
  padding: 15px 32px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
  margin: 4px 2px;
  cursor: pointer;
}
</style>
  </head>

<body class="vsc-initialized">
  <div class="wrapper">
	<div class="columns">
<?php
include("sidebar.php");
?>
	  <div class="main">
<h1>MSC - Projects - <font color="#EE0011">Restricted Area</font></h1>

<p>If you are an Aston student, <b>send me an email</b> using your Aston account asking for the user/pass to enter this section (don't put your Aston credentials here).</p>

<?php
if ((!isset($_SESSION['user']) || $_SESSION['type'] != "msc") || (isset($_SESSION['user']) && $_SESSION['user'] == "nok") ) {
?>
<div style="border:1px solid black; padding-left:8px; width:55%; background:rgba(0, 128, 0, 0.1);">
 <form method="POST" action="login.php">
  <input type="hidden" name="type" value="msc">
  <p>
   <label>User:</label><br>
   <input class="w3-input w3-border" type="text" name="user">
  </p>
  <p>
   <label>Password:</label><br>
   <input class="w3-input w3-border" type="password" name="passwd">
  </p>
  <p>
   <button class="button">Login</button>
  </p>
 </form>
</div>

<?php
   if (isset($_SESSION['message'])) {
      echo ("<p>".$_SESSION['message']."</p>");
   }
?>

<?php
} else {
?>
 <form method="POST" action="MSC-projects-listing.php">
   <button class="button">Enter</button>
 </form>
<?php
}
?>


<?php
include("postfooter.php");
?>

            </div> <!-- div main -->

          </div> <!-- div columns -->

        </div> <!-- div wrapper -->

<?php
include("footer.php");
?>

</div>
</body>
</html>