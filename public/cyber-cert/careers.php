<!DOCTYPE html>
<html lang="en">
<!-- Header -->
<?php include('header.php'); ?>
<body>

<!-- Navbar -->
<?php include('navbar.php'); ?>

<!-- Sidebar -->
<?php include('sidebar-main.php'); ?>

<!-- Overlay effect when opening sidebar on small screens -->
<div class="w3-overlay w3-hide-large" onclick="w3_close()" style="cursor:pointer" title="close side menu" id="myOverlay"></div>

<!-- Main content: shift it to the right by 250 pixels when the sidebar is visible -->
<div class="w3-main" style="margin-left:250px">

  <div class="w3-row w3-padding-64">
    <div class="w3-twothird w3-container">
      <h1 class="w3-text-purple">Careers</h1>
      <p>See the <a href="https://www.sans.org/cybersecurity-careers/20-coolest-cyber-security-careers/" target="_blank">20 <b>Coolest careers</b> in Cyber security</a> by the SANS Institute.</p>
      <p>The <a href="https://www.ukcybersecuritycouncil.org.uk/qualifications-and-careers/careers-route-map/" target="_blank">Cybersecurity Council's UK Career Route Map</a> lists 16 <b>specialisms</b> in cyber security: </p>

      <table class="w3-table w3-striped w3-border w3-bordered">
      <tr valign="center">
        <td class="w3-border">Digital Forensics</td>
        <td class="w3-border">Cyber Threat Intelligence</td>
        <td class="w3-border">Cyber Security Generalist</td>
      </tr>
      <tr>
        <td class="w3-border">Cyber Security Management</td>
        <td class="w3-border">Incident Reponse</td>
        <td class="w3-border">Network Monitoring & Intrusion Detection</td>
      </tr>
      <tr>
        <td class="w3-border">Vulnerability Management</td>
        <td class="w3-border">Security Testing</td>
        <td class="w3-border">Cryptography & Communications Security</td>
      </tr>
      <tr>
        <td class="w3-border">Secure Operations</td>
        <td class="w3-border">Identity & Access Management</td>
        <td class="w3-border">Secure System Architecture & Design</td>
      </tr>
      <tr>
        <td class="w3-border">Cyber Security Audit & Assurance</td>
        <td class="w3-border">Data Protection & Privacy</td>
        <td class="w3-border">Secure System Development</td>
      </tr>
      <tr>
        <td class="w3-border">Cyber Security Governance & Risk Management</td>
        <td class="w3-border">&nbsp;</td>
        <td class="w3-border">&nbsp;</td>
      </tr>
      </table>

    <!-- empty panel -->
    <div class="w3-panel w3-padding-8">
     <p></p>
    </div> 
    
    <div class="w3-panel w3-left w3-padding-8">
     <p>We list next other <b>roles</b> that you may find while looking for jobs in specialised web sites such as <a href="https://www.jobs.ac.uk/search/computer-sciences" target="_blank">jobs.ac.uk</a> or <a href="https://uk.indeed.com/jobs?q=cyber+security" target="_blank">indeed.ac.uk</a>:
     </p>
    </div>

    <div class="w3-card-4">

    <!-- start of first line -->
    <div class="w3-row-padding">
     <div class="w3-col s4"> <!-- 1st col -->
      <div class="w3-panel w3-center w3-border w3-hover-border-purple">
         <h4 class="w3-text-black">Chief Information Security Officer (CISO)</h4>
      </div>
     </div>

     <div class="w3-col s4"> <!-- 2nd col -->
      <div class="w3-panel w3-center w3-border w3-hover-border-purple">
         <h4 class="w3-text-black">Security <br>Administrator</h4>
      </div>
     </div>
     
     <div class="w3-col s4"> <!-- 3rd col -->
      <div class="w3-panel w3-center w3-border w3-hover-border-purple">
         <h4 class="w3-text-black">IT security <br>engineer</h4>
      </div>
     </div>

    </div>
    <!-- end of first line -->

    <!-- start of second line -->
    <div class="w3-row-padding">
     <div class="w3-col s4"> <!-- 1st col -->
      <div class="w3-panel w3-center w3-border w3-hover-border-purple">
         <h4 class="w3-text-black">Security <br>consultant</h4>
      </div>
     </div>

     <div class="w3-col s4"> <!-- 2nd col -->
      <div class="w3-panel w3-center w3-border w3-hover-border-purple">
         <h4 class="w3-text-black">Information assurance <br>analyst</h4>
      </div>
     </div>
     
     <div class="w3-col s4"> <!-- 3rd col -->
      <div class="w3-panel w3-center w3-border w3-hover-border-purple">
         <h4 class="w3-text-black">Cyber security <br>auditor</h4>
      </div>
     </div>

    </div>
    <!-- end of second line -->

    <!-- start of third line -->
    <div class="w3-row-padding">
     <div class="w3-col s4"> <!-- 1st col -->
      <div class="w3-panel w3-center w3-border w3-hover-border-purple">
         <h4 class="w3-text-black">Data governance <br>manager</h4>
      </div>
     </div>

     <div class="w3-col s4"> <!-- 2nd col -->
      <div class="w3-panel w3-center w3-border w3-hover-border-purple">
         <h4 class="w3-text-black">Director of information <br>security</h4>
      </div>
     </div>
     
     <div class="w3-col s4"> <!-- 3rd col -->
      <div class="w3-panel w3-center w3-border w3-hover-border-purple">
         <h4 class="w3-text-black">Information risk <br>consultant</h4>
      </div>
     </div>

    </div>
    <!-- end of third line -->

    </div>

    </div>

    <!-- ads -->
    <?php include('ads.php'); ?>
    <!-- end ads -->

  </div>

  <div class="w3-row">
    <div class="w3-twothird w3-container">
      <h3>Explanation of main roles</h3>
      <p>We explain some roles of cyber security professionals in organisations. Salaries information are based on data from October/2022.</b>

      <div class="w3-card-4">
       <header class="w3-container w3-deep-purple w3-left">
        <h4>Chief Information Security Officer (CISO)</h4>
       </header>

       <div class="w3-container">
        <p>Executive role that manage all protections within organisations and their assets. This is a <b>managerial role</b> that oversees other cyber security related professionals and attending customer data, privacy, and protections. To become a successful CISO you will need to harness communication skills, security strategy, collaborative and supervision skills, governance and policies.</p>
       </div>

       <footer class="w3-container w3-purple">
        <p><b>Salary:</b> Depends on level of expertise, starting at £70k/year, going upwards to an average of £90k/year.</p>
       </footer>

      </div>

      <!-- empty panel -->
      <div class="w3-panel w3-padding-8">
       <p></p>
      </div> 

      <div class="w3-card-4">
       <header class="w3-container w3-deep-purple w3-left">
        <h4>Security administrator</h4>
       </header>

       <div class="w3-container">
        <p>Installs, updates, administers and troubleshoots organisation's systems and digital assets. This is a <b>technical role</b> where professionals protect systems against unauthorised access, potential threats facing the organisation, analysing security requirements and implementing protective measures in systems and applications.</p>
       </div>

       <footer class="w3-container w3-purple">
        <p><b>Salary:</b> Average base is £25k/year, with potential of going upwards depending on skill level.</p>
       </footer>

      </div>

      <!-- empty panel -->
      <div class="w3-panel w3-padding-8">
       <p></p>
      </div> 

      <div class="w3-card-4">
       <header class="w3-container w3-deep-purple w3-left">
        <h4>Director of Information Security</h4>
       </header>

       <div class="w3-container">
        <p>Provides organisations expertise to governance and executive management. This is a <b>managerial role</b> where professionals are expected to be proficient on topics as vary as information security standards and best practices (e.g., ISO 17799, CobiT and ITIL).</p>
       </div>

       <footer class="w3-container w3-purple">
        <p><b>Salary:</b> Average is £105k/year.</p>
       </footer>

      </div>




    </div>

    <!-- empty panel -->
    <div class="w3-panel w3-padding-64">
     <p></p>
    </div> 


  </div>

<!-- Footer -->
<?php include('footer.php'); ?>

<!-- END MAIN -->
</div>

<!-- Further Scripts -->
<?php include('scripts.php'); ?>

</body>
</html>









