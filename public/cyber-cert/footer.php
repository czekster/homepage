  <footer id="myFooter">
    <div class="w3-container w3-theme-l2 w3-padding-8">
      <h4>Cyber Security Certifications and Career Routes</h4>
      <p><i>Maintained by the <a href="https://www.aston.ac.uk/courses/computer-science" target="_blank">Department of Computer Science</a><br>
      School of Informatics and Digital Engineering</i><br>
      <a href="https://www.aston.ac.uk/" target="_blank">Aston University</a>, Birmingham/UK</p>
      <p><b>Last update:</b> 26/10/2022</p>
    </div>

    <div class="w3-container w3-theme-l1">
      <p>Powered by <a href="https://www.w3schools.com/w3css/default.asp" target="_blank">w3.css</a></p>
    </div>
  </footer>
