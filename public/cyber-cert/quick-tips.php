<!DOCTYPE html>
<html lang="en">
<!-- Header -->
<?php include('header.php'); ?>
<body>

<!-- Navbar -->
<?php include('navbar.php'); ?>

<!-- Sidebar -->
<?php include('sidebar-main.php'); ?>

<!-- Overlay effect when opening sidebar on small screens -->
<div class="w3-overlay w3-hide-large" onclick="w3_close()" style="cursor:pointer" title="close side menu" id="myOverlay"></div>

<!-- Main content: shift it to the right by 250 pixels when the sidebar is visible -->
<div class="w3-main" style="margin-left:250px">

  <div class="w3-row w3-padding-64">
    <div class="w3-twothird w3-container">
      <h1 class="w3-text-purple">Quick tips</h1>
      <p>Further information related to certifications, training, and planing for the future: </p>

    <!-- 1st -->
    <div class="w3-twothird w3-container">   
     <div class="w3-panel w3-border w3-border-orange w3-leftbar w3-sand w3-text-dark-grey">
     <h4>Check updated information</h4>

     <div class="w3-container w3-cell">
      <p>This web-site only references potential certification organisations.</p>
      <p>The information retained here might be outdated, so <b>check for the latest information</b> on the specific organisation's web-site.</p>
     </div>

     </div> <!-- larger div -->
    </div>
    <!-- 1st -->

    <!-- 2nd -->
    <div class="w3-twothird w3-container w3-right">   
     <div class="w3-panel w3-border w3-border-light-green w3-rightbar w3-pale-yellow w3-text-dark-grey">
     <h4>Related on-line training</h4>

     <div class="w3-container w3-cell">
      <p>The Internet has a plethora of training opportunities, some even <i>free</i> worth considering. Check traditional on-line courses such as Coursera, Reed, as well as on-line courses from universities around the world (Stanford/US, MIT/US, Oxford/UK, etc).</p>
     </div>

     </div> <!-- larger div -->
    </div>
    <!-- 2nd -->

    <!-- 3rd -->
    <div class="w3-twothird w3-container">   
     <div class="w3-panel w3-border w3-border-light-blue w3-leftbar w3-pale-blue w3-text-dark-grey">
     <h4>Select one major area of interest ASAP!</h4>

     <div class="w3-container w3-cell">
      <p>Do you see yourself as (1) an <b>engineer</b>? (2) a <b>tester</b>? (3) a <b>manager</b>? (4) a <b>risk analyst</b>? (5) a <b>forensic analyst</b>? or (6) a <b>responder</b>?.</p>
     </div>

     </div> <!-- larger div -->
    </div>
    <!-- 3rd -->

    <!-- 4th -->
    <div class="w3-twothird w3-container w3-right">   
     <div class="w3-panel w3-border w3-border-deep-orange w3-rightbar w3-pale-red w3-text-dark-grey">
     <h4>Plan a <i>feasible</i> learning path for your career</h4>

     <div class="w3-container w3-cell">
      <p>There are several ways you can plan and prepare for your future.</p>
      <p>You should be able to devise a <i>feasible</i> plan to meet your prospects and realise your potential within cyber security.</p>
     </div>

     </div> <!-- larger div -->
    </div>
    <!-- 4th -->

    </div>

    <!-- ads -->
    <?php include('ads.php'); ?>
    <!-- end ads -->

    <!-- empty panel -->
    <div class="w3-panel w3-padding-64">
     <p></p>
    </div> 


  </div>

<!-- Footer -->
<?php include('footer.php'); ?>

<!-- END MAIN -->
</div>

<!-- Further Scripts -->
<?php include('scripts.php'); ?>

</body>
</html>









