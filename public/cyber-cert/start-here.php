<!DOCTYPE html>
<html lang="en">
<!-- Header -->
<?php include('header.php'); ?>
<body>

<!-- Navbar -->
<?php include('navbar.php'); ?>

<!-- Sidebar -->
<?php include('sidebar-main.php'); ?>

<!-- Overlay effect when opening sidebar on small screens -->
<div class="w3-overlay w3-hide-large" onclick="w3_close()" style="cursor:pointer" title="close side menu" id="myOverlay"></div>

<!-- Main content: shift it to the right by 250 pixels when the sidebar is visible -->
<div class="w3-main" style="margin-left:250px">

  <div class="w3-row w3-padding-64">
    <div class="w3-twothird w3-container">
      <h1 class="w3-text-purple">Start here</h1>
      <p>We outline here some major areas in the cyber security profession.</p>
      <p>You will notice that some areas are more <i>technical</i> whereas other areas are <i>managerial</i>.</p>

    <div class="w3-card-4">
     <div class="w3-display-container w3-text-white">
       <img src="images/cybersecurity1-banner.png" alt="cybersecurity" style="width:100%">
       <!-- Credit: FreePix.uk (CC BY-SA 3.0) https://i0.wp.com/www.cyberium.info/wp-content/uploads/2022/07/cybersecurity-1.jpg?w=1200&ssl=1-->
     </div>

    <!-- start of first line -->
    <div class="w3-row-padding">
     <div class="w3-col s4"> <!-- 1st col -->
      <div class="w3-panel w3-center w3-border w3-hover-border-purple">
         <h3 class="w3-text-purple">Engineer</h3>
         <p>Professionals taking a hands-on and practical approach to cyber security.</p>
      </div>
     </div>

     <div class="w3-col s4"> <!-- 2nd col -->
      <div class="w3-panel w3-center w3-border w3-hover-border-purple">
         <h3 class="w3-text-purple">Tester</h3>
         <p>How do we know what we have built is what we suppose to have done? It's about <i>validation</i>, in general.</p>
      </div>
     </div>
     
     <div class="w3-col s4"> <!-- 3rd col -->
      <div class="w3-panel w3-center w3-border w3-hover-border-purple">
         <h3 class="w3-text-purple">Manager</h3>
         <p>Oversighting cyber security delivery throughout organisations and aligning standards. Data and privacy concerns, regulations, and governance.</p>
      </div>
     </div>

    </div>
    <!-- end of first line -->

    <!-- start of second line -->
    <div class="w3-row-padding">
     <div class="w3-col s4"> <!-- 1st col -->
      <div class="w3-panel w3-center w3-border w3-hover-border-purple">
         <h3 class="w3-text-purple">Risk Analyst</h3>
         <p>Considering multiple threats facing organisations and devising plans for prevention, responses, and mitigation.</p>
      </div>
     </div>

     <div class="w3-col s4"> <!-- 2nd col -->
      <div class="w3-panel w3-center w3-border w3-hover-border-purple">
         <h3 class="w3-text-purple">Forensic Analyst</h3>
         <p>Investigating and collecting evidence of malicious presence or actions, analysing opportunities for breaching systems.</p>
      </div>
     </div>
     
     <div class="w3-col s4"> <!-- 3rd col -->
      <div class="w3-panel w3-center w3-border w3-hover-border-purple">
         <h3 class="w3-text-purple">Responder</h3>
         <p>Developing a hands-on skillset for fixing, mitigating, restarting, and restoring systems to healthy and consistent state.</p>
      </div>
     </div>

    </div>
    <!-- end of second line -->
    

   </div>

    </div>
    
    <!-- ads -->
    <?php include('ads.php'); ?>
    <!-- end ads -->
  </div>

  <div class="w3-row">
    <div class="w3-twothird w3-container">
      <h1 class="w3-text-purple">Job Titles in Cyber Security</h1>
       <p>As you might have thought, the required skills for each broad domain knowledge varies a lot! As soon as you decide, the better it will be to devise a clear path for further training and deciding on the best <i>learning path</i> aligned to your expectations.</p>
       
      <p>As stated, check the <a href="https://www.ukcybersecuritycouncil.org.uk/qualifications-and-careers/careers-route-map/" target="_blank">Cybersecurity Council's UK Career Route Map</a> for a wider range of positions.</p>

      <h2>Cyber security Engineer</h2>
      <p><b>Main positions:</b> Director of security, Security architect, Network security engineer, Security software developer, Security systems administrator, Technical director, Security analyst.</p>
      <p><b>Skills:</b> Critical thinking, IT networking, System administration, Risk assessment.</p>
      <p><b>Common certifications:</b>
      <ul class="w3-ul w3-border" style="width:60%">
       <li>[<b>comptia</b>] <a href="https://www.comptia.org/certifications/security" target="_blank">CompTIA Security+</a></li>
       <li class="w3-light-gray">[<b>(isc)2</b>] <a href="https://www.isc2.org/Certifications/SSCP" target="_blank">Systems Security Certified Practitioner (SSCP)</a></li>
       <li>[<b>(isc)2</b>] <a href="https://www.isc2.org/Certifications/CISSP" target="_blank">Certified Information Systems Security Professional (CISSP)</a></li>
       <li class="w3-light-gray">[<b>google</b>] <a href="https://cloud.google.com/certification/cloud-security-engineer" target="_blank">Google Professional Cloud Security Engineer</a></li>
       <li>[<b>(isc)2</b>] <a href="https://www.isc2.org/Certifications/CSSLP" target="_blank">Certified Secure Software Lifecycle Professional (CSSLP)</a></li>
       <li class="w3-light-gray">[<b>eccouncil</b>] <a href="https://www.eccouncil.org/programs/certified-network-security-course/" target="_blank">Certified Network Defender</a> (C|ND)</li>
      </ul>
       </p>

      <h2>Cyber security Tester</h2>
      <p><b>Main positions:</b> Penetration tester / Red teamer, Vulnerability researcher, Exploit developer, Ethical hacker ("white hat" hacker), Security research engineer, Internal, third-party, or external auditor.</p>
      <p><b>Skills:</b> Cryptography, Penetration testing and tools, Computer networking, Scripting (bash programming), Malware analysis tools.</p>
      <p><b>Common certifications:</b>
      <ul class="w3-ul w3-border w3-striped" style="width:60%">
       <li>[<b>eccouncil</b>] <a href="https://www.eccouncil.org/programs/certified-ethical-hacker-ceh/" target="_blank">Certified Ethical Hacker (C|EH)</a></li>
       <li class="w3-light-gray">[<b>comptia</b>] <a href="https://www.comptia.org/certifications/pentest" target="_blank">CompTIA PenTest+</a></li>
       <li>[<b>giac</b>] <a href="https://www.giac.org/certifications/penetration-tester-gpen/" target="_blank">GIAC Penetration Tester (GPEN)</a></li>
       <li class="w3-light-gray">[<b>offens. sec.</b>] <a href="https://www.offensive-security.com/pwk-oscp/" target="_blank">Offensive Security Certified Professional (OSCP)</a></li>
      </ul>
       </p>

      <h2>Cyber security Manager</h2>
      <p><b>Main positions:</b> Chief Information Security Officer (CISO).</p>
      <p><b>Skills:</b> Project management, Risk management, Leadership, Collaboration.</p>
      <p><b>Common certifications:</b>
      <ul class="w3-ul w3-border" style="width:60%">
       <li>[<b>isaca</b>] <a href="https://www.isaca.org/credentialing/cism" target="_blank">Certified Information Security Manager (CISM)</a></li>
       <li class="w3-light-gray">[<b>giac</b>] <a href="https://www.giac.org/certifications/certified-project-manager-gcpm/" target="_blank">GIAC Certified Project Manager (GCPM)</a></li>
       <li>[<b>(isc)2</b>] <a href="https://www.isc2.org/Certifications/CISSP" target="_blank">Certified Information Systems Security Professional (CISSP)</a></li>
      </ul>
      </p>

      <h2>Risk Analyst</h2>
      <p><b>Main positions:</b> Risk Analyst.</p>
      <p><b>Skills:</b> Risk management and assessment, Security architecture and strategy, data risk and governance, compliance over Personally Identifiable Information (PII).</p>
      <p><b>Common certifications:</b>
      <ul class="w3-ul w3-border" style="width:60%">
       <li>[<b>isaca</b>] <a href="https://www.isaca.org/credentialing/cisa" target="_blank">Certified Information Systems Auditor (CISA)</a></li>
       <li class="w3-light-gray">[<b>isaca</b>] <a href="https://www.isaca.org/credentialing/cism" target="_blank">Certified Information Security Manager (CISM)</a></li>
       <li>[<b>it-governance</b>] <a href="https://www.itgovernance.co.uk/crisc-certification" target="_blank">Certified in Risk and Information Systems Control (CRISC)</a></li>
       <li class="w3-light-gray">[<b>(isc)2</b>] <a href="https://www.isc2.org/Certifications/CISSP" target="_blank">Certified Information Systems Security Professional (CISSP)</a></li>
      </ul>
      </p>

      <h2>Forensic Analyst</h2>
      <p><b>Main positions:</b> Forensic Analyst.</p>
      <p><b>Skills:</b> Investigative skills over data and systems, application logs, indications of compromise, malware analysis tools, processing large datasets in search of maliciousness.</p>
      <p><b>Common certifications:</b>
      <ul class="w3-ul w3-border" style="width:60%">
       <li>[<b>iacis</b>] <a href="https://www.iacis.com/certification/cfce/" target="_blank">Certified Forensic Computer Examiner (CFCE)</a></li>
       <li class="w3-light-gray">[<b>cce</b>] Certified Computer Examiner (CCE)</li>
       <li>[<b>giac</b>] <a href="https://www.giac.org/certifications/certified-forensic-analyst-gcfa/" target="_blank">GIAC Certified Forensic Analyst (GCFA)</a></li>
       <li class="w3-light-gray">[<b>eccouncil</b>] <a href="https://www.eccouncil.org/academia/essentials/" target="_blank">Digital Forensics Essentials</a> (D|FE)</li>
       <li>[<b>eccouncil</b>] <a href="https://www.eccouncil.org/programs/computer-hacking-forensic-investigator-chfi/" target="_blank">Computer Hacking Forensic Investigator (CHFI)</a></li>
      </ul>
      </p>

      <h2>Cyber security Responder</h2>
      <p><b>Main positions:</b> IT forensics technician, Security operations center analyst, Forensic, intrusion, or malware analyst, Incident responder, Disaster recovery or business continuity manager.</p>
      <p><b>Skills:</b> Attention to detail, Technical writing and documentation, Intrusion detection tools, Forensics software, SIEM.</p>
      <p><b>Common certifications:</b>
      <ul class="w3-ul w3-border" style="width:60%">
       <li>[<b>giac</b>] <a href="https://www.giac.org/certifications/certified-incident-handler-gcih/" target="_blank">GIAC Certified Incident Handler (GCIH)</a></li>
       <li class="w3-light-gray">[<b>eccouncil</b>] <a href="https://www.eccouncil.org/programs/ec-council-certified-incident-handler-ecih/" target="_blank">EC-Council Certified Incident Handler (ECIH)</a></li>
       <li>[<b>cce</b>] Certified Computer Examiner (CCE)</li>
       <li class="w3-light-gray">[<b>iacis</b>] <a href="https://www.iacis.com/certification/cfce/" target="_blank">Certified Forensic Computer Examiner (CFCE)</a></li>
      </ul>
      </p>

    </div>

    <!-- empty panel -->
    <div class="w3-panel w3-padding-64">
     <p></p>
    </div> 


  </div>

<!-- Footer -->
<?php include('footer.php'); ?>

<!-- END MAIN -->
</div>

<!-- Further Scripts -->
<?php include('scripts.php'); ?>

</body>
</html>









