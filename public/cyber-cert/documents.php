<!DOCTYPE html>
<html lang="en">
<!-- Header -->
<?php include('header.php'); ?>
<body>

<!-- Navbar -->
<?php include('navbar.php'); ?>

<!-- Sidebar -->
<?php include('sidebar-main.php'); ?>

<!-- Overlay effect when opening sidebar on small screens -->
<div class="w3-overlay w3-hide-large" onclick="w3_close()" style="cursor:pointer" title="close side menu" id="myOverlay"></div>

<!-- Main content: shift it to the right by 250 pixels when the sidebar is visible -->
<div class="w3-main" style="margin-left:250px">

  <div class="w3-row w3-padding-64">
    <div class="w3-twothird w3-container">
      <h1 class="w3-text-purple">Documents</h1>
      <p>We highlight a set of standards and guidelines to get acquainted with in cyber security: </p>

    <div class="w3-card-4">
     <div class="w3-display-container w3-text-white">
       <img src="images/documents1-banner.png" alt="cybersecurity" style="width:100%">
       <!-- Credit: FreePix.uk (CC0 1.0) https://commons.wikimedia.org/wiki/File:Rainbow_series_documents.jpg -->
     </div>

    <div class="w3-row-padding">
     <div class="w3-col s4">
      <div class="w3-panel w3-center w3-border w3-hover-border-purple">
         <h3 class="w3-text-purple">Standards</h3>

      </div>
     </div>
     <div class="w3-col s4">
      <div class="w3-panel w3-center w3-border w3-hover-border-purple">
         <h3 class="w3-text-purple">Guidelines</h3>

      </div>
     </div>

     <div class="w3-col s4">
      <div class="w3-panel w3-center w3-border w3-hover-border-purple">
         <h3 class="w3-text-purple">Knowledge areas</h3>

      </div>
     </div>

    </div>

   </div>

    </div>

    <!-- ads -->
    <?php include('ads.php'); ?>
    <!-- end ads -->

  </div>

  <div class="w3-row">
    <div class="w3-twothird w3-container">
      <h3>Standards</h3>
      <ul class="w3-ul w3-border" style="width:70%">
       <li><a href="https://www.iso.org/isoiec-27001-information-security.html" target="_blank">ISO/IEC 27001</a> - Information security management</li>
       <li class="w3-light-gray"><a href="https://www.isa.org/intech-home/2018/september-october/departments/new-standard-specifies-security-capabilities-for-c" target="_blank">ISA/IEC 62443</a> standard specifies security capabilities for control system components.</li>
      </ul>

      <!-- empty panel -->
      <div class="w3-panel w3-padding-8">
       <p></p>
      </div> 

      <h3>Guidelines</h3>
      <ul class="w3-ul w3-border" style="width:70%">
       <li><a href="https://www.ncsc.gov.uk/collection/caf/cyber-assessment-framework" target="_blank">NCSC Cyber Assessment Framework</a> (NSCS CAF guidance)</li>
       <li class="w3-light-gray"><a href="https://www.nist.gov/cyberframework" target="_blank">NIST CSF</a> - NIST Cyber Security Framework</li>
       <li><a href="https://www.gov.uk/government/publications/cyber-primer" target="_blank">Cyber Primer</a> (UK Gov)</li>
        <ul><li>This publication explores the fundamentals, threats, operations and functions of cyber.</li></ul>
      </ul>
      
      <!-- empty panel -->
      <div class="w3-panel w3-padding-8">
       <p></p>
      </div> 

      <h3>Knowledge areas</h3>
      <ul class="w3-ul w3-border" style="width:70%">
       <li><a href="https://www.cybok.org" target="_blank">Cyber Security Body of Knowledge</a> (CyBOK)</li>
       <li class="w3-light-gray"><a href="https://www.ncsc.gov.uk/section/education-skills/cybok" target="_blank">CyBOK information</a> at NCSC/UK</li>
       <li>(ISC)2 <a href="https://www.isc2.org/Certifications/CBK" target="_blank">Common Body of Knowledge</a> (CBK) - CISSP</li>
      </ul>

      <!-- empty panel -->
      <div class="w3-panel w3-padding-8">
       <p></p>
      </div> 

      <h3>Miscellaneous</h3>
      <ul class="w3-ul w3-border" style="width:70%">
       <li><a href="http://attack.mitre.org/" target="_blank">Adversarial Tactics, Techniques & and Common Knowledge</a> (MITRE's ATT&CK&reg; framework)</li>
       <li class="w3-light-gray"><a href="https://oasis-open.github.io/cti-documentation/stix/intro.html" target="_blank">Structured Threat Information Expression</a> (STIX&trade;)</li>
      </ul>

      <!-- empty panel -->
      <div class="w3-panel w3-padding-8">
       <p></p>
      </div> 


    </div>
    
    <!-- empty panel -->
    <div class="w3-panel w3-padding-64">
     <p></p>
    </div> 


  </div>

<!-- Footer -->
<?php include('footer.php'); ?>

<!-- END MAIN -->
</div>

<!-- Further Scripts -->
<?php include('scripts.php'); ?>

</body>
</html>









