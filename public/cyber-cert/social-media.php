<!DOCTYPE html>
<html lang="en">
<!-- Header -->
<?php include('header.php'); ?>
<body>

<!-- Navbar -->
<?php include('navbar.php'); ?>

<!-- Sidebar -->
<?php include('sidebar-main.php'); ?>

<!-- Overlay effect when opening sidebar on small screens -->
<div class="w3-overlay w3-hide-large" onclick="w3_close()" style="cursor:pointer" title="close side menu" id="myOverlay"></div>

<!-- Main content: shift it to the right by 250 pixels when the sidebar is visible -->
<div class="w3-main" style="margin-left:250px">

  <div class="w3-row w3-padding-64">
    <div class="w3-twothird w3-container">
      <h1 class="w3-text-purple">Social media links</h1>
      <p>We list some interesting links in social media for you to follow and keep pace with the cyber security career: </p>

    <div class="w3-card-4">
     <div class="w3-display-container w3-text-white">
       <img src="images/social-media-banner.png" alt="social media" style="width:100%">
       <!-- Credit: StockSnap.io (CC0 1.0) https://stocksnap.io/photo/social-media-VKYWD8XTYJ -->
     </div>

    <div class="w3-row-padding">
     <div class="w3-col s4">
      <div class="w3-panel w3-center w3-border w3-hover-border-purple">
         <h3 class="w3-text-purple">LinkedIn</h3>

      </div>
     </div>
     <div class="w3-col s4">
      <div class="w3-panel w3-center w3-border w3-hover-border-purple">
         <h3 class="w3-text-purple">On-line Learning</h3>

      </div>
     </div>

     <div class="w3-col s4">
      <div class="w3-panel w3-center w3-border w3-hover-border-purple">
         <h3 class="w3-text-purple">Twitter</h3>

      </div>
     </div>

    </div>

   </div>


     <div class="w3-row">
      <div class="w3-panel w3-padding-8" style="width:65%">
         <h3>LinkedIn links</h3>

         <ul class="w3-ul w3-border">
          <li><b>Certification</b>
           <ul>
            <li><a href="https://www.linkedin.com/company/isc2/" target="_blank">linkedin.com/company/isc2/</a></li>
            <li><a href="https://www.linkedin.com/company/isaca/" target="_blank">linkedin.com/company/isaca/</a></li>
            <li><a href="https://www.linkedin.com/company/comptia/" target="_blank">linkedin.com/company/comptia/</a></li>
            <li><a href="https://www.linkedin.com/company/giac/" target="_blank">linkedin.com/company/giac/</a></li>
            <li><a href="https://www.linkedin.com/company/ec-council/" target="_blank">linkedin.com/company/ec-council/</a></li>
           </ul>
          </li>
          <li class="w3-light-gray"><b>Cyber security institutes</b>
           <ul>
            <li><a href="https://www.linkedin.com/company/national-cyber-security-centre/" target="_blank">linkedin.com/company/national-cyber-security-centre/</a></li>
            <li><a href="https://www.linkedin.com/company/sans-institute/" target="_blank">linkedin.com/company/sans-institute/</a></li>
            <li><a href="https://www.linkedin.com/company/ciisec/" target="_blank">linkedin.com/company/ciisec/</a></li>
            <li><a href="https://www.linkedin.com/company/it-bcs/" target="_blank">linkedin.com/company/it-bcs/</a></li>
            <li><a href="https://www.linkedin.com/company/mitre/" target="_blank">linkedin.com/company/mitre/</a></li>
           </ul>
          </li>
          <li><b>Other interesting links</b>
           <ul>
            <li><a href="https://www.linkedin.com/company/the-cyber-security-hub/" target="_blank">linkedin.com/company/the-cyber-security-hub/</a></li>
            <li><a href="https://www.linkedin.com/company/ethical-hackers-academy/" target="_blank">linkedin.com/company/ethical-hackers-academy/</a></li>
            <li><a href="https://www.linkedin.com/company/hackthebox/" target="_blank">linkedin.com/company/hackthebox/</a></li>
            <li><a href="https://www.linkedin.com/company/thehackernews/" target="_blank">linkedin.com/company/thehackernews/</a></li>
           </ul>
          </li>
         </ul>
         
         <h3>On-line Learning</h3>
         <ul class="w3-ul w3-border">
          <li><a href="https://edx.org" target="_blank">https://edx.org</a></li>
          <li class="w3-light-gray"><a href="https://coursera.org" target="_blank">https://coursera.org</a></li>
          <li><a href="https://youtube.com" target="_blank">https://youtube.com</a></li>
          <li class="w3-light-gray"><a href="https://udemy.com" target="_blank">https://udemy.com</a></li>
          <li><a href="https://udacity.com" target="_blank">https://udacity.com</a></li>
          <li class="w3-light-gray"><a href="https://skillshare.com" target="_blank">https://skillshare.com</a></li>
          <li><a href="https://teachable.com" target="_blank">https://teachable.com</a></li>
          <li class="w3-light-gray"><a href="https://codeschool.com" target="_blank">https://codeschool.com</a></li>
          <li><a href="https://codeacademy.com" target="_blank">https://codeacademy.com</a></li>
          <li class="w3-light-gray"><a href="https://linkedinlearning.com" target="_blank">https://linkedinlearning.com</a></li>
         </ul>
         
         <h3>Twitter</h3>
         <table class="w3-table w3-striped w3-border w3-bordered">
          <tr>
           <td>
            <ul class="w3-ul w3-border">
             <li><a href="https://twitter.com/CertifyGIAC" target="_blank">@CertifyGIAC</a></li>
             <li class="w3-light-gray"><a href="https://twitter.com/CompTIA" target="_blank">@CompTIA</a></li>
             <li><a href="https://twitter.com/TheHackersNews" target="_blank">@TheHackerNews</a></li>
             <li class="w3-light-gray"><a href="https://twitter.com/offsectraining" target="_blank">@offsectraining</a></li>
             <li><a href="https://twitter.com/ECCOUNCIL" target="_blank">@ECCOUNCIL</a></li>
            </ul>
           </td>
           <td>
            <ul class="w3-ul w3-border">
             <li class="w3-light-gray"><a href="https://twitter.com/kalilinux" target="_blank">@kalilinux</a></li>
             <li><a href="https://twitter.com/metasploit" target="_blank">@metasploit</a></li>
             <li class="w3-light-gray"><a href="https://twitter.com/SecurityTube" target="_blank">@SecurityTube</a></li>
             <li><a href="https://twitter.com/RealTryHackMe" target="_blank">@RealTryHackMe</a></li>
             <li class="w3-light-gray"><a href="https://twitter.com/hackinarticles" target="_blank">@hackinarticles</a></li>
            </ul>
           </td>
          </tr>
         </table>


      </div>
     </div>

    </div>

    <!-- ads -->
    <?php include('ads.php'); ?>
    <!-- end ads -->

    <!-- empty panel -->
    <div class="w3-panel w3-padding-64">
     <p></p>
    </div> 


  </div>

<!-- Footer -->
<?php include('footer.php'); ?>

<!-- END MAIN -->
</div>

<!-- Further Scripts -->
<?php include('scripts.php'); ?>

</body>
</html>









