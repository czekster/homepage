<!DOCTYPE html>
<html lang="en">
<!-- Header -->
<?php include('header.php'); ?>
<body>

<!-- Navbar -->
<?php include('navbar.php'); ?>

<!-- Sidebar -->
<?php include('sidebar-main.php'); ?>

<!-- Overlay effect when opening sidebar on small screens -->
<div class="w3-overlay w3-hide-large" onclick="w3_close()" style="cursor:pointer" title="close side menu" id="myOverlay"></div>

<!-- Main content: shift it to the right by 250 pixels when the sidebar is visible -->
<div class="w3-main" style="margin-left:250px">

  <div class="w3-row w3-padding-64">
    <div class="w3-twothird w3-container">
      <h1 class="w3-text-purple">Certifications</h1>
      <p>After choosing an area to focus your efforts, now it's time to decide how to proceed and check the pre-requirements for the chosen certification: </p>

    <div class="w3-card-4">
     <div class="w3-display-container w3-text-white">
       <img src="images/cybersec-generic-banner.png" alt="cybersecurity" style="width:100%">
       <!-- Credit: PGIPods (CC BY-SA 3.0) https://www.pgipods.com/tag/cybersecurity/page/2/ -->
     </div>

    <div class="w3-row-padding">
     <div class="w3-col s4">
      <div class="w3-panel w3-center w3-border w3-hover-border-purple">
         <h3 class="w3-text-purple">Roadmap</h3>

      </div>
     </div>

     <div class="w3-col s4">
      <div class="w3-panel w3-center w3-border w3-hover-border-purple">
         <h3 class="w3-text-purple">Entry-level</h3>

      </div>
     </div>

     <div class="w3-col s4">
      <div class="w3-panel w3-center w3-border w3-hover-border-purple">
         <h3 class="w3-text-purple">Learning paths</h3>

      </div>
     </div>

     <div class="w3-col s4">
      <div class="w3-panel w3-center w3-border w3-hover-border-purple">
         <h3 class="w3-text-purple">Pre-requirements</h3>

      </div>
     </div>

     <div class="w3-col s4">
      <div class="w3-panel w3-center w3-border w3-hover-border-purple">
         <h3 class="w3-text-purple">Knowledge & Skills</h3>

      </div>
     </div>

    </div>

   </div>

    </div>

    <!-- ads -->
    <?php include('ads.php'); ?>
    <!-- end ads -->

  </div>


  <div class="w3-row">
    <div class="w3-twothird w3-container">
      <h3>Roadmap</h3>
      <p>There are several tracks to choose from in certifications. We suggest looking at some roadmaps outlined by other resources first before deciding on your own study path.</p>

      <p>For instance, check out <a href="https://pauljerimy.com/security-certification-roadmap/" target="_blank">Paul Jerimy's</a> website for a cyber security certification roadmap.</p>
      
      <p>Additionally, look at <a href="https://www.sans.org/cyber-security-skills-roadmap/" target="_blank">SAN's Cyber Skills Roadmap</a>.</p>

      <!-- empty panel -->
      <div class="w3-panel w3-padding-8">
       <p></p>
      </div> 

      <h3>Entry level</h3>
      <p><b>CompTIA Security+</b> (read pre-requirements below) is considered one of the best entry-level certifications for cyber security.
         For this certification you will have to know attack and defence strategies, principles of security and policies, cryptographic standards, identifying network and host-based security related technologies, understanding wireless and remote access, business continuity and vulnerabilities.
      </p>
      <p>The <b>EC-Council Essentials</b> outline an indicative <a href="https://www.eccouncil.org/academia/essentials/" target="_blank">career pathway</a> from entry-level to CISO.</p>

      <!-- empty panel -->
      <div class="w3-panel w3-padding-8">
       <p></p>
      </div> 

      <h3>Learning paths</h3>
      <p>Some certification organisations offer training (paid) resources to candidates, for example, <b>(ISC)2</b> <a href="https://www.isc2.org/Training" target="_blank">has an Official Training web-site</a> and <a href="https://www.isc2.org/Training/Self-Study-Resources" target="_blank">Official Self-Study Resources</a>.</p>
      <p><b>GIAC</b> employs training from the <a href="https://www.sans.org/cyber-security-courses/" target="_blank">SANS institute</a>.</p>
      
      <p><b><a href="https://learning.acm.org/" target="_blank">ACM</a></b> provides certification related training (for members) through the <a href="https://acm.percipio.com/" target="_blank">Percipio</a> and <a href="https://app.pluralsight.com/library/" target="_blank">Pluralsight</a> (requires <i>login</i>) platforms.</p>
      
      <p>On-line courses such as <a href="https://www.coursera.org/search?query=cyber%20security&" target="_blank">Coursera</a>, <a href="https://www.udemy.com/courses/search/?src=ukw&q=cyber+security" target="_blank">Udemy</a>, and <a href="https://www.reed.co.uk/courses/cyber-security/online" target="_blank">Reed</a> have a good provision of topics for preparing for certification (look for more links <a href="social-media.php">on <i>On-line Learning</i> here</a>).</p>

      <!-- empty panel -->
      <div class="w3-panel w3-padding-8">
       <p></p>
      </div> 

      <h3>Pre-requirements</h3>
      <p>Some certifications require previous experience working in industry.
      Another example is <b>CISSP - (ISC)2</b> certification that requires candidates to have at least five years of paid, full-time experience in at least two of the eight (ISC)2 domains or four years paid, full-time experience in at least two of the eight (ISC)2 domains.</p>
      <p>For the <b>Certified Ethical Hacker (CEH)</b>, candidates should first complete a formal CEH training course offered by EC-Council. If you don't want to take the formal training, you must provide evidence of having two years experience in information security and prior educational background.</p>
      <p><b>CompTIA</b> <i>recommends</i> for candidates to complete the CompTIA A+ and CompTIA Network+ before taking the CompTIA Security+ exam, so it increases the chances of success.</p>

      <!-- empty panel -->
      <div class="w3-panel w3-padding-8">
       <p></p>
      </div> 

      <h3>Knowledge & Skills</h3>
      <p>You are expected to demonstrate your knowledge on a set of topics related to cyber security (the list below only show a set of topics in computing):
      </p>
      <ul class="w3-ul w3-border" style="width:75%">
       <li><b>Software Engineering</b>: design, implemementation, and testing systems; Software Development Life Cycle (SDLC); deployment (CD/CI); development methodologies and diagrams
        <ul>
         <li>programming languages: C/C++, Java, C#; scripting languages: Python, bash</li>
         <li>testing methods & techniques: black-box, white-box, gray-box, scenario and model based testing, validation procedures</li>
        </ul>
       </li>
       <li class="w3-light-gray"><b>Operating Systems</b>: processes, dynamic/static libraries, linking, compiling; file systems; GNU/Linux, MS-Windows, MacOS</li>
       <li><b>Cyber security specific</b>: threats, attacks, vulnerabilities in systems and assets; vulnerability life-cycle and catalogues/scores (CVE, NVD, CVSS); attacks (MITRE ATT&CK framework) and APTs;
        <ul>
         <li>viruses, worms, trojan horses, exploits, rootkits, software patches and updates</li>
         <li>DoS/DDoS attacks, phishing attacks, social engineering attacks, assets management</li>
         <li>firewalls, network intrusion detection/prevention systems</li>
         <li>access control, cryptography, hashing</li>
         <li>pentesting tools and GNU/Linux flavours (Kali Linux)</li>
         <li>cyber crime, policies, safety and privacy, GDPR and data breaches, governance</li>
        </ul>
       </li>
       <li class="w3-light-gray"><b>Management</b>: risk management, risk assessment, modelling and mapping risk, assets, likelihood, impact; assessing damage in the infrastructure; coordinate response teams; mitigation</li>
      </ul>

      <!-- empty panel -->
      <div class="w3-panel w3-padding-8">
       <p></p>
      </div> 


    </div>
    
    <!-- empty panel -->
    <div class="w3-panel w3-padding-64">
     <p></p>
    </div> 


  </div>

<!-- Footer -->
<?php include('footer.php'); ?>

<!-- END MAIN -->
</div>

<!-- Further Scripts -->
<?php include('scripts.php'); ?>

</body>
</html>









