<div class="w3-top">
  <div class="w3-bar w3-deep-purple w3-top w3-left-align w3-large">
    <a class="w3-bar-item w3-button w3-right w3-hide-large w3-hover-white w3-large w3-theme-l1" href="javascript:void(0)" onclick="w3_open()"><i class="fa fa-bars"></i></a>
    <a href="index.php" class="w3-bar-item w3-button w3-theme-l1"><i class="fa fa-home"></i></a>
    <a href="start-here.php" class="w3-bar-item w3-button w3-hide-small w3-hover-white">Start Here</a>
    <a href="careers.php" class="w3-bar-item w3-button w3-hide-small w3-hover-white">Careers</a>
    <a href="certifications.php" class="w3-bar-item w3-button w3-hide-small w3-hover-white">Certifications</a>
    <a href="documents.php" class="w3-bar-item w3-button w3-hide-small w3-hover-white">Documents</a>
    <a href="links.php" class="w3-bar-item w3-button w3-hide-small w3-hover-white">Links</a>
    <a href="social-media.php" class="w3-bar-item w3-button w3-hide-small w3-hover-white">Social Media</a>
  </div>
</div>
