<!DOCTYPE html>
<html lang="en">
<!-- Header -->
<?php include('header.php'); ?>
<body>

<!-- Navbar -->
<?php include('navbar.php'); ?>

<!-- Sidebar -->
<?php include('sidebar-main.php'); ?>

<!-- Overlay effect when opening sidebar on small screens -->
<div class="w3-overlay w3-hide-large" onclick="w3_close()" style="cursor:pointer" title="close side menu" id="myOverlay"></div>

<!-- Main content: shift it to the right by 250 pixels when the sidebar is visible -->
<div class="w3-main" style="margin-left:250px">

  <div class="w3-row w3-padding-64">
    <div class="w3-twothird w3-container">
      <h1 class="w3-text-purple">Links</h1>
      <p>Related links for consultation to expand and advance your knowledge in cyber security: </p>

    <!-- start of first line -->
    <div class="w3-row-padding">
     <div class="w3-col s4"> <!-- 1st col -->

       <div class="w3-card-4 w3-center">
        <a href="https://www.ncsc.gov.uk/" target="_blank"><img src="images/ncsc-logo.png" class="w3-padding-16" width="220" alt="NCSC"></a>
        <div class="w3-container w3-center">
          <p><a href="https://www.ncsc.gov.uk/" target="_blank">NCSC/UK</a></p>
        </div>
      </div> 

     </div>

     <div class="w3-col s4"> <!-- 2nd col -->

       <div class="w3-card-4 w3-center">
        <a href="https://www.enisa.europa.eu/" target="_blank"><img src="images/enisa-logo.png" class="w3-padding-16" width="120" alt="ENISA/EU"></a>
        <div class="w3-container w3-center">
          <p><a href="https://www.enisa.europa.eu/" target="_blank">ENISA/EU</a></p>
        </div>
      </div>
     </div>
     
     <div class="w3-col s4"> <!-- 3rd col -->

       <div class="w3-card-4 w3-center">
        <a href="https://www.cpni.gov.uk/" target="_blank"><img src="images/cpni-logo.png" class="w3-padding-16" width="180" alt="CPNI/UK"></a>
        <div class="w3-container w3-center">
          <p><a href="https://www.cpni.gov.uk/" target="_blank">CPNI/UK</a></p>
        </div>
      </div> 

     </div>

    </div>
    <!-- end of first line -->

    <!-- empty panel -->
    <div class="w3-panel w3-padding-8">
     <p></p>
    </div> 

    <!-- start of second line -->
    <div class="w3-row-padding">
     <div class="w3-col s4"> <!-- 1st col -->

       <div class="w3-card-4 w3-center">
        <a href="https://www.nist.gov/cybersecurity" target="_blank"><img src="images/nist-logo.png" class="w3-padding-16" width="220" alt="NIST"></a>
        <div class="w3-container w3-center">
          <p><a href="https://www.nist.gov/cybersecurity" target="_blank">NIST/US</a><br>(cyber security)</p>
        </div>
      </div> 

     </div>

     <div class="w3-col s4"> <!-- 2nd col -->

       <div class="w3-card-4 w3-center">
        <a href="https://www.cisa.gov/cybersecurity" target="_blank"><img src="images/cisa-logo2.png" class="w3-padding-16" width="200" alt="CISA/US"></a>
        <div class="w3-container w3-center">
          <p><a href="https://www.cisa.gov/cybersecurity" target="_blank">CISA/US</a></p>
        </div>
      </div>
     </div>
     
     <div class="w3-col s4"> <!-- 3rd col -->

       <div class="w3-card-4 w3-center">
        <a href="https://www.sans.org/uk/" target="_blank"><img src="images/sans-logo.png" class="w3-padding-16" width="150" alt="SANS/Intl"></a>
        <div class="w3-container w3-center">
          <p><a href="https://www.sans.org/uk/" target="_blank">SANS/Intl.</a></p>
        </div>
      </div> 

     </div>

    </div>
    <!-- end of second line -->

    <!-- empty panel -->
    <div class="w3-panel w3-padding-8">
     <p></p>
    </div> 

    <!-- start of third line -->
    <div class="w3-row-padding">
     <div class="w3-col s4"> <!-- 1st col -->

       <div class="w3-card-4 w3-center">
        <a href="https://www.isaca.org/" target="_blank"><img src="images/isaca-logo.png" class="w3-padding-16" width="220" alt="ISACA"></a>
        <div class="w3-container w3-center">
          <p><a href="https://www.isaca.org/" target="_blank">ISACA</a> - Information Systems Audit and Control Association</p>
        </div>
      </div> 

     </div>

     <div class="w3-col s4"> <!-- 2nd col -->

       <div class="w3-card-4 w3-center">
        <a href="https://www.mitre.org/focus-areas/cybersecurity" target="_blank"><img src="images/mitre-logo.png" class="w3-padding-16" width="180" alt="MITRE/US"></a>
        <div class="w3-container w3-center">
          <p><a href="https://www.mitre.org/focus-areas/cybersecurity" target="_blank">MITRE/US</a> <br>(focus area: cyber security)</p>
        </div>
      </div>
     </div>
     
     <div class="w3-col s4"> <!-- 3rd col -->

       <div class="w3-card-4 w3-center">
        <a href="https://www.ciisec.org/" target="_blank"><img src="images/ciis-logo.png" class="w3-padding-16" width="240" alt="CIIS"></a>
        <div class="w3-container w3-center">
          <p><a href="https://www.ciisec.org/" target="_blank">Chartered Institute of Information Security/UK</a></p>
        </div>
      </div> 

     </div>

    </div>
    <!-- end of third line -->

    </div>

    <!-- ads -->
    <?php include('ads.php'); ?>
    <!-- end ads -->
    
    <!-- empty panel -->
    <div class="w3-panel w3-padding-8">
     <p></p>
    </div>

  <div class="w3-row">
   <div class="w3-panel w3-padding-8" style="width:60%">
      <h3>Choosing a Professional Society</h3>

     <table class="w3-table w3-striped w3-border w3-bordered">
      <tr valign="center">
        <td class="w3-border w3-center"><br><a href="https://learning.acm.org/" target="_blank"><img src="images/acm-logo.png" width="240"></a></td>
        <td valign="center" class="w3-border">
         <ul class="w3-ul">
          <li><a href="https://learning.acm.org/" target="_blank">Association for Computing Machinery</a> - (ACM/US)
           <ul><li>They offer platforms for advancing your career and preparing for certifications (e.g., PluralSight, Percipio) - you will need to <a href="https://www.acm.org/membership/join" target="_blank">Join ACM</a></li></ul>
          </li>
         </ul>
        </td>
      </tr>
      <tr valign="center">
        <td class="w3-border w3-center"><br><br><a href="https://www.computer.org/" target="_blank"><img src="images/ieee-cs-logo.png" width="230"></a></td>
        <td valign="center" class="w3-border">
         <ul class="w3-ul">
          <li class="w3-light-gray"><a href="https://www.computer.org/" target="_blank">IEEE Computer Society</a> - (IEEE-CS/US)</li>
           <ul>
            <li>Check the <a href="https://www.ieee.org/membership-catalog/index.html?srchProdType=Societies&searchType=prodType" target="_blank">catalog of societies</a> for IEEE</li>
            <li>This society offer its own <a href="https://www.computer.org/education/certifications" target="_blank">certifications</a> (mostly about software engineering)</li>
           </ul>
         </ul>
        </td>
      </tr>
      <tr valign="center">
        <td class="w3-border w3-center"><a href="https://www.bcs.org/" target="_blank"><img src="images/bcs-logo.png" width="100"></a></td>
        <td class="w3-border">
         <ul class="w3-ul">
          <li><a href="https://www.bcs.org/" target="_blank">BCS, The Chartered Institute for IT</a> (known as British Computer Society until 2009) - check <a href="https://www.bcs.org/membership-and-registrations/become-a-member/" target="_blank">information for joining</a>
           <ul><li>The institute has a comprehensive <a href="https://www.bcs.org/deliver-and-teach-qualifications/training-providers-and-adult-education-centres/deliver-our-professional-certifications/" target="_blank">certification portfolio</a> (for the general area of Computer Science and TI).
          </li>
         </ul>
        </td>
      </tr>
     </table>


   </div>
  </div>
 
  
  <div class="w3-row">
   <div class="w3-panel w3-padding-8" style="width:45%">
      <h3>Taking your career to the next level (research)</h3>

      <ul class="w3-ul w3-border">
       <li><a href="https://www.ncsc.gov.uk/information/academic-centres-excellence-cyber-security-research" target="_blank">Academic Centres of Excellence in Cyber Security Research</a> - ACEs-CSR (NCSC)</li>
        <ul><li>Check the Centres for Doctoral Training (CDT) in cyber security</li></ul>
      </ul>

   </div>
  </div>

  <div class="w3-row">
    <div class="w3-panel w3-padding-8">
      <h3>Other interesting links</h3>
      <p>Check out <a href="https://hackr.io/blog/best-cybersecurity-certification" target="_blank">Hack.io's Best Cybersecurity Certifications</a> to Boost Your Career.</p>
    </div>

    <div class="w3-panel w3-padding-16" style="width:35%">
      <h3>Ethical Hacking platforms</h3>

      <ul class="w3-ul w3-border">
        <li class="w3-sand"><h5>Platforms to Practice Ethical Hacking and CTF <br>(some have paid content):</h5></li>
        <li><a href="https://tryhackme.com/" target="_blank">tryhackme</a></li>
        <li class="w3-light-gray"><a href="https://www.hackthebox.com/" target="_blank">hacktheboxv</a></li>
        <li><a href="https://www.pentesterlab.com/" target="_blank">pentester lab</a></li>
        <li class="w3-light-gray"><a href="https://academy.tcm-sec.com/p/practical-ethical-hacking-the-complete-course" target="_blank">tcm-security</a></li>
        <li><a href="https://www.vulnhub.com/" target="_blank">Vulnhub</a></li>
        <li class="w3-light-gray"><a href="https://www.offensive-security.com/labs/individual/" target="_blank">Offensive Security</a></li>
        <li><a href="https://vulnmachines.com/" target="_blank">Vulnmachines</a></li>
        <li class="w3-light-gray"><a href="https://portswigger.net/web-security" target="_blank">Portswigger Web Security Academy</a></li>
        <li><a href="https://www.hacker101.com/" target="_blank">Hacker101</a></li>
        <li class="w3-light-gray"><a href="https://picoctf.com/" target="_blank">PicoCTF</a></li>
        <li><a href="https://hackmyvm.eu/" target="_blank">HackMyVm</a></li>
        <li class="w3-light-gray"><a href="https://www.cybrary.it/course/ethical-hacking/" target="_blank">Cybrary</a></li>
        <li><a href="https://www.rangeforce.com/" target="_blank">RangeForce</a></li>
        <li class="w3-light-gray"><a href="https://letsdefend.io/" target="_blank">Letsdefend</a></li>
        <li><a href="https://www.virtualhackinglabs.com/" target="_blank">vhackinglabs</a></li>
        <li class="w3-light-gray"><a href="https://www.bugbountyhunter.com/training/" target="_blank">BugBountyHunt3r</a></li>
        <li><a href="https://ctftime.org/" target="_blank">CTFTime</a></li>
        <li class="w3-light-gray"><a href="https://247ctf.com/" target="_blank">247CTF</a></li>
      </ul>


    </div>

    <!-- empty panel -->
    <div class="w3-panel w3-padding-64">
     <p></p>
    </div> 

  </div>

  </div>

<!-- Footer -->
<?php include('footer.php'); ?>

<!-- END MAIN -->
</div>

<!-- Further Scripts -->
<?php include('scripts.php'); ?>

</body>
</html>









