<!DOCTYPE html>
<html lang="en">
<!-- Header -->
<?php include('header.php'); ?>
<body>

<!-- Navbar -->
<?php include('navbar.php'); ?>

<!-- Sidebar -->
<?php include('sidebar-main.php'); ?>

<!-- Overlay effect when opening sidebar on small screens -->
<div class="w3-overlay w3-hide-large" onclick="w3_close()" style="cursor:pointer" title="close side menu" id="myOverlay"></div>

<!-- Main content: shift it to the right by 250 pixels when the sidebar is visible -->
<div class="w3-main" style="margin-left:250px">

  <div class="w3-row w3-padding-64">
    <div class="w3-twothird w3-container">
      <h1 class="w3-text-purple">Cyber Security Certifications and Career Routes</h1>
      <p>After (or during) your undergraduate studies, you may advance your knowledge and skills in Cyber Security by <b>adding certifications</b> to your portfolio.</p>

      <p>They substantiate and complement your toolset for tackling Cyber Security related problems in organisations, and give you a significant <b>competitive advantage</b> in your career.</p>

      <p>There is a myriad of certifications and career routes to consider, from entry-level to advanced and vendor-specific, it all depends on your <b>future aspirations</b>.</p>

     <div class="w3-panel w3-border w3-leftbar w3-light-gray w3-text-deep-purple" style="width:85%">
      <h3><i>"I don't know <b>where</b> to begin!"</i></h3>

      <div class="w3-container w3-cell">
      <p>If you are <i>completely lost</i> we suggest to start looking at the possible careers within Cyber Security at the <a href="https://www.ukcybersecuritycouncil.org.uk/qualifications-and-careers/careers-route-map/" target="_blank">Cybersecurity Council's UK Career Route Map</a>.</p>
      <p>If you are uncertain about the major areas of cyber security to choose from, you could check our <a href="start-here.php"><b>Start Here</b></a> subsection.</p>
      </div>
     
      <div class="w3-container w3-cell">
       <p><a href="https://www.ukcybersecuritycouncil.org.uk/qualifications-and-careers/careers-route-map/" target="_blank"><img src="images/uk-cyber-sec-council.png" width="150"></a></p>
      </div>

     </div> <!-- larger div -->

    </div>

    <!-- ads -->
    <?php include('ads.php'); ?>
    <!-- end ads -->

  </div>

  <div class="w3-row">
    <div class="w3-twothird w3-container">
      <a name="basic"><br><br>
      <h2 class="w3-text-purple">Basic information - UK institutes</h2>
      </a>
      <p>You could start looking at the National Cyber Security Centre's <a href="https://www.ncsc.gov.uk/information/certified-training" target="_blank">NCSC Certified Training</a>, a certification process carried out by <a href="https://apmg-international.com/" target="_blank">APMG</a>, an independent certification body accredited by the United Kingdom Accreditation Service (UKAS).
      <div class="w3-panel w3-right w3-padding-16">
      <a href="https://www.ncsc.gov.uk/" target="_blank"><img src="images/ncsc-logo.png" width="250" align="right"></a>
      </div>
      </p>
      
      <p>Access NCSC's <a href="" target="_blank">Certified Cyber Professional (CCP) assured service</a> for learning benefits and how to achieve recognition in the profession.</p>
      
      <p>Another source to access is the <a href="https://nationalcareers.service.gov.uk/search-results?SearchTerm=cyber+security" target="_blank">National Careers Service</a> (for cyber security) and, more broadly, the <a href="https://nationalcareers.service.gov.uk/careers-advice" target="_blank">Career's Advice</a>.
      </p>
      
      <p>Finally, check the <a href="https://www.gov.uk/government/cyber-security" target="_blank">gov.uk site about cyber security</a>.</p>

    </div>

    <div class="w3-twothird w3-container">   
     <div class="w3-panel w3-border w3-border-orange w3-leftbar w3-sand w3-text-dark-grey" style="width:85%">
     <h4>Tip</h4>

     <div class="w3-container w3-cell">
      <p>Always thrive to find <b>accredited certification institutes</b> in the UK or around the globe.</p>
      <p>We list here several companies, however, feel free to conduct your own <b>due diligence</b> on this matter.</p>
     </div>

     </div> <!-- larger div -->
    </div>


    <!-- empty panel -->
    <div class="w3-panel w3-padding-16">
     <p></p>
    </div>

    <div class="w3-twothird w3-container">
      <a name="entry-level"><br><br>
      <h2 class="w3-text-purple">Entry-Level Certifications</h2>
      </a>

       <!-- start of first line -->
       <div class="w3-row-padding">

        <div class="w3-col s4"> <!-- 1st col -->

          <div class="w3-card-4 w3-center">
           <a href="https://www.comptia.org/certifications/cybersecurity-analyst" target="_blank"><img src="images/comptia-logo.png" class="w3-padding-16" width="200" alt="GIAC"></a>
           <div class="w3-container w3-center">
             <p><a href="https://www.comptia.org/certifications/cybersecurity-analyst" target="_blank">CompTIA Cybersecurity Analyst</a> - CySA+</p>
           </div>
         </div>
        </div>

        <div class="w3-col s4"> <!-- 2nd col -->

          <div class="w3-card-4 w3-center">
           <a href="https://www.isc2.org/Certifications/CC" target="_blank"><img src="images/isc2-logo.png" class="w3-padding-16" width="230" alt="ISC_2"></a>
           <div class="w3-container w3-center">
             <p><a href="https://www.isc2.org/Certifications/CC" target="_blank">ISC2 - Certified in Cybersecurity </a> - (CC)</p>
           </div>
         </div> 

        </div>

        <div class="w3-col s4"> <!-- 3rd col -->

          <div class="w3-card-4 w3-center">
           <a href="https://www.isaca.org/credentialing/cism" target="_blank"><img src="images/cism-logo.png" class="w3-padding-16" width="230" alt="CISM"></a>
           <div class="w3-container w3-center">
             <p><a href="https://www.isaca.org/credentialing/cism" target="_blank">CISM</a> - Certified Information Security Manager (<a href="https://www.isaca.org/" target="_blank">ISACA</a>)</p>
           </div>
         </div> 

        </div>


       </div>
       <!-- end of first line -->

    <!-- empty panel -->
    <div class="w3-panel w3-padding-8">
     <p></p>
    </div>

       <!-- start of second line -->
       <div class="w3-row-padding">

        <div class="w3-col s4"> <!-- 1st col -->

          <div class="w3-card-4 w3-center">
           <a href="https://www.giac.org/" target="_blank"><img src="images/giac-logo.png" class="w3-padding-16" width="200" alt="GIAC"></a>
           <div class="w3-container w3-center">
             <p><a href="https://www.giac.org/" target="_blank">GIAC Certifications</a> - Global Information Assurance Certification</p>
           </div>
         </div>
        </div>

        <div class="w3-col s4"> <!-- 2nd col -->

          <div class="w3-card-4 w3-center">
           <a href="https://learn.microsoft.com/en-us/certifications/mta-security-fundamentals/" target="_blank"><img src="images/ms-mta-logo.png" class="w3-padding-16" width="170" alt="Certified Ethical Hacker"></a>
           <div class="w3-container w3-center">
             <p><a href="https://learn.microsoft.com/en-us/certifications/mta-security-fundamentals/" target="_blank">Microsoft MTA</a> - Security Fundamentals</p>
           </div>
         </div>
        </div>

        <div class="w3-col s4"> <!-- 3rd col -->

          <div class="w3-card-4 w3-center">
           <a href="https://www.eccouncil.org/programs/certified-ethical-hacker-ceh/" target="_blank"><img src="images/ec-council-logo.png" class="w3-padding-16" width="230" alt="EC-Council Essentials"></a>
           <div class="w3-container w3-center">
             <p><a href="https://www.eccouncil.org/academia/essentials/" target="_blank">EC Council Essentials</a></p>
           </div>
         </div>
        </div>


       </div>
       <!-- end of second line -->

    <!-- empty panel -->
    <div class="w3-panel w3-padding-8">
     <p></p>
    </div>

       <!-- start of third line -->
       <div class="w3-row-padding">

        <div class="w3-col s4"> <!-- 1st col -->

          <div class="w3-card-4 w3-center">
           <a href="https://www.offensive-security.com/" target="_blank"><img src="images/offensive-security-logo.png" class="w3-padding-16" width="240" alt="Offensive security"></a>
           <div class="w3-container w3-center">
             <p><a href="https://www.offensive-security.com/" target="_blank">Offensive security</a> - several choices</p>
           </div>
         </div>
        </div>

        <!-- more columns will go here (2 more) -->


       </div>
       <!-- end of third line -->
       
    </div> <!-- end of 'entry-level' -->

    <!-- empty panel -->
    <div class="w3-panel w3-padding-16">
     <p></p>
    </div> 


    <div class="w3-twothird w3-container">
      <a name="costs"><br><br>
      <h2 class="w3-text-purple">Cost estimations (as of October/2022)</h2>
      </a>
      <ul class="w3-ul w3-border" style="width:75%">
       <li><b>(ISC)2</b>
        <ul>
         <li>Certified Information Systems Security Professional (CISSP): $749</li>
         <li>Security Administrator Certification: $249</li>
         <li>Premier Cybersecurity Certification: $599</li>
        </ul>
       </li>
       <li class="w3-light-gray"><b>ISACA</b>
        <ul>
         <li>Certified Information Systems Auditor (CISA) or Certified Information Security Manager (CISM): $575 for members, $760 for non-members</li>
        </ul>
       </li>
       <li><b>MTA: Security Fundamentals (Microsoft): </b> $300</li>
       <li class="w3-light-gray"><b>CompTIA Security+:</b> 
        <ul>
         <li>Cybersecurity Certification: $949</li>
         <li>Advanced-Level Cybersecurity Certification: $480</li>
        </ul>
       </li>
       <li><b>Certified Ethical Hacker (CEH):</b> $950 to $1,199, depending on testing location</li>
       <li class="w3-light-gray"><b>Offensive Security</b>: it varies (from $799 to $2499 or more), check their <a href="https://www.offensive-security.com/pricing/" target="_blank">pricing website</a></li>
       <li><b>GIAC</b>
        <ul>
         <li>Security Essentials Certification: $949</li>
         <li>Incident Handler Certification: $949</li>
        </ul>
       </li>
<!--
       <li class="w3-light-gray"></li>
       <li></li>
       <li class="w3-light-gray"></li>
-->
      </ul>
      <p>Note that some certifications <b>ask for pre-requirements</b> such as prior experience working in industry or knowledge in some area. Check those pre-requirements for each certification you are planning.</p>
    </div>

    <!-- empty panel -->
    <div class="w3-panel w3-padding-8">
     <p></p>
    </div> 

    <div class="w3-twothird w3-container">
      <a name="hot-careers"><br><br>
      <h2 class="w3-text-purple">Hot careers</h2>
      </a>
      <ul>
       <li><b>Cyber security researcher:</b> apply novel and hot topics in Computer Science such as Artificial Intelligence & Machine Learning (AI/ML), and Data Science applied to protecting systems and discovering intrusions</li>
       <li><b>Bug hunter:</b> discover bugs in applications (and getting paid to find them!), finding defects in APIs or libraries</li>
       <li><b>Cyber Threat Intelligence</b> (CTI) related</li>
       <ul>
        <li><b>Threat hunter:</b> use investigative skills to determine (or rule out) the presence of APTs in your networks and systems (threat agents lurking within systems)</li>
        <li><b>Threat Intellicence officer:</b> work with Cyber Threat Intelligence to treat internal and external feeds to realise potentials attacks or learn new vectors</li>
       </ul>
       <li><b>Pentester:</b> stress and subject applications to intense loads (as in stress testing) or finding external vulnerabilities (treating systems as black-boxes)</li>
      </ul>

    </div>
    
    <!-- empty panel -->
    <div class="w3-panel w3-padding-64">
     <p></p>
    </div>
    
    <div class="w3-twothird w3-container">
     <!-- Credit: Flickr Computer Security (BY-SA 2.0) https://www.flickr.com/photos/111692634@N04/15327725543 -->
     <div class="w3-display-container w3-text-white w3-center">
       <img src="images/padlock1-banner.png" alt="digital padlock" style="width:100%">
     </div>
    </div>

    <!-- empty panel -->
    <div class="w3-panel w3-padding-64">
     <p></p>
    </div> 
    <!-- empty panel -->
    <div class="w3-panel w3-padding-64">
     <p></p>
    </div> 


  </div>



<!-- Footer -->
<?php include('footer.php'); ?>

<!-- END MAIN -->
</div>

<!-- Further Scripts -->
<?php include('scripts.php'); ?>

</body>
</html>









