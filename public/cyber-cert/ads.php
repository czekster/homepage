<?php
$num_ads = rand(0,4);
$ads[0] = '<a href="https://www.aston.ac.uk/study/courses/cybersecurity-bsc" target="_blank"><img src="images/aston-logo.png" width="100%"><br>Cybersecurity BSc at Aston University</a>';
$ads[1] = '<img src="images/twitter-logo.png" width="10%"><a href="https://twitter.com/hashtag/CyberSecurity?src=hashtag_click" target="_blank">Twitter #cybersecurity</a>';
$ads[2] = '<a href="https://www.ukcybersecuritycouncil.org.uk/qualifications-and-careers/careers-route-map/" target="_blank"><img src="images/uk-cyber-sec-council.png" width="50%"></a>';
$ads[3] = '<a href="https://www.ncsc.gov.uk/" target="_blank"><img src="images/ncsc-logo.png" width="85%" align="center"></a>';
$ads[4] = 'Check out our <a href="social-media.php">Social Media</a> links';
#add more ads next
#$ads[5] = '';
$rand_keys = array_rand($ads, 2); # this function will get any two ads from the array
?>

    <div class="w3-third w3-container">
      <p class="w3-border w3-padding-large w3-padding-32 w3-center"><?php echo($ads[$rand_keys[0]]); ?></p>
      <p class="w3-border w3-padding-large w3-padding-32 w3-center"><?php echo($ads[$rand_keys[1]]); ?></p>
    </div>
