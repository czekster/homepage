<nav class="w3-sidebar w3-bar-block w3-collapse w3-large w3-theme-l5 w3-animate-left" id="mySidebar">
  <a href="javascript:void(0)" onclick="w3_close()" class="w3-right w3-xlarge w3-padding-large w3-hover-black w3-hide-large" title="Close Menu">
    <i class="fa fa-remove"></i>
  </a>
  <h4 class="w3-bar-item"><b>Menu</b></h4>
  <a class="w3-bar-item w3-button w3-hover-deep-purple" href="index.php">Home</a>
  <a class="w3-bar-item w3-button w3-hover-deep-purple" href="index.php#basic">Basic information</a>
  <a class="w3-bar-item w3-button w3-hover-deep-purple" href="index.php#entry-level">Entry level</a>
  <a class="w3-bar-item w3-button w3-hover-deep-purple" href="index.php#costs">Costs</a>
  <a class="w3-bar-item w3-button w3-hover-deep-purple" href="index.php#hot-careers">Hot careers</a>
  <hr class="style-one">
  <a class="w3-bar-item w3-button w3-hover-deep-purple" href="quick-tips.php">Quick tips</a>
</nav>
