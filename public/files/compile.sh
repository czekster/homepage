#!/bin/sh

latex $1.tex
bibtex $1
latex $1.tex
bibtex $1
latex $1.tex
bibtex $1
dvips $1.dvi
ps2pdf $1.ps