#!/bin/sh

rm *.aux -f
rm *.bbl -f
rm *.backup -f
rm *.bak -f
rm *.???~ -f
rm *.log -f
rm *.dvi -f
rm *.ps -f
rm *.pdf -f
rm *.toc -f
rm *.lot -f
rm *.lof -f
rm *.loa -f
rm *.blg -f


