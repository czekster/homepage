#!/usr/bin/perl

# This script will take a file with many runs and execute the confidence intervals.
# author: rmelo
# last modification: 14/01/2007

use strict;
use warnings;
#use Number::Format qw(:subs :vars);
#$THOUSANDS_SEP   = '.';
#$DECIMAL_POINT   = ',';
#$INT_CURR_SYMBOL = 'DEM';

if (@ARGV != 2) { 
   print "missing FILE parameter. \nusage: perl intervals.pl FILE RUNS\n";
   print "FILE = file to be open\nRUNS = number of executions, e.g. 50\n";
   exit;
}

my $file = $ARGV[0];
my $N = $ARGV[1];
my $line;

open(INFILE, "<$file") or die("cannot open $file");
my(@lines) = <INFILE>;
close(INFILE);
my $cont = 0;
foreach $line (@lines) {
   $cont += $line*1;
}

my $aux = 0;
my $idx = 0;
my $mean = $cont/$N;
foreach $line (@lines) {
   $line =~ s/\n//g;
   my $time = $line*1;
   $aux += ($time-$mean)**2;
}
my $sd = sqrt($aux/($N-1));
my $ci = (1.96 * $sd) / sqrt($N); #95% of confidence
my $mean_1 = $mean-$ci;
#$mean_1 = format_number($mean_1, 4, 1);
my $mean_2 = $mean+$ci;
#$mean_2 = format_number($mean_2, 4, 1);
#$mean = format_number($mean, 4, 1);
#$ci = format_number($ci, 4, 1);
print "".($mean_1)."\t".($mean)."\t".($mean_2)."\t\t".($mean)."+-".$ci." with 95% of confidence\n";



