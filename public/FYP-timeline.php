<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="generator" content="GitLab Pages">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Ricardo M. Czekster's homepage</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="syntax.css">
	<link rel="stylesheet" href="main.css">
    <link rel="stylesheet" href="style.css">
  </head>

<body class="vsc-initialized">
  <div class="wrapper">
	<div class="columns">
<?php
include("sidebar.php");
?>
	  <div class="main">
		<h1>FYP timeline</h1>

<p>You need <b>motivation</b>.</p>

<img src="images/motivation.png"><br>

<p>False. You need a <b>deadline</b>.</p>

<p>In the meantime, I'll provide a <b><font color="#aa1111">timeline</font></b> for handling <b>your</b> FYP. </p>


<hr width="100%" align="left" color="#91A3AB">

<p>Note that activities herein are not <i>written in stone</i>, ie, it will certainly change as you go.</p>

<a name="top"></a>
<h2>Tentative timeline</h2>
&bull;&nbsp;<a href="#late-sep-mid-oct">Late September to Mid-October</a><br/>
&bull;&nbsp;<a href="#mid-oct-end-oct">Mid-October to End-October</a><br/>
&bull;&nbsp;<a href="#november">November</a><br/>
&bull;&nbsp;<a href="#december">December</a><br/>
&bull;&nbsp;<a href="#january">January</a><br/>
&bull;&nbsp;<a href="#february">February</a><br/>
&bull;&nbsp;<a href="#march">March</a><br/>
&bull;&nbsp;<a href="#april">April</a><br/>

<h2>Overview of tasks/milestones</h2>
<table border="1" width="85%" cellspacing="1">
 <tr align="middle">
  <td width="7%" colspan="2" bgcolor="#aa2211">Sep</td>
  <td width="14%" colspan="2" bgcolor="#bb3311">Oct</td>
  <td width="7%" bgcolor="#cc4411">Nov</td>
  <td width="14%" colspan="2" bgcolor="#dd5511">Dec</td>
  <td width="7%" bgcolor="#ee6611">Jan</td>
  <td width="7%" bgcolor="#ff7711">Feb</td>
  <td width="7%" bgcolor="#ff9911">Mar</td>
  <td width="14%" colspan="2" bgcolor="#ffAA11">Apr</td>
 </tr>
 <tr>
  <td width="7%">&nbsp;</td>
  <td width="7%" bgcolor="#ADD8E6">#1</td>
  <td width="7%"><font size="-3">thinking</font></td>
  <td width="7%" bgcolor="#ADD8E6">#2</td>
  <td width="7%"><font size="-3">planning<br>tools?</font></td>
  <td width="7%"><font size="-3">planning<br>coding?</font></td>
  <td width="7%" bgcolor="#ADD8E6">#3</td>
  <td width="7%"><font size="-3">coding<br>testing</font></td>
  <td width="7%"><font size="-3">coding<br>testing</font></td>
  <td width="7%"><font size="-3">writing<br>prepping</font></td>
  <td width="7%" bgcolor="#ADD8E6">#4</td>
  <td width="7%" bgcolor="#ADD8E6">#5</td>
 </tr>
</table>

&bull;&nbsp;<b>#1</b> Kickstart meeting<br>
&bull;&nbsp;<b>#2</b> Submit Project Definition Form in Blackboard<br>
&bull;&nbsp;<b>#3</b> Submit Partial Report (Term Report)<br>
&bull;&nbsp;<b>#4</b> Submit Final Report<br>
&bull;&nbsp;<b>#5</b> Present <b>Demo</b><br>
<table border="0" cellpadding="3" cellspacing="3">
 <tr>
  <td rowspan="2" width="1%" bgcolor="#AABBAA">&nbsp;</td>
  <td style = "border: 1px dotted red;" bgcolor="#eeeeee"><font color="#cc0000">&rArr;</font>&nbsp;<font color="#111111">Check Ethics submission deadlines (it varies each year)</font></td>
 </tr>
 <tr>
  <td style = "border: 1px dotted red;" bgcolor="#eeffee">
&rarr;&nbsp;Are you generating (primary) or using (secondary) a dataset that contain any human subject data?<font color="#f70d1a"> (if 'yes', you <b>will need</b> Ethics approval)</font><br>
&rarr;&nbsp;Are you planning to do Usability Testing with actual users?<font color="#f70d1a"> (if 'yes', you <b>will need</b> Ethics approval)</font>
  </td>
 </tr>
</table>


<br/>
<hr width="100%" align="left" color="#91A3AB">

<a name="late-sep-mid-oct"></a>
<p><b><i>Late September to Mid-October</i></b>: <mark>Your problem is having a problem.</mark></p>

<p>You have perhaps a project assigned by your supervisor. <b>Does it REALLY motivate you?</b> Remember: you are the one working on this for the next six months!</p>
<p>Propose changes, new directions, new features, and so on.</p>

<p>Have clarity on your tool's objectives and draft (high-level) a list of desired features.</p>

<p>Think as if <b>you are the user</b> of your tool: what are the things (features) the system exposes you to?</p>

<p>&rarr;&nbsp;Proceed writing the <a href="FYP-info.php#FYP-kit">Project Definition Form</a> using my pre-filled MS-Word document (it's not assessed, however, it is <b>highly recommended</b> that you produce one, to help you clear your ideas).
</p>

<a href="#top">top</a><br/>

<!--  ###################### -->
<br/>
<hr width="100%" align="left" color="#91A3AB">

<a name="mid-oct-end-oct"></a>
<p><b><i>Mid-October to End-October</i></b>: <mark>Broad survey on tools, datasets, techniques, methods, algorithms, and related work. Start 'formalising' your ideas</mark></p>

<p>Go wild on Internet in search of <i>what other people are doing</i>. Find the tools they used in their approaches, datasets, etc, everything you think it's interesting for shaping your own approach.</p>

<p>Make notes of the interesting things you find - they will be a part of your Partial Report.</p>

<p>Think about your <b>objective</b> (limit to one objective with 1-3 sub-objectives).</p>

<table border="0" cellpadding="3" cellspacing="3" width="80%">
 <tr>
  <td rowspan="2" width="1%" bgcolor="#AABBAA">&nbsp;</td>
  <td style = "border: 1px dotted red;" bgcolor="#eeeeee"><font color="#111111"><b>Question:</b></font></td>
 </tr>
 <tr>
  <td style = "border: 1px dotted red;" bgcolor="#eeffee">
&rarr;&nbsp;<font color="#111111">What will be your <b>deliverable</b>?</font>
</font>
  </td>
 </tr>
</table>

<p><font size="+2" color="#00cc00">&radic;</font>&nbsp;<b><a href="https://www.dictionary.com/browse/strive" target="_blank">Strive</a>&#x2197;</b> (ie, <i>make great effort</i>) finding <font color="#aa1111"><b>good resources</b></font> that you will be able to reference later, ie, stable tools and datasets with 'good' licenses (Creative Commons, Apache, MIT, and so on), papers out of <a href="https://ieeexplore.ieee.org/" target="_blank">IEEExplore</a>&#x2197;, <a href="https://dl.acm.org/" target="_blank">Portal ACM</a>&#x2197;, <a href="https://www.sciencedirect.com" target="_blank">Science Direct</a>&#x2197;, or <a href="https://scholar.google.com" target="_blank">Google Scholar</a>&#x2197; making a note of its <a href="https://en.wikipedia.org/wiki/Digital_object_identifier" target="_blank">Digital Object Identifier </a>&#x2197;(DOI) and for tools/datasets, its URL.

<p><font size="+2" color="#00cc00">&radic;</font>&nbsp;Related tools give you <b><i>insights</i></b> for coming up with your original ideas on how to explore additional features or features that are absent from that tool.</p>

<p><font size="+2" color="#00cc00">&radic;</font>&nbsp;Remember that for the FYP you will be assessed on the <a href="https://en.wikipedia.org/wiki/Systems_development_life_cycle" target="_blank">Software Development Life-Cycle (SDLC)</a>&#x2197; that you produced for your tool.</p>

<table border="0" cellpadding="3" cellspacing="3" width="100%">
 <tr>
  <td rowspan="4" width="1%" bgcolor="#AABBAA">&nbsp;</td>
  <td style = "border: 1px dotted red;" bgcolor="#eeeeee"><font color="#111111"><b>Observation:</b></font></td>
 </tr>
 <tr>
  <td style = "border: 1px dotted red;" bgcolor="#eeffee">
&rarr;&nbsp;<font color="#111111">We are <b>aware</b> of the use of <a href="https://en.wikipedia.org/wiki/Large_language_model" target="_blank">Large Language Models (LLM)</a>&#x2197; tools (eg, OpenAI's ChatGPT, Google's Gemini, Anthropic's Claude, and so on) so we recommend you to use it <b>consciously</b> in <i>assisting</i> you clear your ideas and providing you with insights.
  </td>
 </tr>
 <tr>
 <td style = "border: 1px dotted red;" bgcolor="#eeffee">
  <font size="+2" color="#cc0000">&rArr;</font>&nbsp;However, you must <mark>come up with your own ideas and do your own writing.</mark> Remember, you will have to present a demo and answer questions about it, so you might as well <b><i>know  what you are doing</i></b>.
  </td>
 </tr>
 <tr>
 <td style = "border: 1px dotted red;" bgcolor="#eeffee">
  <font size="+2" color="#cc0000">&rArr;</font>&nbsp;<i>You will sign a disclaimer stating that your Report is free of LLM auto-generated texts, and not adhering to this might invalidate the module (fail).</i></font>
</font>
  </td>
 </tr>
</table>


<br><hr width="50%" align="left" color="#11A3AB">

<p><b>Next, choose a <i>Software Development Methodology</i> to follow.</b> You have as options: <br>
(i) <b><a href="https://en.wikipedia.org/wiki/Waterfall_model" target="_blank">Waterfall Lifecycle</a>&#x2197;</b>, that follows a linear sequential approach;<br>
(ii) <b><a href="https://en.wikipedia.org/wiki/Unified_process" target="_blank">Unified Process (UP)</a>&#x2197;</b>, ie, creating diagrams written in the <a href="https://en.wikipedia.org/wiki/Unified_Modeling_Language" target="_blank">Unified Modelling Language</a>&#x2197; (UML), eg, Use Case Diagrams, Class Diagrams, Activity Diagrams, and so on; or<br>
(iii) <b>Agile</b> approaches, eg, pure Agile, Scrum based, Kanban, eXtreme Programming (XP), or a combination.</p>

<p>The main difference between (i) and (ii) is that waterfall is sequential and linear approach, whereas UP is iterative and incremental.</p>

<p>&rarr;&nbsp;Agile is not a process <i>per se</i>, it's about using as guidelines the <a href="http://agilemanifesto.org/" target="_blank">Agile manifesto</a>&#x2197;. It is useful when you have changing requirements, offering a flexible approach to tackle emerging issues arising from those changes.</p>
<table border="0" cellpadding="10">
 <tr>
  <td width="10%" bgcolor="#eeffaa"><b>&#129488;</b></td>
  <td style = "border: 1px dashed green;" bgcolor="#eeddff">For Agile, you will need to come up with a set of <b>User Stories</b> AND <i>matched</i> <b>Acceptance Criteria</b> that comprises your tool's <i><b>features</b></i>. For more information read <a href="materials/user-stories-and-acceptance-criteria.pdf" target="_blank">"user stories and acceptance criteria"</a>&#x2197; and <a href="materials/Writing_Good_User_Stories.pdf" target="_blank">"writing good user stories"</a>&#x2197;.</td>
 </tr>
</table>

<p>&rarr;&nbsp;<a href="http://en.wikipedia.org/wiki/Unified_Process" target="_blank">Unified Process</a>&#x2197; is an <i>extensible framework</i> centred on structure and documentation, suitable for when you know all your requirements beforehand. It's iterative and incremental.</p>
<table border="0" cellpadding="10">
 <tr>
  <td width="10%" bgcolor="#eeffaa"><b>&#129488;</b></td>
  <td style = "border: 1px dashed green;" bgcolor="#eeddff">For UP, you will need to come up with the appropriate documentation regarding the methodology in the form of (useful) diagrams, ie, Use Case Diagrams, and so on. Access more information about the <a href="materials/UML-Unified-Process.pdf">Unified Process with examples</a>.</td>
 </tr>
</table>

<br><hr width="50%" align="left" color="#11A3AB">

<p><font size="+2" color="#cc0000">&rArr;</font>&nbsp;Note that you can <b>combine</b> everything: employ Agile approach, generate a Backlog (which is a Scrum practice), clarify your tool's features with a detailed Use Case Diagram and a Class Diagram to explain your system's internals. <b>You just need to explain everything in the report.</b></p>

<table border="0" cellpadding="10">
 <tr>
  <td width="10%" bgcolor="#eeffaa"><b>&#129488;</b></td>
  <td style = "border: 1px dashed green;" bgcolor="#eeddff"><block>Tip:</block> Git-based versioning systems such as <a href="https://github.com/" target="_blank">GitHub</a>&#x2197;, <a href="https://about.gitlab.com/" target="_blank">GitLab</a>&#x2197;, and <a href="https://bitbucket.org/product" target="_blank">BitBucket</a>&#x2197; have internal tools to help you write User Stories (and Acceptance Criteria) - check it out and use it at your discretion.
</td>
 </tr>
</table>

<p><font size="+2" color="#cc0000">&rArr;</font>&nbsp;By the <b>end of October</b> you are expected to have a "stable" list of <font size="+2" color="#ff0022">[</font>Use Cases <b>AND</b> <i>drafts</i> for complementary diagrams<font size="+2" color="#ff0022">]</font> (if you picked Unified Process) or <font size="+2" color="#ff0022">[</font>User Stories <b>AND</b> Acceptance Criteria<font size="+2" color="#ff0022">]</font> (if you chose Agile) comprising your features (it is suggested to have a list of 5 to 10 features).


<table border="0" cellpadding="3" cellspacing="3" width="100%">
 <tr>
  <td rowspan="2" width="1%" bgcolor="#AABBAA">&nbsp;</td>
  <td style = "border: 1px dotted red;" bgcolor="#eeeeee"><font color="#111111"><b>Question:</b></font></td>
 </tr>
 <tr>
  <td style = "border: 1px dotted red;" bgcolor="#eeffee">
&rarr;&nbsp;<font color="#111111">At this point, since you are effectivelly <i>building a tool</i> you need to decide whether or not you will do <b>usability tests with actual users</b>. For this, <b><font color="#aa1111">you will need to apply for Ethics considerations</font>.</b> Be conscious of the deadline for applying (check BlackBoard).</font>
</font>
  </td>
 </tr>
</table>
<br>

<a href="#top">top</a><br/>

<!--  ###################### -->
<br/>
<hr width="100%" align="left" color="#91A3AB">

<a name="november"></a>
<p><b><i>November</i></b>: <mark>Refine your ideas, tool design and drafting FYP Partial Report</mark></p>

<p>&rarr;&nbsp;First things first: review and get acquainted with your written <b><font color="#aa1111">requirements</font></b> (note: I'll use onwards <i><b>"requirements"</b></i> to state the use of User Stories/Acceptance Criteria or Unified Process' Diagrams, whatever you chose).</p>

<p>&rarr;&nbsp;Start working on your <i><b>methodology</b></i>, ie, the list of steps that you will follow to come up with your tool at the end. This will enter the Partial Report to submit next month.
</p>

<table border="0" cellpadding="10">
 <tr>
  <td width="1%" bgcolor="#eeffee">&nbsp;</td>
  <td style = "border: 1px dashed green;" bgcolor="#eeeeff"><block>Tip:</block> from now on adopt a <b><a href="https://www.dictionary.com/browse/systematic" target="_blank">systematic</a>&#x2197;</b> stance, ie, <i>be methodical</i>, following the methodology you work on earlier.<br>
  Apply <i>rigour</i> to your investigations, be <i>consistent</i>. <br>
  Write it down your approach and apply to all your research <i>items/tools/datasets/algorithms/etc</i>.</td>
 </tr>
</table>

<p>&rarr;&nbsp;Come up with some <i>mock screens</i> (hand drawn or using a graphics tool like <a href="https://www.drawio.com/" target="_blank">draw.io</a>&#x2197; (it integrates with Google Drive).
</p>

<p>&rarr;&nbsp;Understand the <i><b>flow</b></i> of your tool, ie, how you will <i><b>expose the features</b></i> to users.
</p>

<p>&rarr;&nbsp;Choose <i><b>programming languages and scripting tools</b></i> that your tool will be created with.
</p>

<p>&rarr;&nbsp;If you are working with a dataset, <i><b>code a script or use a tool</b></i> to interact with the data, observing types and what you will extract/process altogether.
</p>

<p>&rarr;&nbsp;Compare similar tools' features, positioning and contrasting with your proposition. Differentiate your approach.
</p>

<br><hr width="50%" align="left" color="#11A3AB">

<p>&rarr;&nbsp;Start working on the <i><b>Partial FYP Report</b></i>, adding everything you have done so far. It will have the objective (and sub-objectives), the tool's features (written as requirements), a draft of related tools and related work (papers and white papers), as well as a timeline until delivery in April.<br>
&nbsp;&nbsp;&nbsp;&bull;&nbsp;Download the <a href="FYP-info.php#FYP-kit">"Term Report or Report"</a> file.
</p>

<p>&rarr;&nbsp;<i><b>Learn how to cite other researchers' work</b></i> (see <a href="FYP-faq.php#papers-cite">this question at my FYP-faq page</a> on where to find "good" references). Consult BlackBoard for further materials.</p>

<p>&rarr;&nbsp;<i><b>Extend (or revise)</b></i> the ideas you put on the Project Definition Form.</p>

<p>&rarr;&nbsp;Exercise <i><b>clarity</b></i> when writing your document, and <i><b>precision/accuracy</b></i> on stating exactly what you need to convey.</p>

<table border="0" cellpadding="10">
 <tr>
  <td width="1%" bgcolor="#eeffee">&nbsp;</td>
  <td style = "border: 1px dashed green;" bgcolor="#eeeeff"><block>Tip:</block> Make notes on the discussions made with your supervisor, date them, and put them under an <i>Appendix</i> in the Partial Report (by the very end, after "References").</td>
 </tr>
</table>
<br>

<table border="0" cellpadding="10">
 <tr>
  <td width="10%" bgcolor="#eeffaa"><b>&#129488;</b></td>
  <td style = "border: 1px dashed green;" bgcolor="#eeddff"><i><b>Develop your own notion of quality</b></i>: ask yourself, "is this <i>good enough?</i>" and "can I improve this even further?"</td>
 </tr>
</table>

<p><font size="+2" color="#cc0000">&rArr;</font>&nbsp;You are expected by this point to have a <b>clear understanding</b> of the tasks ahead (before the next SDLC step, which is implementation/testing).</p>

<a href="#top">top</a><br/>

<!--  ###################### -->
<br/>
<hr width="100%" align="left" color="#91A3AB">

<a name="december"></a>
<p><b><i>December</i></b>: <mark>Filling out blanks and refining Partial Report</mark></p>

<p>&rarr;&nbsp;At this point, you are refining your ideas, until you submit the Partial Report (or Term Report) on the system.</p>

<p><font size="+2" color="#cc0000">&rArr;</font>&nbsp;You are expected to submit the Partial Report by Mid-December (check <b><i>actual</i></b> deadlines on BlackBoard).
</p>

<p>&rarr;&nbsp;When submitted, take a few days off, as you accomplished your milestone.</p>

<br><hr width="50%" align="left" color="#11A3AB">

<p>&rarr;&nbsp;Whenever you feel like it, you can start <b>prototyping</b> your tool, choose auxiliary tools and Integrated Development Environment (IDE), install software and libraries, and get acquainted with your surroundings.</p>

<table border="0" cellpadding="5">
 <tr>
  <td width="10%" bgcolor="#eeffaa"><b>&#129488;</b></td>
  <td style = "border: 1px dashed green;" bgcolor="#eeddff">
   <p><b>Programming tips:</b></p>
   <ul>
    <li>Adopt an <b><i>incremental</i></b> approach to developing code.</li>
    <ul>
     <li>Use a versioning system (<b><i><a href="https://git-scm.com/" target="_blank">Git</a>&#x2197;</i></b> is highly recommended, in  <a href="https://github.com/" target="_blank">GitHub</a>&#x2197;,  <a href="https://about.gitlab.com/" target="_blank">GitLab</a>&#x2197;, or  <a href="https://bitbucket.org/product" target="_blank">BitBucket</a>&#x2197;), commmit and push frequently.</li>
    </ul>
    <li>Test often, either using <a href="https://www.geeksforgeeks.org/unit-testing-software-testing/" target="_blank">Unit Testing</a>&#x2197; or other technique you deem useful.</li>
    <ul>
     <li><b>REALLY</b> test it, try to break it! Fix all issues you observe. Learn about <a href="https://en.wikipedia.org/wiki/Fuzzing" target="_blank">Fuzzy Testing</a>&#x2197;.</li>
    </ul>
    <li>Start coding your proposed <b><i>core features first</i></b>, then supplemental features.</li>
    <ul>
     <li>Idea here is to code your <i>functional</i> requirements (features) first, then move on coding your <i>non-functional</i> (ie, quality related) requirements, like performance, security, usability, scalability, and so on).</li>
    </ul>
    <li><b><i>Make notes</i></b> on algorithms and decisions you took (in either the source code or in the Final Report).</li>
    <ul>
      <li>Comment your code using <a href="https://www.doxygen.nl/" target="_blank">Doxygen</a>&#x2197; - so later on you can automatically generate an iterative documentation.</li>
    </ul>
    <!--<li></li>-->
   </ul>
  </td>
 </tr>
</table>
<br/>


<a href="#top">top</a><br/>

<!--  ###################### -->
<br/>
<hr width="100%" align="left" color="#91A3AB">

<a name="january"></a>
<p><b><i>January</i></b>: <mark>Start working on your project - implementation</mark></p>

<p>&rarr;&nbsp;For about the next 1-3 months you will be implementing and testing a lot of your own tool.</p>

<p>&rarr;&nbsp;<b>Follow the plan you outlined</b>, your timeline - what are you supposed to be doing?</p>

<p>&rarr;&nbsp;As early as you start the better, so you can account for <i>bumps in the road</i>, ie, unexpected things that might happen, diversions, things you should change direction, find other tools/datasets, and so on.</p>

<p>&rarr;&nbsp;Don't forget: the idea is that you must have a <b>working (and useful) deliverable</b> to demonstrate to a panel.</p>

<a href="#top">top</a><br/>

<!--  ###################### -->
<br/>
<hr width="100%" align="left" color="#91A3AB">

<a name="february"></a>
<p><b><i>February</i></b>: <mark>Keep going over the SDLC</mark></p>

<p>&rarr;&nbsp;Follow the plan you outlined. At this point, it's expected that you are doing a lot of <i>testing</i>, making sure your tool's behaviour is appropriate and that you meet the requirements you put forth.
</p>

<p>&rarr;&nbsp;Remember, you can only do <b><i>Usability Testing</i></b> after you have a <b>stable version</b>.</p>

<p>&rarr;&nbsp;When you do and <font color="#ee2233">you are cleared by the Ethics Committee to pursue this task</font>, run it with actual users, documenting all interaction from users in your tool. Write everything down the Final Report in a section/sub-section you created for this.</p>

<a href="#top">top</a><br/>

<!--  ###################### -->
<br/>
<hr width="100%" align="left" color="#91A3AB">

<a name="march"></a>
<p><b><i>March</i></b>: <mark>Focus writing your outcomes on the FYP Report</mark></p>

<p>&rarr;&nbsp;Follow the same principles outlined before, about being systematic, precise, and concise.
</p>

<p>&rarr;&nbsp;REFRAIN from capturing screens with texts or source code. Create an Appendix and put those, as text, in the Report.</p>

<p>&rarr;&nbsp;Use a <a href="https://tohtml.com/" target="_blank">syntax highlighter tool</a>&#x2197; if you wish to put code snippets in the Report.</p>


<a href="#top">top</a><br/>

<!--  ###################### -->
<br/>
<hr width="100%" align="left" color="#91A3AB">

<a name="april"></a>
<p><b><i>April</i></b>: <mark>Last tool refinements and preparing for the demonstration</mark></p>

<p>&rarr;&nbsp;Fix issues that arised (if any) before presentation.
</p>

<p>&rarr;&nbsp;Make sure your demonstration takes 20 minutes, no more, no less.
</p>


<a href="#top">top</a><br/>



<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>

<?php
include("postfooter.php");
?>

            </div> <!-- div main -->

          </div> <!-- div columns -->

        </div> <!-- div wrapper -->

<?php
include("footer.php");
?>

</div>
</body>
</html>