<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="generator" content="GitLab Pages">
    <meta name="keywords" content="ricardo melo czekster, czekster, cyber security"/>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Ricardo M. Czekster's homepage</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="syntax.css">
	<link rel="stylesheet" href="main.css">
    <link rel="stylesheet" href="style.css">
  </head>

<body class="vsc-initialized">
  <div class="wrapper">
	<div class="columns">
<?php
include("sidebar.php");
?>
	  <div class="main">
		<h1>Code snippets</h1>

<br><hr style="width:50%;text-align:left;margin-left:0;color:#ddeeff">
<a name="top"></a>
<h2>Index</h2>
&bull;&nbsp;01. <a href="#bash">Bash</a><br />
&bull;&nbsp;02. <a href="#perl">Perl</a><br />
&bull;&nbsp;03. <a href="#ccpp">C/C++</a><br />
&bull;&nbsp;04. <a href="#mapl">Maple/Octave/Matlab</a><br />
&bull;&nbsp;05. <a href="#late">Latex</a><br />
&bull;&nbsp;06. <a href="#beam">Beamer</a><br />
&bull;&nbsp;07. <a href="#other">Miscelaneous</a><br />
&bull;&nbsp;08. <a href="#gnuplot">GNUplot</a><br />
&bull;&nbsp;09. <a href="#R">R</a><br />
&bull;&nbsp;10. <a href="#ms-win">MS-Windows stuff</a><br />

<br/>
<hr WIDTH="100%">
<br/>

<a name="bash"></a>
<b>01. <a href="#bash">Bash</a></b><br />
&nbsp;&nbsp;&nbsp;01.001. <a href="#bash01001">Setting the current DATE and TIME</a><br />
&nbsp;&nbsp;&nbsp;01.002. <a href="#bash01002">Run PEPS2003 (passing args) in a whole directory of .san files, save progress to an output file</a><br />
&nbsp;&nbsp;&nbsp;01.003. <a href="#bash01003">Run a command, inside a loop (use of <i>for</i> and <i>while</i>) iterates in directory</a><br />
&nbsp;&nbsp;&nbsp;01.004. <a href="#bash01004">Using <i>if</i> - testing for initial parameters</a><br />
&nbsp;&nbsp;&nbsp;01.005. <a href="#bash01005">Multiple <i>if</i></a><br />
<p align="right"><a href="#top">top</a></p>

<br />
<a name="perl"></a>
<b>02. <a href="#perl">Perl</a></b><br />
&nbsp;&nbsp;&nbsp;02.001. <a href="#perl02001">Standard Perl header</a><br />
&nbsp;&nbsp;&nbsp;02.002. <a href="#perl02002">Open a file for reading, iterate through lines, remove \n</a><br />
&nbsp;&nbsp;&nbsp;02.003. <a href="#perl02003">Trim, changes multiple spaces for single space and functions for trim and strip (removing spaces inside strings)</a><br />
&nbsp;&nbsp;&nbsp;02.004. <a href="#perl02004">Open all *.txt files of a directory, runs a system command to print the last line of the file</a><br />
&nbsp;&nbsp;&nbsp;02.005. <a href="#perl02005">Print blocks of text</a><br />
&nbsp;&nbsp;&nbsp;02.006. <a href="#perl02006">Formatting numbers</a><br />
&nbsp;&nbsp;&nbsp;02.007. <a href="#perl02007">Printing blocks of text to an output file</a><br />
&nbsp;&nbsp;&nbsp;02.008. <a href="#perl02008">Using hashes with arrays and iterating</a><br />
&nbsp;&nbsp;&nbsp;02.009. <a href="#perl02009">Creating a dotty file from a matrix</a><br />
&nbsp;&nbsp;&nbsp;02.010. <a href="#perl02010">Passing two arrays to a subrotine and printing</a><br />
&nbsp;&nbsp;&nbsp;02.011. <a href="#perl02011">Date and time and printf</a><br />
&nbsp;&nbsp;&nbsp;02.012. <a href="#perl02012">Match everything betwen { and }</a><br />
&nbsp;&nbsp;&nbsp;02.013. <a href="#perl02013">Open file, match pattern between ( and ), reverse and print array without ending spaces</a><br />
&nbsp;&nbsp;&nbsp;02.014. <a href="#perl02014">Filling a string with zeroes (before the number)</a><br />
&nbsp;&nbsp;&nbsp;02.015. <a href="#perl02015">Take a file with many runs and execute the confidence intervals</a><br />
&nbsp;&nbsp;&nbsp;02.016. <a href="#perl02016">Call subrotine with array and variables</a><br />
&nbsp;&nbsp;&nbsp;02.017. <a href="#perl02017">Using hashes of arrays</a><br />
&nbsp;&nbsp;&nbsp;02.018. <a href="#perl02018">Convert an array to a matrix</a><br />
&nbsp;&nbsp;&nbsp;02.019a. <a href="#perl02019a">Convert photos from high definition to 4x6 inches (or 10x15 cms) format (1024x768) - using convert</a><br />
&nbsp;&nbsp;&nbsp;02.019b. <a href="#perl02019b">Convert photos from high definition to another size - using Image::Resize</a><br />
&nbsp;&nbsp;&nbsp;02.020. <a href="#perl02020">Convert movies from MPG ou MOV to AVI format</a><br />
&nbsp;&nbsp;&nbsp;02.021. <a href="#perl02021">Order hashes by keys or values</a><br />
&nbsp;&nbsp;&nbsp;02.022. <a href="#perl02022">Permutation Algorithm</a><br />
&nbsp;&nbsp;&nbsp;02.023. <a href="#perl02023">Open huge files in Perl</a><br />
&nbsp;&nbsp;&nbsp;02.024. <a href="#perl02024">Create a basic gnuplot file in Perl</a><br />
&nbsp;&nbsp;&nbsp;02.025. <a href="#perl02025">Read folder, iterate through subfolders, then its files</a><br />
&nbsp;&nbsp;&nbsp;02.026. <a href="#perl02026">Pattern matching of datetime format (e.g. 2010-10-13 17:55:39,666)</a><br />
&nbsp;&nbsp;&nbsp;02.027. <a href="#perl02027">Passing two hashes to a function</a><br />
&nbsp;&nbsp;&nbsp;02.028. <a href="#perl02028">Checks if number is an integer (greater than zero) or if number is integer or float</a><br />
&nbsp;&nbsp;&nbsp;02.029. <a href="#perl02029">Checks two datetime parameters ('yyyy mm dd hh:mm:ss') are valid</a><br />
&nbsp;&nbsp;&nbsp;02.030. <a href="#perl02030">Move files without system call</a><br />
&nbsp;&nbsp;&nbsp;02.031. <a href="#perl02031">Working with XML file and parameters</a><br />
&nbsp;&nbsp;&nbsp;02.032. <a href="#perl02032">(classic) Permutation algorithm</a><br />
&nbsp;&nbsp;&nbsp;02.033. <a href="#perl02033">Sub-routines in Perl</a><br />

<p align="right"><a href="#top">top</a></p>

<br />
<a name="ccpp"></a>
<b>03. <a href="#ccpp">C/C++</a></b><br />
&nbsp;&nbsp;&nbsp;03.001. <a href="#ccpp03001">Solve an infinitesimal generator</a><br />
&nbsp;&nbsp;&nbsp;03.002. <a href="#ccpp03002">Removing equals from a list</a><br />
&nbsp;&nbsp;&nbsp;03.003. <a href="#ccpp03003">Removing chars from string</a><br />
&nbsp;&nbsp;&nbsp;03.004. <a href="#ccpp03004">Converting string to upper or lower case</a><br />
&nbsp;&nbsp;&nbsp;03.005. <a href="#ccpp03005"><i>sampler</i> - ferramenta para geração de samples da distribuição uniforme</a><br />

<p align="right"><a href="#top">top</a></p>

<br />
<a name="mapl"></a>
<b>04. <a href="#mapl">Maple/Octave/Matlab</a></b><br />
&nbsp;&nbsp;&nbsp;04.001. <a href="#mapl04001">Solve a pepa model in Maple</a><br />
&nbsp;&nbsp;&nbsp;04.002. <a href="#mapl04002">Solve a Markov Chains model in Octave</a><br />
&nbsp;&nbsp;&nbsp;04.003. <a href="#mapl04003">Print in file in Octave</a><br />

<p align="right"><a href="#top">top</a></p>

<br />
<a name="late"></a>
<b>05. <a href="#late">Latex</a></b><br />
&nbsp;&nbsp;&nbsp;05.001. <a href="#late05001">Figures in Latex</a><br />
&nbsp;&nbsp;&nbsp;05.002. <a href="#late05002">Putting vertical labels in tables</a><br />
&nbsp;&nbsp;&nbsp;05.003. <a href="#late05003">Script for cleaning latex files and compiling latex</a><br />
&nbsp;&nbsp;&nbsp;05.004. <a href="#late05004">Full latex project for Lecture Notes in Computer Science format</a><br />
&nbsp;&nbsp;&nbsp;05.005. <a href="#late05005">Highlighting paragraphs</a><br />
&nbsp;&nbsp;&nbsp;05.006. <a href="#late05006">Adding fancy notes</a><br />
&nbsp;&nbsp;&nbsp;05.007. <a href="#late05007">Better cell alignment with tabular</a><br />

<p align="right"><a href="#top">top</a></p>

<br />
<a name="beam"></a>
<b>06. <a href="#beam">Beamer</a></b><br />

&nbsp;&nbsp;&nbsp;06.001. <a href="#beam06001">A presentation in Beamer, with multiple slides</a><br />
&nbsp;&nbsp;&nbsp;06.002. <a href="#beam06002">Beamer complete list of themes</a><br />

<p align="right"><a href="#top">top</a></p>

<br />
<a name="other"></a>
<b>07. <a href="#other">Miscelaneous</a></b><br />
&nbsp;&nbsp;&nbsp;07.001. <a href="#other07001">A Makefile without having to tell all .c files</a><br />
&nbsp;&nbsp;&nbsp;07.002. <a href="#other07002">Convert a .wav to .mp3 using <i>lame</i></a><br />
&nbsp;&nbsp;&nbsp;07.003. <a href="#other07003">Create a small book from a ps file</a><br />
&nbsp;&nbsp;&nbsp;07.004. <a href="#other07004">Merging PDFs</a><br />
&nbsp;&nbsp;&nbsp;07.005. <a href="#other07005">Converting between file formats</a><br />
&nbsp;&nbsp;&nbsp;07.006. <a href="#other07006">Use regex in Notepad++</a><br />

<p align="right"><a href="#top">top</a></p>

<br />
<a name="gnuplot"></a>
<b>08. <a href="#gnuplot">GNUplot</a></b><br />
&nbsp;&nbsp;&nbsp;08.001. <a href="#gnuplot08001">General GNUplot scripting</a><br />

<p align="right"><a href="#top">top</a></p>

<br />
<a name="R"></a>
<b>09. <a href="#R">R</a></b><br />
&nbsp;&nbsp;&nbsp;09.001. <a href="#R09001">Several scripts</a><br />

<p align="right"><a href="#top">top</a></p>

<br />
<a name="ms-win"></a>
<b>10. <a href="#ms-win">MS-Windows stuff</a></b><br />
&nbsp;&nbsp;&nbsp;10.001. <a href="#ms-win10001">Batch script to test HTTP code of a URL</a><br />

<p align="right"><a href="#top">top</a></p>

</tr>
</table>

<br /><br /><br />

<!--++++++++++++++++++++++++++++++++++bash+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++-->

<hr WIDTH="100%">
<font size="+1">01. Bash programming</font><br />
01.001. <a name="bash01001">Setting the current DATE and TIME</a><br />
<pre>
#!/bin/bash
DATE=`date +%d%m%Y.%H%S`
cd `somedir`
</pre>
<br />
<p align="right"><a href="#top">top</a></p>

01.002. <a name="bash01002">Run PEPS2003 (passing args) in a whole directory of .san files, save progress to an output file</a><br />
<pre>

echo "" > file.log
dirsan=/tmp
for i in $dirsan/*.san; do           #for all existing files in 'san' directory
   ni=${i//.san/}    #removes file termination .san (san wont accept it)
   rm $dirsan/agg/ $dirsan/cnd/ $dirsan/des/ $dirsan/dsc/ $dirsan/jit/ -rf
   rm $dirsan/*.vct -rf #remove previous peps stuff
   hora=`date +%H:%M:%S%t%d/%m` #saves current time, to mark progress
   echo "running $ni at $hora" >> file.log  #save to file.log what's running
   echo 4 2 100000 1 1 $ni 2 1 1 vector 0 2 | $dirsan/peps
      #the last line does 3 things: 
        #1. calls peps setting iterations to 100000 
        #2. compiling (1 1) and 
        #3. solving (2 1 1) then exiting (0 2)
      #we are assuming that the executable is in the dirsan directory
done
</pre>
<br />
<p align="right"><a href="#top">top</a></p>

01.003. <a name="bash01003">Run a command, inside a loop (use of <i>for</i> and <i>while</i>) iterates in directory</a><br />
<pre>
t=0
c=0
o=0
cut=10
term=62
dir=$base'models/8slaves/'
while [ $t -le $term ]; do
   path=$dir'term'$t
   for i in $path/*; do
      file=$path/'8slaves_'$t'_t'$o'.out'
      `rm $file -f`
      let o+=1
      while [ $c -le $cut ]; do
         `$base''split $i $c >> $file`
         let c+=1
      done
      c=0
   done
   o=0
   let t+=1
done
</pre>
<br />
<p align="right"><a href="#top">top</a></p>

01.004. <a name="bash01004">Using <i>if</i> - testing for initial parameters</a><br />
<pre>
#!/bin/bash


if [ "$1" = "" ]; then
   echo "error. missing parameter n"
else
   echo "success"
fi
</pre>
<br />
<p align="right"><a href="#top">top</a></p>

01.005. <a name="bash01005">Multiple <i>if</i></a><br />

<pre>
#!/bin/sh

if [ "$1" = "" ]; then
  echo "invalid command. options: get/getall/put"
else
if [ "$1" = "getall" ]; then
  echo "retrieving ALL from foo"
else
if [ "$1" = "get" ]; then
  echo "retrieving *.htm from foo"
else
if [ "$1" = "put" ]; then
  if [ "$2" = "" ]; then
    echo "choose a file."
  else
    echo "uploading $2 to foo"
  fi
fi
fi
fi
fi
</pre>
<br />
<p align="right"><a href="#top">top</a></p>


<!--++++++++++++++++++++++++++++++++++perl+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++-->
<hr WIDTH="100%">
<font size="+1">02. Perl programming</font><br />
02.001. <a name="perl02001">Standard Perl header</a><br />
<pre>
#!/usr/bin/perl

use strict;
use warnings;

if (@ARGV != 1) { 
   print "missing FILE parameter. \nusage: perl file.pl FILE\n";
   exit;
}

</pre>
<br />
<p align="right"><a href="#top">top</a></p>

02.002. <a name="perl02002">Open a file for reading, iterate through lines, remove \n</a><br />
<pre>
open(INFILE, "<$file") or die("cannot open hash file named $file\n");
my(@lines) = &lt;INFILE&gt;;
close(INFILE);
foreach my $line (@lines) {
   $line =~ s/\n//g;
}
</pre>
<br />
<p align="right"><a href="#top">top</a></p>

02.003. <a name="perl02003">Trim, changes multiple spaces for single space</a><br />

<pre>
$line =~ s/^\s+//;  #trim at start
$line =~ s/\s+$//;  #trim at end
$line =~ s/\s+/ /g; #remove multiple spaces for single space
</pre>

Or using functions to trim or strip spaces within strings:<br />
<pre>
# removes spaces from beginning and end of strings
sub  trim { my $s = shift; $s =~ s/^\s+|\s+$//g; return $s };

# substitutes extra spaces in the middle of strings for single spaces
sub strip { my $s = shift; $s =~ s/ +/ /g; return $s };
</pre>

<br />
<p align="right"><a href="#top">top</a></p>

02.004. <a name="perl02004">Open all *.txt files of a directory, runs a system command to print the last line of the file</a><br />
<pre>
#!/usr/bin/perl

my $path = $ARGV[0]."output/";
my @contents;
opendir MYDIR, $path;
@contents = grep !/^\.\.?$/,grep /.txt/, readdir MYDIR;
closedir MYDIR;
foreach my $file (@contents) {
   print "file=$file\n";
   system("tail -n 1 $path$file");
}
</pre>
<br />
<p align="right"><a href="#top">top</a></p>

02.005. <a name="perl02005">Print blocks of text</a><br />
<pre>

print &gt;&gt;END;
\\begin{table*}[!hbt]
\\begin{center}
END


</pre>
<br />
<p align="right"><a href="#top">top</a></p>

02.006. <a name="perl02006">Formatting numbers </a>(<font color="red">requires module to be installed</font>)<br />
<pre>
#put this on the header
use Number::Format qw(:subs :vars);
$THOUSANDS_SEP   = '.';
$DECIMAL_POINT   = ',';
$INT_CURR_SYMBOL = 'DEM';


#use it like this:
$mem = format_number($mem, 2, 1);
$complex = format_number($complex);
</pre>
<br />
<p align="right"><a href="#top">top</a></p>

02.007. <a name="perl02007">Printing blocks of text to an output file</a><br />

<pre>
open(OUTFILE, ">$name") or die("cannot open $name\n");
print OUTFILE &gt;&gt;END;
#!/bin/sh

set term postscript eps enhanced color
set xtics 2
plot[0:$termo][0:$lcut]   'bc_$model.txt' title "term x cut" with boxes 
END
close(OUTFILE);

#or

open(OUTFILE, ">$name") or die("cannot open $name\n");
print OUTFILE "this is some text to be printed\n";
close(OUTFILE);

</pre>
<br />
<p align="right"><a href="#top">top</a></p>

02.008. <a name="perl02008">Using hashes with arrays and iterating</a><br />
<pre>
$strut->{$lid} = {
   'gid'   =>   $gid,
   'aa'    =>   [@aa],
   'ja'    =>   [@ja],
   'ia'    =>   [@ia],
   'order' =>   $order,
   'nz'    =>   $nz,
};

#defining an array of hashes:
push @terms, $strut;

#iterating
foreach my $st (@terms) {
   foreach my $termo (keys %$strut) {
      print "gid=".($strut->{$termo}{'gid'})."\n";
      print "lid=$termo\n";
      print "order=".$strut->{$termo}{'order'}."\n";
      print "nz=".$strut->{$termo}{'nz'}."\n";
      print "id=".$strut->{$termo}{'id'}."\n";
   }
}

#finally, to print the whole structure, with arrays:
sub print_strut {
   my @lterms = @_;
   print "call to print...\n";
   foreach my $st (@lterms) {
      foreach my $termo (keys %$st) {
         print "term $termo [".($st->{$termo}{'gid'})."] nz=".$st->{$termo}{'nz'}."\n";
         my @aux=@{$st->{$termo}{'aa'}};
         print "aa: ";
         foreach my $a_ (@aux) {
            print "".$a_." ";
         }
         print "\n";
         my @aux=@{$st->{$termo}{'ja'}};
         print "ja: ";
         foreach my $a_ (@aux) {
            print "".$a_." ";
         }
         print "\n";
         my @aux=@{$st->{$termo}{'ia'}};
         print "ia: ";
         foreach my $a_ (@aux) {
            print "".$a_." ";
         }
         print "\n";
      }
      print "\n";
   }
}
</pre>
<br />
<p align="right"><a href="#top">top</a></p>

02.009. <a name="perl02009">Creating a dotty file from a matrix</a><br />

<pre>
print "digraph G {\n";
print "\tnode [shape = circle];\n";
for (my $i = 0; $i &lt; $SIZE; $i++) {
   for (my $j = 0; $j &lt; $SIZE; $j++) {
      if ($matrix[$i][$j] ne "") {
         my $label = "label$i$j";
         my $auxi = ($i+1) &lt; 10 ? "0".($i+1) : ($i+1);
         my $auxj = ($j+1) &lt; 10 ? "0".($j+1) : ($j+1);
         print "\tS".($auxi)." -> S".($auxj)." [ label = \"$label\" ]\n";
      }
   }
}
print "}\n";
</pre>
<br />
<p align="right"><a href="#top">top</a></p>

02.010. <a name="perl02010">Passing two arrays to a subrotine and printing</a><br />
<pre>

#call: verify_changes(@arr1, @arr2);

#subrotine:
sub verify_changes {
   my @v1 = @{$_[0]};
   my @v2 = @{$_[1]};
   print "printing...\n";
   for (my $i = 0; $i &lt; $size; $i++) {
      print "$v1[$i] ";
   }
   print "\n";
   for (my $i = 0; $i &lt; $size; $i++) {
      print "$v2[$i] ";
   }
   print "\n";
}
</pre>
<br />
<p align="right"><a href="#top">top</a></p>

02.011. <a name="perl02011">Date and time and printf</a><br />
<pre>
(my $sec,my $min,my $hour,my $mday,my $mon,my $year,my $wday,my $yday,my $isdst)=localtime(time);
printf "%4d-%02d-%02d %02d:%02d:%02d\n",$year+1900,$mon+1,$mday,$hour,$min,$sec;
#or
my $date = sprintf("%02d/%02d/%4d %02d:%02d:%02d\n",$mday,$mon+1,$year+1900,$hour,$min,$sec);
print $date."\n";
</pre>
<br />
<p align="right"><a href="#top">top</a></p>

02.012. <a name="perl02012">Match everything betwen { and }</a><br />

<pre>
if ($line =~ /(\s*)authors=\{(.*)\}/) {
   print "authors=$2\n";
}
</pre>
<br />
<p align="right"><a href="#top">top</a></p>

02.013. <a name="perl02013">Open file, match pattern between ( and ), reverse and print array without ending spaces</a><br />
<pre>
#!/usr/bin/perl

use strict;
use warnings;

#this script will revert the .mdd final file in the same format peps works

if (@ARGV != 1) { 
   print "missing FILE parameter. \nusage: perl revert.pl FILE\nenter a .rep file with termination\n";
   exit;
}

my $rep_file = $ARGV[0];
open(INFILE, "&lt;$rep_file") or die("cannot open $rep_file");
my(@lines) = &lt;INFILE&gt;;
close(INFILE);

my $name;
my $states;
my @entries;
my $cont = 0;
foreach my $line (@lines) {
   $line =~ s/\n//g;
   if ($line =~ /\(\s(.*)\s\)/) {  #for example ( 2 2 3 4 2 1 )
      $states = $1;
      (@entries) = split(" ", $states);
      @entries = reverse(@entries); #example of reverse
      foreach my $entry (@entries) {
         print "".((++$cont % @entries == 0) ? "$entry" : "$entry ");
      }
      #$cont = 0;
      print "\n";
   }
}
</pre>
<br />
<p align="right"><a href="#top">top</a></p>

02.014. <a name="perl02014">Filling a string with zeroes (before the number)</a><br />

<pre>
my $i = 10;
my $val = sprintf("%05d",$i);
</pre>

<br />
<p align="right"><a href="#top">top</a></p>

02.015. <a name="perl02015">Take a file with many runs and execute the confidence intervals</a><br />
<br /><a href="files/intervals.pl">Download file here</a> -> Coding language: Perl.<br />
<br /><a href="files/samples.txt">Download samples file here</a> Text file containing some values.<br />

<br />
<p align="right"><a href="#top">top</a></p>

02.016. <a name="perl02016">Call subrotine with array and variables</a><br />

<pre>
deal(\@sizes,\@cuts,\@nzs,\@perms,$n_aut,$n_evt,$model);

...

sub deal {
   my ($sz,$ct,$nz,$pe,$auts,$evts,$model_) = @_;
   my @sizes_ = @$sz;
   my @cuts_ = @$ct;
   my @nzs_ = @$nz;
   my @perms_ = @$pe;
}
</pre>

<br />
<p align="right"><a href="#top">top</a></p>

02.017. <a name="perl02017">Using hashes of arrays</a><br />

<pre>
my %rosa = (
  'N66' => [80,0,0,0,1,1,70,100],
  'N23' => [1,70,80,100,30,3,2,1],
  'EQU' => [1,15,50,80,100,75,45,10],
  'S23' => [1,1,2,3,30,100,80,70],
  'S66' => [80,100,70,1,1,0,0,0]
);

...

foreach my $key (keys %rosa) {
   my @forces = @{$rosa{$key}};    #this is where you take the array reference
   print "$key\n";
   foreach my $f (@forces) {       #now, just use it
      print "$f ";
   }
   print "\n";
}
</pre>

<br />
<p align="right"><a href="#top">top</a></p>

02.018. <a name="perl02018">Convert an array to a matrix</a><br />

<pre>
#!/usr/bin/perl

#implementado em 18/01/2010

use strict;
use warnings;

#converte um vetor em uma matriz
#util para percorrer um vetor e imprimir a linha da matriz, acessando os indices corretos

#colocar valores multiplos para fechar matrizes quadradas (nao testado)

if (@ARGV != 2) { 
   print "falta TAMANHO ou DIMENSAO. \nuso: perl vec2mat.pl TAMANHO DIMENSAO\n\tTAMANHO: tamanho total do vetor\n\tDIMENSAO: dimensao final da matriz\n";
   exit;
}

my $SIZE = $ARGV[0];  #tamanho do vetor
my $DIM = $ARGV[1];   #cria uma matriz DIMxDIM a partir do vetor de SIZE posicoes

#inicia um vetor qualquer com valores quaisquer
my @ARR = ();
for (my $i = 0; $i < $SIZE; $i++) {
   push @ARR, $i;
}

#calcula o tamanho da linha
my $linha = @ARR / $DIM;

my $c = 0;
my $val = 0;
my $aux = 0;
for(my $i = 0; $i < @ARR; $i++) {
   my $o = ($c++ % $DIM);
   $val = $aux + ($linha * $o);
   print "$ARR[$val] ";
   if (($i + 1) % $DIM == 0) {
      print "\n";
      $aux++;
   }
}

</pre>

<br />
<p align="right"><a href="#top">top</a></p>


02.019a. <a name="perl02019a">Convert photos from high definition to 4x6 inches (or 10x15 cms) - 1024x768 - using convert</a><br />

<pre>
#!/usr/bin/perl

use strict;
use warnings;

if (@ARGV != 1) { 
   print "missing PATH parameter. \nusage: perl convert.pl PATH\n";
   exit;
}

my $path = $ARGV[0];

my @contents;
opendir MYDIR, $path;
@contents = grep !/^\.\.?$/,grep /.JPG/i, readdir MYDIR;
closedir MYDIR;
system("rm new -rf");
system("mkdir new");
foreach my $file (sort @contents) {
   my $fp = "$path/$file";
   my $aux = $file;
   $aux =~ s/\./;/;
   my $filename;
   my $term;
   ($filename,$term) = split(";",$aux);
   my $new_name = $filename."\_.".$term;
   print "[$aux] converting $file... to $new_name\n";
   system("convert -resize 1024x768 $file $new_name");
   system("mv $new_name ./new");
}
</pre>

<br />
<p align="right"><a href="#top">top</a></p>

02.019b. <a name="perl02019b">Convert photos from high definition to another size - using Image::Resize</a><br />

<pre>
#!/usr/bin/perl

use strict;
use warnings;

use Time::Local;
use Cwd qw();

use Image::Resize;
#install this package: on command.exe type: cpan Image::Resize

#use: perl resize-photos.pl FOLDER
#this script will convert JPG images and put in a new folder, created according to current time (folder will be called "FOLDER-dd-mm-yyyy_hh-mm-ss")

if (@ARGV != 1) {
   print "missing OUTPUT-DIR parameter. \nusage: perl resize-photos.pl OUTPUT-DIR\n";
   exit;
}

my $path = $ARGV[0];

#current localtime
(my $sec,my $min,my $hour,my $mday,my $mon,my $year,my $wday,my $yday,my $isdst)=localtime(time);
my $date = sprintf("%02d-%02d-%4d_%02d-%02d-%02d",$mday,$mon+1,$year+1900,$hour,$min,$sec);

#create directory with the current date-time
my $dirname = $path."-".$date;
print "mkdir \"$dirname\"\n";
system("mkdir \"$dirname\"");

my @contents;
opendir MYDIR, $path;
@contents = grep !/^\.\.?$/,grep /.JPG/i, readdir MYDIR;
closedir MYDIR;
my $cont = 1;
my $size = @contents;
foreach my $file (@contents) {
   print "[".$cont."/".$size."] processing file=$file...\n";
   $cont++;
   my $image = Image::Resize->new($path."\\".$file);
   my $gd = $image->resize(2400, 2400);

   $file =~ s/.jpg//ig;
   my $new_file = $dirname."\\".$file."-new.jpg";
   open(DISPLAY, ">$new_file");
   binmode DISPLAY;
   print DISPLAY  $gd->jpeg();
   close(DISPLAY);
}
print "converted images in '$dirname' folder.\n";
</pre>

<br />
<p align="right"><a href="#top">top</a></p>



02.020. <a name="perl02020">Convert movies from MPG ou MOV to AVI format</a><br />

<pre>
#!/bin/bash

for i in *.[Mm][Pp][Gg]; do
   filename=${i//.MPG/}
   mencoder $i -ofps 25 -ovc xvid -oac mp3lame -lameopts abr:br=192 -srate 48000 -xvidencopts fixed_quant=4 -o $filename.avi
done

</pre>

ou, para converter de MOV para AVI

<pre>
#!/bin/bash

for i in *.[Mm][Oo][Vv]; do
   filename=${i//.MOV/}

   mencoder $i -ovc lavc -lavcopts vcodec=mpeg4:vpass=1 -oac mp3lame -lameopts br=128:cbr:vol=0:mode=0 -o $filename.avi
   mencoder $i -ovc lavc -lavcopts vcodec=mpeg4:vpass=2 -oac mp3lame -lameopts br=128:cbr:vol=0:mode=0 -o $filename.avi
done
</pre>

<br />
<p align="right"><a href="#top">top</a></p>

02.021. <a name="perl02021">Order hashes by keys or values</a><br />

<pre>
#order by keys
my %FILES;
foreach my $key (sort keys %FILES) {
   print "$key => $FILES{$key}\n";
}

#1. order by values
my %FILES;
my @keys = sort { $FILES{$b} cmp $FILES{$a} } keys %FILES;
foreach my $key (@keys) {
   print "$key => $FILES{$key}\n";
}

#2. order when the key is an IP address, e.g. 200.12.34.3 or 15.189.22.33
my %IP;
foreach my $key (sort {(pack'C*',split/\./,$a) cmp (pack'C*',split/\./,$b)} keys %IP) {
   print "IP = $key => $IP{$key}\n";
}

#3. numeric hashes
foreach my $key (sort { $processes{$b} <=> $processes{$a} } keys %processes) {
}

</pre>
<p align="right"><a href="#top">top</a></p>

02.022. <a name="perl02022">Permutation Algorithm</a><br />

<pre>
#!/usr/bin/perl

use strict;
use warnings;

permute(["x", "a", "b", "c"], []);

sub permute {
    my @items = @{ $_[0] };
    my @perms = @{ $_[1] };
    unless (@items) {
        verify(@perms);
    } else {
        my(@newitems,@newperms,$i);
        foreach $i (0 .. $#items) {
            @newitems = @items;
            @newperms = @perms;
            unshift(@newperms, splice(@newitems, $i, 1));
            permute([@newitems], [@newperms]);
        }
    }
}


sub verify {
    my @list = @_;
    print "@list\n";
}
</pre>

<br />
<p align="right"><a href="#top">top</a></p>

02.023. <a name="perl02023">Open huge files in Perl</a><br />

<pre>
#!/usr/bin/perl

use strict;
use warnings;

open(INFILE, "<$file") or die("cannot open file named $file\n");
print "creating dat file with *hot* vector positions named '$hname'...\n";
while (<INFILE>) {
   if ($_ =~ /vec\[(\d*)\] = (\d*)/) {
      #do something
   }
}

close(INFILE);

</pre>

<br />
<p align="right"><a href="#top">top</a></p>

02.024. <a name="perl02024">Create a basic gnuplot file in Perl</a><br />

<pre>

#!/usr/bin/perl

use strict;
use warnings;

if (@ARGV != 1) { 
   print "missing FILE parameter. \nusage: perl create_gnuplot.pl FILE\n";
   exit;
}

my $file = $ARGV[0];
if (!($file =~ /.log$/)) {
   print "file extension MUST be .log. exiting.\n";
   exit;
}

print "opening file named '$file'...\n";

my $base = $file;
$base =~ s/.log$//;

#create a gnuplot
my $gname = "$base.plot";
print "creating new gnuplot file named '$gname'\n";
open(OUTFILE, ">$gname") or die("cannot open $gname\n");
print OUTFILE "set terminal postscript enhanced color\n";
print OUTFILE "set output \"$base.eps\"\n";

#print OUTFILE "set logscale y\n";
print OUTFILE "set key right top\n";
print OUTFILE "set ylabel \"\# of vector access\"\n";
print OUTFILE "set xlabel \"Index\"\n";

print OUTFILE "set style line 1 lt 5 lc rgb \"#0000FF\" lw 1\n";
print OUTFILE "plot \\\n";
print OUTFILE " \"$base.hot\" using 1:2 ls 1 with impulses title \"Accesses\"\n";
close(OUTFILE);

system("gnuplot $base.plot");
system("evince $base.eps");

</pre>

<br />
<p align="right"><a href="#top">top</a></p>

02.025. <a name="perl02025">Read folder, iterate through subfolders, then its files</a><br />

<pre>
#!/usr/bin/perl

use strict;
use warnings;

if (@ARGV != 1) {
  print "missing parameter DATA_DIR\nusage: perl iterate.pl DATA_DIR\n";
  exit;
}

my $path = "./".$ARGV[0];
print "reading directory $path ...\n";

my @dirs;
opendir DIR, $path;
@dirs = grep !/^\.\.?$/,grep /test/, readdir DIR;
closedir DIR;

foreach my $dir (sort @dirs) {
  print "directory read $dir\n";
  my @files;
  opendir FILE, $path.$dir;
  @files = grep !/^\.\.?$/,, readdir FILE;
  closedir FILE;
  foreach my $file (sort @files) {
  print "\tfile read $dir/$file\n";
  }  
}
</pre>

<br />
<p align="right"><a href="#top">top</a></p>

02.026. <a name="perl02026">Pattern matching of datetime format (e.g. 2010-10-13 17:55:39,666)</a><br />

<pre>
if ($timestamp =~ /^(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2}),(\d{3})$/) {
    $Y = $1; $M = $2; $D = $3; $H = $4; $MI = $5; $S = $6; $CCC = $7;
    print "format: d=$D m=$M y=$Y h=$H mi=$MI s=$S ccc=$CCC\n";
}
</pre>

<br />


02.027. <a name="perl02027">Passing two hashes to a function</a><br />

<pre>
...
   compute(\%statespace, \%mc);


sub compute {
   my %statespace = %{ $_[0] };
   my %mc = %{ $_[1] };

   #...   
}
</pre>

<br />
<p align="right"><a href="#top">top</a></p>

02.028. <a name="perl02028">Checks if number is an integer (greater than zero)</a><br />

<pre>
# checks if number is an integer greater than zero
sub isInteger { 
   my $d = shift;
   return 1 if ($d =~ /^(\d+)$/);
   return 0;
}

# validates integer or float numbers: i.e. 35 or 87.53
sub isIntegerOrFloat {
   my $p = shift;
   return 1 if ($p =~ /^\d+(\.\d+)?(\s*)$/gi);
   return 0;
}
</pre>

<br />
<p align="right"><a href="#top">top</a></p>

02.029. <a name="perl02029">Checks two datetime parameters ('yyyy mm dd hh:mm:ss') are valid</a><br />

<pre>
use Time::Local;
use Cwd qw();

# checks if an interval of dates are correct, i.e., end is after start
# Parameters: first is the start time
#             second is the stop time
sub isValidDateInterval {
   my $d1_ = shift;
   my $d2_ = shift;
   
   my($yy1,$mm1,$dd1,$h1,$m1,$s1) = (0,0,0,0,0,0);
   $d1_ =~ m/^(\')(\d{4})\s(\d{2})\s(\d{2})\s(\d{2}):(\d{2}):(\d{2})(\')$/g;
   $yy1=$2; $mm1=$3; $dd1=$4; $h1=$5; $m1=$6; $s1=$7;
   #print "date1: $dd1/$mm1/$yy1 $h1:$m1:$s1\n";
   my $seconds1 = timelocal($s1,$m1,$h1,$dd1,$mm1-1,$yy1);
   #print "seconds1=$seconds1\n";
   
   my($yy2,$mm2,$dd2,$h2,$m2,$s2) = (0,0,0,0,0,0);
   $d2_ =~ m/^(\')(\d{4})\s(\d{2})\s(\d{2})\s(\d{2}):(\d{2}):(\d{2})(\')$/g;
   $yy2=$2; $mm2=$3; $dd2=$4; $h2=$5; $m2=$6; $s2=$7;
   #print "date2: $dd2/$mm2/$yy2 $h2:$m2:$s2\n";
   my $seconds2 = timelocal($s2,$m2,$h2,$dd2,$mm2-1,$yy2);
   #print "seconds2=$seconds2\n";

   #print "diff=".($seconds2-$seconds1)."\n";
   return 1 if ($seconds2-$seconds1>0);
   return 0;
}
</pre>

<br />
<p align="right"><a href="#top">top</a></p>


02.030. <a name="perl02030">Move files without system call</a><br />

<pre>
use File::Copy;
move("mc-$target_year.txt", "output/mc-$target_year.txt") or die "Move mc-$target_year.txt -> output/mc-$target_year.txt failed: $!";
</pre>

<br />
<p align="right"><a href="#top">top</a></p>


02.031. <a name="perl02031">Working with XML file and parameters</a><br />

<pre>
#!/usr/bin/perl

use strict;
use warnings;

use XML::LibXML;

if (@ARGV != 1) { 
   print "missing FILE parameter. \nusage: perl test.pl FILE\n";
   exit;
}

my $file = $ARGV[0];

my $dom = XML::LibXML->load_xml(location => $file);
my $parser = XML::LibXML->new();
my $tree = $parser->parse_file($file);
my $root = $tree->getDocumentElement;

foreach my $id ($root->findnodes('//AMC/PROBABILITES_SEQUENCES/SEQUENCE')) {
   my $prob =  $id->findvalue('@PROBABILITE');
   print "Probability: $prob\n";
   foreach my $trans ($id->findnodes('./BRANCHE/TRANSITION')) {
      my $objet =  $trans->findvalue('@OBJET');
      my $realization = $trans->findvalue('@TRANS');
      print "\tObject: $objet\n";
      print "\tRealization: $realization\n";
   }
}

XML test file:

&#60;AMC&#62;
 &#60;PROBABILITES_SEQUENCES&#62;
  &#60;SEQUENCE TRONQUEE="CIBLE" PROBABILITE="0.22983"&#62;
   &#60;BRANCHE DATE_FIN_MOY="2595.97313362042"&#62;
    &#60;TRANSITION OBJET="acquire" REGLE="success_undetected" TRANS="real"/&#62;
   &#60;/BRANCHE&#62;
   &#60;BRANCHE DATE_FIN_MOY="2595.97313362042"&#62;
    &#60;TRANSITION OBJET="data" REGLE="real_nd" TRANS="no_realization"/&#62;
    &#60;TRANSITION OBJET="abs" REGLE="real_nd" TRANS="nd_real"/&#62;
    &#60;TRANSITION OBJET="muscular" REGLE="real_nd" TRANS="no_realization"/&#62;
   &#60;/BRANCHE&#62;
  &#60;/SEQUENCE&#62;
  &#60;SEQUENCE TRONQUEE="CIBLE" PROBABILITE="0.10047"&#62;
   &#60;BRANCHE DATE_FIN_MOY="2588.08094838842"&#62;
    &#60;TRANSITION OBJET="balance" REGLE="success_undetected" TRANS="real"/&#62;
   &#60;/BRANCHE&#62;
   &#60;BRANCHE DATE_FIN_MOY="2588.08094838842"&#62;
    &#60;TRANSITION OBJET="data" REGLE="real_nd" TRANS="nd_real"/&#62;
    &#60;TRANSITION OBJET="abs" REGLE="real_nd" TRANS="nd_real"/&#62;
    &#60;TRANSITION OBJET="muscular" REGLE="real_nd" TRANS="no_realization"/&#62;
   &#60;/BRANCHE&#62;
  &#60;/SEQUENCE&#62;
 &#60;/PROBABILITES_SEQUENCES&#62;
&#60;/AMC&#62;

</pre>

<br />
<p align="right"><a href="#top">top</a></p>

02.032. <a name="perl02032">(classic) Permutation algorithm</a><br />

<pre>
#!/usr/bin/perl

use strict;
use warnings;

#incredible permutation algorithm! See Knuth.
my %parameters = (
   "123.456"     => "1;2;10",
   "123.457"     => "2;8",
   "123.458"     => "3;12",
   "123.459"     => "5;10",
);

# start of permutation algorithm for creating all the possible scenarios from the parameters (above)
my $tam = keys %parameters;

my @scenarios;
my $cont = 0;
my @keys;
foreach my $key (keys %parameters) {
   my @arr = split(";", $parameters{$key});
   $scenarios[$cont] = @arr; #gets the size of the variations
   $keys[$cont] = $key;
   $cont++;
}

my @pgs;
#initialisation
for (my $i=0;$i<$tam;$i++) { $pgs[$i] = 0; }
my @vv;
$vv[0] = $scenarios[0];
for (my $i=1;$i<$tam;$i++) { $vv[$i] = $scenarios[$i] - 1; }
#algorithm 'per se'
$pgs[$tam-1] = -1;
my $a = $tam - 1;
my $nz;
$cont = 0;
my @idx_scenarios = ();
while ($pgs[0] != $vv[0]) {
   if ($pgs[$a] < $vv[$a]) {
       $pgs[$a] = $pgs[$a] + 1;
       $a = $tam - 1;
       if ($pgs[0] != $vv[0]) {
           my $str = "";
           for (my $i = 0; $i < $tam; $i++) {
               $str .= "$pgs[$i]";
               $str .= $i==$tam-1?"":";";
           }
           #print "".($cont++).": $str\n";
           push @idx_scenarios, $str;
       }
   } else {
       $a--;
       #restart from a+1 till the end with zeroes
       for (my $i = $a + 1; $i < $tam; $i++) {
           $nz = ($pgs[$i] != -1) ? 0 : -1;
           $pgs[$i] = $nz;
       }
   }
}

foreach (my $i=0; $i < @keys; $i++) {
   print "Key: $keys[$i] Size: $scenarios[$i]\n";
}
print "\nIndices: \n";
print "A total of ".(@idx_scenarios)." scenarios was created:\n";
$cont = 1;
foreach my $elem (@idx_scenarios) {
   print "[".($cont++)."]: $elem\n";
}

#accessing values
print "\nScenarios: ";
$cont = 0;
print "\n";
for (my $i=0; $i < @idx_scenarios; $i++) { 
   $cont++;
   my @aux = split(";", $idx_scenarios[$i]);
   print "[".$cont."] ";
   for (my $j=0; $j < @aux; $j++) {
      my @elem = split(";",$parameters{$keys[$j]});
      print "$keys[$j]=$elem[$aux[$j]]";
      print ($j==@aux-1?"":", ");
   }
   print "\n";
}

</pre>


<br />
<p align="right"><a href="#top">top</a></p>

02.033. <a name="perl02033">Sub-routines in Perl</a><br />

On each file, add
<pre>
require './bdmp-routines.pl';
</pre>

And on this file, add all sub (sub-routines), and end the file with:
<pre>
1;
</pre>
(otherwise, it won't work)

<br/>
<p align="right"><a href="#top">top</a></p>


<!--++++++++++++++++++++++++++++++++++ccpp+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++-->
<hr WIDTH="100%">
<font size="+1">03. C/C++ programming</font><br />

03.001. <a name="ccpp03001">Solve an infinitesimal generator</a><br />
<br /><a href="files/aps.zip">Download file here</a> -> Coding language: C++.<br />

Obs.: the file must have the following format:<br />
<pre>
q1
n9
-2 2 0 0 0 0 0 0 0
0 -6 1 5 0 0 0 0 0
0 0 -10 0 10 0 0 0 0
0 0 0 -6 6 0 0 0 0
5 0 0 0 -13 3 5 0 0
0 0 0 0 0 -5 0 5 0
0 0 0 0 0 0 -4 4 0
0 0 0 5 0 0 0 -7 2
0 0 0 0 5 0 0 0 -5
</pre>
<br />
The call for the executable is: ./aps ITERATIONS FILE<br />
Example: ./aps 100 file.txt<br />
The output will be the resulting vector.<br />
<br />
<p align="right"><a href="#top">top</a></p>

03.002. <a name="ccpp03002">Removing equals from a list</a><br />

<pre>

#include &lt;vector&gt;
#include &lt;list&gt;
#include &lt;iostream&gt;
#include &lt;iomanip&gt;
#include &lt;fstream&gt;

#include &lt;stdlib.h&gt;
#include &lt;math.h&gt;
#include &lt;stdio.h&gt;
#include &lt;time.h&gt;
#include &lt;assert.h&gt;


using namespace std;

//for minimum and maximum float value
#include &lt;float.h&gt;

std::vector&lt;int&gt; removeequals(int *y, int size) {
   std::list&lt;int&gt; l;
   for (int i = 0; i &lt; size; i++) {
      l.push_back(y[i]);
   }
   l.sort();
   l.unique();
   std::vector&lt;int&gt; v(l.begin(), l.end());
   return v;
}

int main(char argc, char** argv) {
    
   if (argc != 1) {
      printf("Usage: ./list\n\n");
      //printf("FILE file name to be open\nmodel - model to be used\nruns - number of executions\n");
      exit(0);
   }
   int *y = (int*)malloc(sizeof(int)*10);
   y[0] = 15;
   y[1] = 11;
   y[2] = 11;
   y[3] = 12;
   y[4] = 11;
   y[5] = 14;
   y[6] = 11;
   y[7] = 14;
   y[8] = 12;
   y[9] = 11;

   std::vector&lt;int&gt; newv = removeequals(y, 10);
   for (int i = 0; i &lt; newv.size(); i++) {
      int value = (int) newv[i];
      cout &lt;&lt; "value["&lt;&lt;i&lt;&lt;"]=" &lt;&lt; value &lt;&lt; endl;
   }
   return 0;
}

</pre>
<br />
<p align="right"><a href="#top">top</a></p>

03.003. <a name="ccpp03003">Removing chars from string</a><br />
<pre>
string StringUtils::removechar(const char r, const string& in) {
   string out = in;
   while (out.find(r) != string::npos) {
      string::size_type pos = out.find(r);
      out.erase(pos,1);
   }
   return out;
}

</pre>
<br />
<p align="right"><a href="#top">top</a></p>

03.004. <a name="ccpp03004">Converting string to upper or lower case</a><br />
<pre>
string lcase(const string& in) {
   string out = in;
   std::transform(out.begin(), out.end(), out.begin(), ::tolower);
   return out;
}

string ucase(const string& in) {
   string out = in;
   std::transform(out.begin(), out.end(), out.begin(), ::toupper);
   return out;
}


</pre>
<br />
<p align="right"><a href="#top">top</a></p>


03.005. <a name="ccpp03005"><i>sampler</i> - ferramenta para geração de samples da distribuição uniforme</a><br />
<br />

<a href="files/sampler.zip">baixe aqui a ferramenta</a><br />

<br />
<p align="right"><a href="#top">top</a></p>


<!--++++++++++++++++++++++++++++++++++mapl+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++-->
<hr WIDTH="100%">
<font size="+1">4. Maple/Matlab/Octave</font><br />

04.001. <a name="mapl04001">Solve a pepa model in Maple</a><br />
<pre>

n:=2;
l:=1;
r:=3;
p:=5;
t:=4;

with(linalg):
Q := array(sparse,1..96,1..96):
read `mobile2agents.maple`:
b := array(sparse,1..96):
for i to 96 do
    Q[i,96] := 1.0
od:
b[96] := 1:
QT := transpose(Q):
P := linsolve(QT,b);
</pre>


<pre>
n:=2;
l:=1;
r:=3;
p:=5;
t:=4;

with(linalg):
Q := array(sparse,1..96,1..96):
read `mobile2agents.maple`:
b := array(sparse,1..96):
for i to 96 do
    Q[i,96] := 1.0
od:
b[96] := 1:
QT := transpose(Q):
P := linsolve(QT,b);

</pre>

<br />
<p align="right"><a href="#top">top</a></p>


04.002. <a name="mapl04002">Solve a Markov Chains model in Octave</a><br />

<pre>
#author: Ricardo M. Czekster
#created: 01/27/2010

clear;

#infinitesimal generator definition
#every line must sums to zero (diagonal has the negative sum of the line)
Q = [-109.3154, 100, 0, 0, 0, 9.1854, 0, 0, 0.13, 0, 0;
200, -205.13, 5, 0, 0, 0, 0, 0, 0, 0, 0.13;
0, 0.0004, -135.0004, 8, 0, 0, 127, 0, 0, 0, 0;
0, 0, 41.58, -58.58, 17, 0, 0, 0, 0, 0, 0;
0, 0, 0, 1.69, -3.04, 1.35, 0, 0, 0, 0, 0;
87, 0, 0, 0, 0.18, -87.18, 0, 0, 0, 0, 0;
0, 0, 14, 0, 0, 0, -17.5, 3.5, 0, 0, 0;
0, 0, 0, 0, 0, 0, 4.2336, -104.4436, 100, 0.21, 0;
100, 0, 0, 0, 0, 0, 0, 419.58, -519.58, 0, 0;
0, 0, 0, 0, 0, 0, 0, 2.77, 0, -10.57, 7.8;
0, 0.013, 0, 0, 0, 0, 0, 0, 0, 30.24, -30.253];

#just saves original matrix before manipulation (uncomment here if needed)
#O = Q;

#last column set to one
for i=1:11
  Q(i,11) = 1;
endfor

b = [0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 1];

#transpose matrix Q
QT = Q';

#linear solution (results in vector P)
P = QT \ b;

#probability output (times 100)
#P *= 100;
</pre>

<br />
<p align="right"><a href="#top">top</a></p>

04.003. <a name="mapl04003">Print in file using Octave</a><br />

<pre>

#create a file with .m extension

1;

start = 1;
lmax = 100;

X_t = zeros(1,lmax);

#create a file named out.txt with n and X as values from start to lmax
filename = "out.txt";
fid = fopen (filename, "w+");
fprintf(fid,"#n X\n");

i = 1;
for n=start:lmax

   fprintf(fid,"%d %f\n",n,X_t(i));

   i = i + 1;
endfor
fclose(fid);

</pre>

<br/>
<p align="right"><a href="#top">top</a></p>



<!--++++++++++++++++++++++++++++++++++late+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++-->
<hr WIDTH="100%">
<font size="+1">05. Latex</font><br />

05.001. <a name="late05001"></a><b>Figures in Latex</b><br />

<pre>
\begin{figure}[!htb]
\centering
\includegraphics[scale=1]{image.png}
\caption{This is a caption.}
\label{fig:label}
\end{figure}
</pre>

<br />
<p align="right"><a href="#top">top</a></p>

<hr style="width:50%;text-align:left;margin-left:0;color:#ddeeff">


05.002. <a name="late05002"></a><b>Putting vertical labels in tables</b><br />
<pre>
\usepackage{rotating}

...

\begin{tabular}{|r|r|}\hline
\begin{sideways}Paper\end{sideways} &\begin{sideways}Static\end{sideways} \\
\hline
HAR1994j & Journal \\
SWRT1996c & Conference \\
\hline
\end{tabular}
</pre>

<br />
<p align="right"><a href="#top">top</a></p>

<hr style="width:50%;text-align:left;margin-left:0;color:#ddeeff">


05.003. <a name="late05003"></a><b>Script for cleaning latex files and compiling latex</b><br />
<br /><a href="files/clean.sh">Cleaning a latex directory with a bash script</a> -> Script language: bash.<br />
<br /><a href="files/compile.sh">Compiling latex from a bash script</a> -> Script language: bash.<br />
<br/>

<hr style="width:50%;text-align:left;margin-left:0;color:#ddeeff">


05.004. <a name="late05004"></a><b>Full latex project for Lecture Notes in Computer Science format</b><br />
<br /><a href="files/template_lncs.zip">Template for LNCS project</a> -> Latex files.<br />
<br />

<hr style="width:50%;text-align:left;margin-left:0;color:#ddeeff">

05.005. <a name="late05005"></a><b>Highlighting paragraphs</b><br />
<pre style="overflow-x:hidden;">
\usepackage{xcolor}
\newcommand{\highlight}{\textcolor{red}}
</pre>
<br />
<p align="right"><a href="#top">top</a></p>

<hr style="width:50%;text-align:left;margin-left:0;color:#ddeeff">


05.006. <a name="late05006"></a><b>Adding fancy notes</b><br />
<pre>
\usepackage{todonotes}
\reversemarginpar
\newcommand{\observation}[2][Rand]{\todo[author={\tiny\hfill\textbf{#1}},fancyline,size=\small,color=black!20,bordercolor=black!20]{\scriptsize #2}}
\newcommand{\obs}[2][Rand]{{\observation[#1]{#2}}}
%use: \obs[RPI]{Observe this.} where RPI = Random Person Initials
</pre>
<br />
<p align="right"><a href="#top">top</a></p>

<hr style="width:50%;text-align:left;margin-left:0;color:#ddeeff">

05.007. <a name="late05007"></a><b>Better cell alignment with tabular</b><br />
Put this inside the cell (between &): <font color="#558844">(it's noisy but it works!)</font>
<pre style="overflow-x:hidden;">
\begin{tabular}[c]{@{}l@{}}
 line 1 \\
 line 2
\end{tabular}} \\ \hline
</pre>
<br />
<p align="right"><a href="#top">top</a></p>



<!--++++++++++++++++++++++++++++++++++beam+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++-->
<hr WIDTH="100%">
<font size="+1">06. Beamer presentations</font><br />

06.001. <a name="beam06001">A presentation in Beamer, with multiple slides</a><br />
<br />
Download <a href="files/beamer.zip">presentation+figures</a><br />
<a href="http://www.math-linux.com/spip.php?article77">Link to some useful tips</a><br />

<br />
<p align="right"><a href="#top">top</a></p>

06.002. <a name="beam06002">Beamer complete list of themes</a><br />

<br />
To use it, change the following line: <br />
\usetheme{THEME}<br />
I particularly like the <i>Frankfurt</i> theme. It is the most classic. He is without sections, only balls to mark where you are in the presentation.<br />
<br />
<pre>
AnnArbor
Antibes
Bergen
Berkeley
Berlin
Boadilla
CambridgeUS
Copenhagen
Darmstadt
Dresden
Frankfurt
Goettingen
Hannover
Ilmenau
JuanLesPins
Luebeck
Madrid
Malmoe
Marburg
Montpellier
PaloAlto
Pittsburgh
Rochester
Singapore
Stuttgart
Szeged
Warsaw
</pre>
<br />
<p align="right"><a href="#top">top</a></p>


<!--++++++++++++++++++++++++++++++++++other+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++-->
<hr WIDTH="100%">
<font size="+1">07. Miscelaneous</font><br />

07.001. <a name="other07001">07. A Makefile without having to tell all .c files</a><br />
<pre>
#
# Makefile of XXXX XXXX
# Changed by xxxx@xxx.xxxxx.xx
#

CPP=g++
INCLUDE_DIR = ./include
BIN_DIR = .
OBJ_DIR = .
LIB_DIR = ./lib

# The options of compilation (debug or optimization)
CPPFLAGS= -I/usr/local/include -Wno-non-virtual-dtor -Wno-deprecated -O3 -I$(INCLUDE_DIR)

# The options of linkage
LDFLAGS= -L$(LIB_DIR) -lyourlib

SOURCES := $(patsubst %.cpp,%.o,$(wildcard *.cpp))
OBJECTS=$(SOURCES:.cpp=.o)
EXECUTABLE=hello

compile: $(SOURCES) $(EXECUTABLE)
	
$(EXECUTABLE): $(OBJECTS)
	$(CPP) $(LDFLAGS) $(OBJECTS) -o $@

.cpp.o:
	$(CPP) $(CPPFLAGS) -c $< -o $@

all: compile

clean:
	rm -f *.o hello

</pre>
<br />
<p align="right"><a href="#top">top</a></p>

07.002. <a name="other07002">Convert a .wav to .mp3 using <i>lame</i></a><br />
<pre>
for i in *.wav; do lame --preset standard $i `basename $i .wav`.mp3; done
</pre>
<br />
<p align="right"><a href="#top">top</a></p>


07.003. <a name="other07003">Create a small book from a ps file</a><br />
<pre>
psbook IN OUT
psnup -pa4 -2 IN OUT
</pre>
<br />
<p align="right"><a href="#top">top</a></p>


07.004. <a name="other07004">Merging PDFs</a><br />
<pre>
gs -dBATCH -dNOPAUSE -q -sDEVICE=pdfwrite -sOutputFile=output.pdf file1.pdf file2.pdf 
</pre>
<br />
<p align="right"><a href="#top">top</a></p>

07.005. <a name="other07005">Converting between file formats</a><br />
<pre>
iconv --from-code=ISO-8859-1 --to-code=UTF-8 ./file.txt > out.txt
</pre>
<br />
<p align="right"><a href="#top">top</a></p>

07.006. <a name="other07006">Use regex in Notepad++</a><br />
Example of use of Arena's output (in Portuguese):<br />
Open Search Dialog (Ctrl-F) then change 'Search Mode' to "Regular expression"<br />
<pre>
Atendimento no Caixa.Queue.NumberInQueue.*\s*\.\d*
</pre>
<br />
<p align="right"><a href="#top">top</a></p>



<!--++++++++++++++++++++++++++++++++++gnuplot+++++++++++++++++++++++++++++++++++++++++++++++++++++++++-->
<hr WIDTH="100%">
<font size="+1">08. GNUplot</font><br />

08.001. <a name="gnuplot08001">08. General GNUplot scripting</a><br />
<br />
<b>Positioning legends in figure:</b><br />
<pre>
set key {  left | right | top | bottom | outside | below }
</pre>
<br />
<b>Second axis y:</b><br />
<pre>
plot "ad.txt" using 1:2 axes x1y2 ls 1 with boxes title "Experiment 2 - 9"
</pre>
<br />
<b>Columns operations:</b><br />
<pre>
plot \
 "ad.txt" using 1:($3-$2) with boxes title "Experiment [3 - 2]", \
 "" using 1:2 with lines title "Experiment 2"

file "ad.txt" #(note that it has 3 columns, ie, $1, $2, $3):
1 10 2
2 15 5
3 21 10
4 15 8
5 11 10
6 9 3
...

</pre>
<br />
<b>Line styles example:</b><br />
<pre>
set term postscript eps color
set output "ad.eps"

set logscale xy
set xrange[9.5:16.5]
set key right bottom
set grid

set style line 1 lt 1 lc rgb "red" lw 3
set style line 2 lt 2 lc rgb "blue" lw 1
set style line 3 lt 3 lc rgb "yellow" lw 3
set style line 4 lt 4 lc rgb "#aabbcc" lw 1
set style line 5 lt 5 lc rgb "grey" lw 1

plot \
 "ad.txt" using 1:2 ls 1 with lines title "Experiment 1", \
 "" using 1:3 ls 2 with lines title "Experiment 2", \
 "" using 1:4 ls 3 with lines title "Experiment 3", \
 "" using 1:5 ls 4 with lines title "Experiment 4", \
 "" using 1:6 ls 5 with lines title "Experiment 5"

</pre>
<br />
<b>Multiplot example: </b><br />
<pre>
set term postscript eps color
set output "results.eps"

set multiplot layout 1, 2 title "Results for Master-Slave Model"

######################################################################
set title "Time (s) - loglog scale"

set logscale xy
set xrange[4.5:10.5]
set key left top

plot \
 "slaves.txt" using 1:2 with lines title "Exp. 1", \
 "" using 1:3 with lines title "Experiment 2", \
 "" using 1:4 with lines title "Experiment 3", \
 "" using 1:5 with lines title "Experiment 4", \
 "" using 1:6 with lines title "Experiment 5"

######################################################################
set title "Memory (Kb) - loglog scale"

set logscale xy
set xrange[4.5:10.5]
set key left top

plot \
 "slaves.txt" using 1:11 ls 1 with lines title "Exp. 1", \
 "" using 1:12 ls 3 with lines title "Exp. 3", \
 "" using 1:13 ls 5 with lines title "Exp. 5", \
 "" using 1:14 ls 6 with lines title "Exp. 6", \
 "" using 1:15 ls 7 with lines title "Exp. 7", \
 "" using 1:16 ls 8 with lines title "Exp. 8"

</pre>
<br />
<b>Animation (.gif) and drawing a matrix: </b><br />
<pre>
set terminal gif animate delay 1
set output "floripa_certo.gif"

#set view 60,60
set view 60,45
#set view 0,180
#set nosurface
#set contour base
#set dgrid3d
set surface
set pm3d
set cntrparam levels auto 30
splot "floripa_certo_1.dat" matrix with lines
splot "floripa_certo_501.dat" matrix with lines 
splot "floripa_certo_1001.dat" matrix with lines 

</pre>

<br />
<b>Multiplot with single axis X and the rest 'stacked'</b><br />


<pre>
set terminal postscript
set output "data.ps"
unset key
NX=1; NY=3
DX=0.01; DY=0.01; SX=0.85; SY=0.25
set bmargin DX; set tmargin DX; set lmargin DY; set rmargin DY
## set the margin of each side of a plot as small as possible
## to connect each plot without space
set size SX*NX+DX*1.5,SY*NY+DY*1.8
set multiplot
## First Figure-bottom
set size SX,SY
set xrange [0:140]
set yrange [-0.9:0.9]
set ytic -0.6,0.3,0.6 ### from -0.6 to 0.6 with 0.3 interval
set origin DX,DY;
plot sin(x)
###Second Figure-middle
set origin DX,DY+SY;
unset xtics
plot sin(x**2)
##- Third Figure-top
set origin DX,DY+SY*2
plot cos(x)
unset multiplot
</pre>
<br />


<!--++++++++++++++++++++++++++++++++++gnuplot+++++++++++++++++++++++++++++++++++++++++++++++++++++++++-->
<hr WIDTH="100%">
<font size="+1">09. R</font><br />

09.001. <a name="R9001">Several scripts</a><br />
<br />
<b>Standard commands:</b><br />
<pre>
#used for the transpose function
library(data.table)

#remove all objects defined earlier
rm(list=ls(all=TRUE))
#remove all plots in the environment
graphics.off()

#set working directory
setwd("C:\\Users\\stout\\Desktop\\Rscripts")
outputfile = "output-longstudy.txt"
#read .csv file
data <- read.table(outputfile, header=FALSE, sep=";")
#count entries
maxf = max(count.fields(outputfile, sep = ';'))
#read to variable, adding "V#" into each column
data <- read.table(outputfile, header = FALSE, sep = ";",  
           col.names = paste0("V",seq_len(maxf)), fill = TRUE)

#save first column with the path names
pathnames <- data[,1]
#remove first column
data$V1 <- NULL
#transpose the data, because each column corresponds to a probab. over time
t_data <- transpose(data)
#---------------------------------------------

#summary of one column (named V4)
summary(t_data$V4)
#summary of all data
summary(data)

graphics.off()
#selecting only observations in column 'V5' higher than 0.001
high_prob <- data[data$V5 > 0.001, ]
#plotting a boxplot on that variable
boxplot(high_prob)

#plotting two simple lines on column 'V5'
plot(t_data$V5,type = "l", lty = 1, col="red")
lines(t_data$V2,type = "l", lty = 1, col="blue")

##############
graphics.off()
#plotting MULTIPLE data series using matplot (use of cbind function)
matplot(cbind(t_data1,t_data2), type = c("l"),main="Ensemble of attack paths",cex.main=1.5,
        pch=1,col = 2:32,xlim=c(1,36), xaxs="i", yaxs="i",
        ylim=c(0,0.011),ylab="Path probability",xlab="Time (in hours)",axes=F)
box()
axis(1, seq(0,36,1),las=1, cex.axis=0.75, font=1)
#use in conjunction with ylim (above) --> 0.011
axis(2, seq(0,0.011,0.01),cex.axis=0.85, las=2)

</pre>
<p align="right"><a href="#top">top</a></p>



<!--++++++++++++++++++++++++++++++++++gnuplot+++++++++++++++++++++++++++++++++++++++++++++++++++++++++-->
<hr WIDTH="100%">
<font size="+1">10. MS-Windows stuff</font><br />

10.001. <a name="ms-win10001">Batch script to test HTTP code of a URL</a><br />
<br />
<b>File: test-url.bat</b><br />
<pre>
ECHO OFF
::CMD will no longer show us what command it’s executing(cleaner)

ECHO This is a cURL script.

:: Print some text
FOR /L %%i IN (1,1,200000) DO (
  echo | set /p= [%%i] & curl -LI https://www.google.com -o /dev/null -w %%{http_code}\n -s
  timeout 60 > nul :: set the timeout to wait between requests
)
</pre>
<p align="right"><a href="#top">top</a></p>


<!--   END    -->


<?php
include("postfooter.php");
?>

          </div>

        </div>

<?php
include("footer.php");
?>

</div>

<script>
  let text = document.getElementById('info').innerHTML;
  const copyContent = async () => {
    try {
      await navigator.clipboard.writeText(text);
      console.log('Content copied to clipboard');
    } catch (err) {
      console.error('Failed to copy: ', err);
    }
  }
</script>
</body>
</html>