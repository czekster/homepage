<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="generator" content="GitLab Pages">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Ricardo M. Czekster's homepage</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="syntax.css">
	<link rel="stylesheet" href="main.css">
    <link rel="stylesheet" href="style.css">
  </head>

<body class="vsc-initialized">
  <div class="wrapper">
	<div class="columns">
<?php
include("sidebar.php");
?>
	  <div class="main">
		<h1>Research highlights</h1>
<table border="0" align="right"><tr><td><img align="right" width="400" src="images/research1.png"><font size="-1"><br/>Source: Pixabay<a href=https://pixabay.com/illustrations/light-bulb-brain-mind-science-6010651/"> (CC public domain)</a></font></td></tr></table>

<p>Feel free to propose a research project about computing and send me an e-mail.
Check my <a href="research.php">main research interests in this link</a>.</p>

<p>You may also read my <a href="FYP.php">Final Year Project (FYP) page</a> for relevant information on writing and preparing scientific documents as well. You can also check out some <a href="tips-for-better-writing.php">tips for writing better</a>.</p>

<h3>General guidance</h3>
<p>Scientists care about <i><b>objective truth</b></i>, facts over opinions, and evidence-based data to support their theories and musings. There's a quote from a book by P. C. Hodgell that is usually attributed to Carl Sagan that states that <i>"If it can be destroyed by the truth, it deserves to be destroyed by the truth."</i></p>

<p>If you always seek the truth and have provided evidence to support your claims in a scientific and peer-reviewed paper, then, to the best of your knowledge, you have nothing to worry about.</p>

<p>Scientists base their research ideas by building up on top of other respectful research results from reproducible artefacts. As Isaac Newton beautifully put it, <i>"If I have seen further, it is by standing on the shoulders of giants."</i></p>

<p>I list here some <b>guidance and expectations</b> from my research students at any level:
<ul>
 <li><mark>First and foremost <b>you</b> are responsible for your career.</mark>
  <ul>
   <li>Supervisors can only give you advice and guidance on how to proceed towards an objective. However, it is you who is <i>behind the wheel</i>, controlling your career.</li>
  </ul>
 </li>
 <li>Incorporate the <b>"Build Something!"</b> principle in your tasks: show something, code a project, state the obvious, improve on it, try other things, get feedback, demonstrate and present your ideas, interact, discuss, implement other things, etc.</li>
 <li>Have good <b>research questions</b>: they guide everything else you do in research.</li>
 <li><b>Produce your own texts and research</b>: authentic research matters to drive your our own ideas. 
  <ul>
   <li>You may use other people's work and tools to learn, but do your <i>own 'thing'</i> and perspective but remember: all will remain published in public domains, you don't want to have it retracted due to technicalities, plagiarism - self-plagiarism included - or other problem.
   </li>
  </ul>
 </li>
 <li>Learn how to profit from <b>good persuasion skills</b>: this calls for creating arguments that can be sustained by data and research outcomes.</li>
 <li><b><i>Collect</i> data; never <i>invent</i> data</b>: you want your research to replicable and reproducible (other teams).
  <ul>
   <li>Plan for collecting data that is useful towards your research question.</li>
   <li>If you invent data or blatantly lie about it, eventually you will be discovered and have your research retracted, which is something completely harmful for your research integrity and reputation.
   </li>
  </ul>
 </li>
 <li>Improve your <b>analytical skills</b>: there's always room for working and perfecting your skills on regards to this matter. Strive to learn new methods and approaches, employ different tools, attempt to reach (same) conclusions several different ways.</li>
</ul>
</p>


<?php
include("postfooter.php");
?>

            </div> <!-- div main -->

          </div> <!-- div columns -->

        </div> <!-- div wrapper -->

<?php
include("footer.php");
?>

</div>
</body>
</html>