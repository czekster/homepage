# -*- coding: utf-8 -*-
"""
Created on Thu Aug 31 08:52:30 2023

@author: stout
"""

import requests
import requests.auth

DEBUG = False

my_user="YOUR USER"
my_pass="YOUR PASS"
my_useragent="MyClient/0.1 by MyName"
client_id='reddit-provides-this'
client_secret='reddit-provides-this'

client_auth = requests.auth.HTTPBasicAuth(client_id, client_secret)
post_data = {"grant_type": "password", "username": my_user, "password": my_pass}
headers = {"User-Agent": my_useragent}
response = requests.post("https://www.reddit.com/api/v1/access_token", auth=client_auth, data=post_data, headers=headers)

#print(response.json())

###########

#response = requests.get("https://oauth.reddit.com/api/v1/me", headers=headers)
#print(response.json())

#testing
##RUN:
# pip install asyncpraw

import praw
from prawcore.exceptions import NotFound

import arrow
import re

##################
LIMIT = 100    # retrieval limit for posts
CROP = 100     # interrupt the process after showing N posts (used for debug)
FILE = True     # redirect output to file

if FILE:
    file1 = open("output.txt", "w", encoding="utf-8")
def main():
    reddit = praw.Reddit(
        client_id=client_id,
        client_secret=client_secret,
        password=my_pass,
        user_agent=my_useragent,
        username=my_user,
        check_for_async=False
    )
    #print(reddit.user.me())

    my_subreddits = [
        "cybersecurity", 
        "ethicalhacking", 
        "malware", 
        "threatintel", 
        "blueteamsec"
    ] # look paper for more subreddits

    regexes = [
        "attack.*", "attacker.*", 
        "cybersecurity.*",
        "security.*",
        "malware.*",
        "threat.*", "threat actor.*",
        "CVE.*", "NVD.*",
        "MS-Windows.*", "Android.*", "MacOS.*", "iOS.*", "GNU-Linux.*", "Linux.*", 
        "vulnerability*"
    ] # you need to have more regexes here!
    combined = "(" + ")|(".join(regexes) + ")"
    
    p=0
    for mysubreddit in my_subreddits:
        subreddit = reddit.subreddit(mysubreddit)
        i=0

        if FILE:
            file1.write("\n\nSubreddit: " + mysubreddit)
        print("Subreddit: " + mysubreddit) 
        #pick one next:
        #submissions = subreddit.stream.submissions() #Streams in PRAW are meant for new posts or comments that were recently submitted to a subreddit
        #submissions = subreddit.new()
        submissions = subreddit.hot(limit=LIMIT) # i'm using this method
        #submissions = reddit.subreddit("all").search(mysubreddit, sort="hot", syntax=None, limit=LIMIT)
            # 'sort' can be one of: "relevance", "hot", "top", "new", or "comments". (default: "relevance").
        for submission in submissions:
            p=p+1
            print("["+str(p)+"] ")   # used just to show progress (ie, whether script is 'stuck' or not)
            if (DEBUG): print("Checking user '" + (submission.author.name if hasattr(submission.author, 'name') else "")  + "'")

            if (hasattr(submission.author, 'name') and        #checks valid user
                user_exists(reddit, submission.author) and    #checks valid user
                submission.stickied == False and              #checks only 'unpinned' submissions ('unstickied')
                re.search(combined, submission.title)): 
                i=i+1
                process_submission(i, submission)
            if (i==CROP):
                break
    if FILE:
        file1.close()   # last command for main() function: it closes the file handler

def user_exists(reddit, name):
    try:
        if hasattr(reddit.redditor(name), 'id'):
            reddit.redditor(name).id
    except NotFound:
        return False
    except AttributeError:
        return False
    return True

def process_submission(i, submission):
    #date = arrow.get(submission.author.created_utc).to('local').format('YYYY-MM-DD')
    if FILE:
        file1.write("\n\nProcessing submission [" + submission.permalink + "]")
        date = arrow.get(submission.author.created_utc).to('local').humanize() if hasattr(submission.author, 'created_utc') else ""
        file1.write("---\nRedditor: " + submission.author.name + 
              " (karma: " + (str(submission.author.comment_karma) if hasattr(submission.author, 'comment_karma') else "") +  ")")
        file1.write("\nSubmission: " + str(i) + ": " + submission.title +
              ", score:" + str(submission.score) +
              ", upvote_ratio: " + str(submission.upvote_ratio) +
              ", num comments: " + str(submission.num_comments) +
              ", date_created: " + date if hasattr(submission.author, 'created_utc') else "" +
              "")
    else:
        print("\n\nProcessing submission [" + submission.permalink + "]")
        date = arrow.get(submission.author.created_utc).to('local').humanize() if hasattr(submission.author, 'created_utc') else ""
        print("---\nRedditor: " + submission.author.name + 
              " (karma: " + (str(submission.author.comment_karma) if hasattr(submission.author, 'comment_karma') else "") +  ")")
        print("\nSubmission: " + str(i) + ": " + submission.title +
              ", score: " + str(submission.score) +
              ", upvote_ratio: " + str(submission.upvote_ratio) +
              ", num comments: " + str(submission.num_comments) +
              ", date_created: " + date if hasattr(submission.author, 'created_utc') else "" +
              "")
    #also interesting: attribute 'comments': Provides an instance of CommentForest.

if __name__ == "__main__":
    main()








