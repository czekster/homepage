<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="generator" content="GitLab Pages">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Ricardo M. Czekster's homepage</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="syntax.css">
	<link rel="stylesheet" href="main.css">
    <link rel="stylesheet" href="style.css">
    <script src="https://platform.linkedin.com/badges/js/profile.js" async defer type="text/javascript"></script>
  </head>

<body class="vsc-initialized">
  <div class="wrapper">
	<div class="columns">
<?php
include("sidebar.php");
?>
	  <div class="main">
		<h1>Reading list</h1>
<table border="0" align="right"><tr><td><img align="right" width="300" src="images/reading.png"><font size="-1"><br/>Source: Copyright: 2009 Yovko Lambrev. (CC BY 3.0)</a></font></td></tr></table>
<p>This page will list interesting reads I have found in the Internet.</p>
<p>Enjoy.</p>
<br/>
<ol>
 <li>General</li>
 <ul>
  <li><a href="http://ranum.com/security/computer_security/editorials/dumb/index.html" target="_blank">The Six Dumbest Ideas in Computer Security</a>&#x2197;</li>
  <li><a href="https://spectrum.ieee.org/the-real-story-of-stuxnet" target="_blank">The Real Story of Stuxnet - IEEE Spectrum</a>&#x2197;</li>
 <!-- <li><a href="link" target="_blank">XXXX</a></li> -->
 </ul>
 <li>Blogs</li>
 <ul>
  <li><a href="https://www.schneier.com/" target="_blank">Schneier on Security</a>&#x2197;</li>
  <li><a href="https://krebsonsecurity.com/" target="_blank">Krebs on Security</a>&#x2197;</li>
  <li><a href="https://www.sans.org/white-papers/" target="_blank">SANS institute Information Security White Papers</a>&#x2197;</li>
 </ul>
 <li>Institutes</li>
 <ul>
  <li><a href="https://www.ncsc.gov.uk/" target="_blank">UK's National Cyber Security Centre</a>&#x2197; (NCSC)</li>
  <li><a href="https://www.enisa.europa.eu/" target="_blank">European Union's Agency for Cybersecurity</a>&#x2197; (ENISA)</li>
 </ul>
 <li>Books</li>
 <ul>
  <li><b><i>This Is How They Tell Me the World Ends: A True Story</i></b> by Nicole Perlroth (Author), 2022</li>
  <li><b><i>The Art of Invisibility: The World's Most Famous Hacker Teaches You How to Be Safe in the Age of Big Brother and Big Data</i></b> by Kevin Mitnick (Author), Mikko Hypponen (Foreword), 2017</li>
  <li><a href="https://www.amazon.co.uk/Bruce-Schneier/e/B000AP7EVS?ref=sr_ntt_srch_lnk_1&qid=1680928136&sr=1-1" target="_blank">Bruce Schneier @ Amazon</a>&#x2197;</li>
 </ul>
</ol>

<?php
include("postfooter.php");
?>

            </div> <!-- div main -->

          </div> <!-- div columns -->

        </div> <!-- div wrapper -->

<?php
include("footer.php");
?>

</div>
</body>
</html>