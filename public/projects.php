<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="generator" content="GitLab Pages">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Ricardo M. Czekster's homepage</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="syntax.css">
	<link rel="stylesheet" href="main.css">
    <link rel="stylesheet" href="style.css">
    <script src="https://platform.linkedin.com/badges/js/profile.js" async defer type="text/javascript"></script>
  </head>

<body class="vsc-initialized">
  <div class="wrapper">
	<div class="columns">
<?php
include("sidebar.php");
?>
	  <div class="main">
		<h1>Projects</h1>
<table border="0" align="right"><tr><td><img align="right" width="300" src="images/research-project.png"><font size="-1"><br/>Source: Copyright: <a href="https://meta.wikimedia.org/wiki/Communications/Audience_research/Research_and_notes#Wikimedia_Audience_Research_Project:_Internal_Synthesis_Workshop_Summary" target="_blank">2016 MKramer</a>. (CC SA 4.0)</a></font></td></tr></table>
<p>This page will list previous and current projects I was/am involved with.</p>
<br/><br/><br/><br/><br/>

<h2>Research</h2>
<ul>
 <li><b>2023</b>: <font color="#0011ee"><b>[PI]</b></font> Dynamic risk assessment for enhancing the cybersecurity posture of IoT networks in smart infrastructure (FAPERGS/RS/BRAZIL, Grant number: 22/2551-0001368-6)</li>
 <ul>
  <li><b>[co-PI]</b> Dr Paul Grace, Aston/UK</li>
  <li>Dr Cesar Marcon, PUCRS/Brazil</li>
  <li>Dr Fabiano Hessel, PUCRS/Brazil</li>
 </ul>
 <li><b>2019-2023</b>: <font color="#0011ee"><b>[member]</b></font> The Active Building Centre Research Programme (EPSRC/UK, Grant number: EP/V012053/1)</li>
 <li><b>2016-2017</b>: <font color="#0011ee"><b>[co-PI]</b></font> Quantitative analysis techniques to improve the efficiency of complex processes in health care (Royal Academy of Engineering/UK, Grant number: NRCP1617/5/62)</li>
 <ul>
  <li><b>[co-PI]</b> Professor Juliana Bowles</li>
 </ul>
 <li><b>2016</b>: <font color="#0011ee"><b>[PI]</b></font> Computational modelling and efficient energy simulation of industrial processes (Edital Universal 2016, CNPq/Brazil, Grant number: 403294/2016-9)</li>
  <li><b>2016-2017</b>: <font color="#0011ee"><b>[member]</b></font> Modeling energy constraints in industrial environments, Brazil</li>
 <li><b>2014-2016</b>: <font color="#0011ee"><b>[PI]</b></font> Modelling and simulation of industrial processes (Six Sigma), Brazil</li>
 <li><b>2011-2013</b>: <font color="#0011ee"><b>[PI]</b></font> Performance Testing of virtualized infrastructures, Brazil</li>
 <li><b>2006-2010</b>: <font color="#0011ee"><b>[member]</b></font> Paleoprospec (Petrobras/PUCRS), Brazil</li>
 <li><b>2008</b>: <font color="#0011ee"><b>[member]</b></font> CODA (Process Algebra for COllective DynAmics), LFCS/Edinburgh, Scotland</li>
 <li><b>2007</b>: <font color="#0011ee"><b>[member]</b></font> DOCCA (Design and Optimization of Collaborative Computing Arch.), LIG/France</li>
</ul>
 

<!--
<h2>Other</h2>
<ul>
 <li></li>
 <li></li>
 <li></li>
</ul>
-->


<?php
include("postfooter.php");
?>

            </div> <!-- div main -->

          </div> <!-- div columns -->

        </div> <!-- div wrapper -->

<?php
include("footer.php");
?>

</div>
</body>
</html>