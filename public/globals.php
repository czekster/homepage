<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

function isLocalhost($whitelist = ['127.0.0.1', '::1']) {
    return in_array($_SERVER['REMOTE_ADDR'], $whitelist);
}

$DEBUG = isLocalhost() ? 1 : 0;
// 1 = testing
// 0 = production

if ($DEBUG == 1)
   error_reporting(E_ALL);
else
   error_reporting(0); // turn off error reporting

// GLOBAL VARS
// GLOBAL CONSTANTS
$SESSION_TIMEOUT = 600; // in seconds (10 min = 600s)

// paging size
$LIMIT = 10;

// Include other files
//equire_once('Profile.php');

function report_error($str) {
   global $DEBUG;
   if ($DEBUG == 1)
      echo ("Error: ".$str."<br>\n");
   else
      echo ("<font color=\"#EE0000\">Internal error.</font><br>\n");
}

function formatDate($date) {
   return gmdate("d/m/Y H:i", strtotime($date));
}

function formatDateSimple($date) {
   if ($date == "0000-00-00 00:00:00") return "";
   return gmdate("d/m/Y", strtotime($date));
}

function currentTime() {
   return gmdate("d/m/Y H:i");
}

?>
