<?php
session_start();

include_once('globals.php');
global $SESSION_TIMEOUT;

if (!isset($_SESSION['user']) || (time() - $_SESSION["login_time_stamp"] > $SESSION_TIMEOUT)) {
	session_destroy();
   session_start();
   $_SESSION['user'] = "nok";
	$_SESSION['message'] = "Invalid user/pass or expired session (10 min of inactivity).";
   header("Location: FYP-projects.php");
} else { //updates time (last seen) in session
   $_SESSION["login_time_stamp"] = time();
   $_SESSION["user"] = "ok";
}
?>