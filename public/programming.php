<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="generator" content="GitLab Pages">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Ricardo M. Czekster's homepage</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="syntax.css">
	<link rel="stylesheet" href="main.css">
    <link rel="stylesheet" href="style.css">
  </head>

<body class="vsc-initialized">
  <div class="wrapper">
	<div class="columns">
<?php
include("sidebar.php");
?>
	  <div class="main">
<h1>Programming</h1>
<table align="right"><tr><td>
<img width="270" src="images/code.png"><br>
Source: <a href="https://commons.wikimedia.org/wiki/File:Programming_code.jpg"> (CC BY-SA 4.0)</a></font>
</td></tr></table>

<p>I am a proficient programmer in C/C++, Java, and Perl. I have been coding in these programming languages for a long time now (I started with Pascal - yes, I'm <i>that</i> old).</p>

<p>Check out my <a href="https://github.com/czekster?tab=repositories" target="_blank">GitHub repositories</a>&#x2197; and my public <a href="https://gitlab.com/experimental335" target="_blank">GitLab projects</a>&#x2197;.</p>

<p>Throughout the years I have been using some <a href="code-snippets.php">code snippets</a> to help my daily tasks. Feel free to use it!</p>

<p>Nowadays, I'm learning more about Python and perhaps Go.</p>

<?php
include("postfooter.php");
?>

            </div> <!-- div main -->

          </div> <!-- div columns -->

        </div> <!-- div wrapper -->

<?php
include("footer.php");
?>

</div>
</body>
</html>