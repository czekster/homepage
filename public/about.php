<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="generator" content="GitLab Pages">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Ricardo M. Czekster's homepage</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="syntax.css">
	<link rel="stylesheet" href="main.css">
    <link rel="stylesheet" href="style.css">
  </head>

<body class="vsc-initialized">
  <div class="wrapper">
	<div class="columns">
<?php
include("sidebar.php");
?>
	  <div class="main">
		<h1>About</h1>
<table border="0" align="right"><tr><td><img align="right" width="300" src="images/aston.png"><font size="-1"><br/>Source: <a href="https://www.flickr.com/photos/ell-r-brown/8235602520"> (CC BY 2.0)</a></font></td></tr></table>
<p>I am a Lecturer in Computer Science at Aston University since Jan/2022.</p>

<p>My previous appointments include eight years teaching computing in Higher Education Institutes (HEI) in Brazil such as Pontifical Catholic University of Rio Grande do Sul (PUC-RS) and Feevale University. 
I have taught modules such as Introduction to Programming, Object Oriented Programming, Performance Evaluation (Modelling & Simulation), Cyber security, and Software Design.</p>

<p>Besides teaching, I have worked after my PhD at Siemens Corporate Research in Princeton (USA) during 2010 and also as Research Associate in Newcastle University (2019-2021).</p>

<p>In terms of research funding, I was awarded international grants and acted as co-PI at the Newton Research Collaboration Programme (grant number NRCP1617/5/62) in 2016/2017, a joint work with Professor Juliana Bowles (University of St Andrews, Scotland) and also on Brazilian funded research projects (Edital Universal 2016 - CNPq - grant number 403294/2016-9). See all my projects <a href="projects.php">in this link</a>.</p>

<p>More information on <a href="https://scholar.google.com.br/citations?user=M4SZcHsAAAAJ&hl=pt-BR" target="_blank">Google Scholar</a>&#x2197;, <a href="https://www.researchgate.net/profile/Ricardo-Czekster" target="_blank">ResearchGate</a>&#x2197; and <a href="https://www.linkedin.com/in/ricardoczekster/" target="_blank">LinkedIn</a>&#x2197;.</p>


<h2>Qualifications in Computer Science</h2>
<p>
<ul>
 <li>PhD: Mar/2006 to Mar/2010</li>
 <li>MSc: Mar/2004 to Jan/2006</li>
 <li>BSc: Mar/1998 to Aug/2002</li>
</ul>
</p>

<h2>Employment</h2>
<p>
<ul>
 <li>Jan/2022 - present: Lecturer in Computer Science, Aston University, Birmingham, United Kingdom</li>
 <li>Aug/2019 to Dec/2021: Research Associate, Newcastle University, Newcastle upon Tyne, United Kingdom</li>
 <li>Fev/2014 to Jan/2019: Associate Lecturer in Computer Science, HEI at Brazil</li>
 <li>Mar/2011 to Jan/2014: Associate Lecturer in Computer Science, HEI/PUCRS at Brazil</li>
 <li>Jun/2010 to Dec/2010: Researcher (Performance Evaluation), Siemens Corporate Research (SCR), Princeton, USA</li>
 <li>Mar/2010 to Jun/2010: Assistant Lecturer in Computer Science, Feevale University, Novo Hamburgo, Brazil</li>
</ul>


<?php
include("postfooter.php");
?>

            </div> <!-- div main -->

          </div> <!-- div columns -->

        </div> <!-- div wrapper -->

<?php
include("footer.php");
?>

</div>
</body>
</html>