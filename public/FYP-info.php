<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="generator" content="GitLab Pages">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Ricardo M. Czekster's homepage</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="syntax.css">
	<link rel="stylesheet" href="main.css">
    <link rel="stylesheet" href="style.css">
  </head>

<body class="vsc-initialized">
  <div class="wrapper">
	<div class="columns">
<?php
include("sidebar.php");
?>
	  <div class="main">
		<h1>FYP information</h1>
<table border="0" align="right"><tr><td><img align="right" width="200" src="images/helping-hand.png"><font size="-1"><br/>Source: <a href="https://commons.wikimedia.org/wiki/File:A_Helping_Hand.jpg"> (CC BY-SA 4.0)</a></font></td></tr></table>
<p>Feel free to propose a project on computing and send me an e-mail.
Check my <a href="FYP-faq.php">FAQ about how to conduct and engage in a successful FYP</a>.</p>

<h3>Fundamentals</h3>
<p>
For the FYP you <b>must</b> choose one option out of the following: 1. work on a phase (or more) of the software lifecycle (i.e., Requirements, Design, Implementation, Verification, or Maintenance); or 2. work with research. For the latter, you will have to detail which research methodology you will use.
</p>
<p>You are expected to conduct a substantial computing project gathering background information from reliable sources and then present your results, where you will be assessed on oral performance (and answers to questions) as well as the writing you provided (the document).</p>
<p>And remember, <b>check whether or not you require an Ethics Approval on your project</b> (<a href="research-faq.php#ethics">more information here</a>).
</p>
<p>
You will need to make sure that you adhere to the dataset's <b>terms and conditions</b> for using the data.
</p>
<p>
It is also <b>your responsibility</b> to check whether you are complying with Aston's regulations on this matter and whether you are acting within the terms and conditions of the dataset and sources used as input.
Very rarely the committee will grant you <i>retrospective ethics approval</i> - you may ask, however, you should have a remarkable reason.</p>

<h3>Basic infrastructure</h3>
<p>
If you are working on a stand-alone solution I encourage you to write a GUI in Java/SWT or Qt (find information about these frameworks).</p>

<p>
Now, if you decide on building a web-page, why don't you give a chance to a basic infrastructure such as Apache+MySQL+PHP using the <a href="https://sourceforge.net/projects/wampserver/" target="_blank">WAMP server</a>&#x2197;?
</p>

<table border="0" cellpadding="3" cellspacing="3">
 <tbody><tr>
  <td rowspan="2" width="1%" bgcolor="#AABBAA">&nbsp;</td>
  <td style="border: 1px dotted red;" bgcolor="#eeeeee"><font color="#cc0000">⇒</font>&nbsp;<font color="#111111">Important</font></td>
 </tr>
 <tr>
  <td style="border: 1px dotted red;" bgcolor="#eeffee">
Download this <a href="materials/simple-website-howto.docx">How-To on how to create a simple application using WAMP</a>
  </td>
 </tr>
</tbody>
</table>

<br/>
<hr width="100%" align="left" color="#91A3AB">
<br/>

<p>If you are interested in more modern infrastructure, why not use VM (suggested use of <a href="https://www.virtualbox.org/" target="_blank">Oracle's VirtualBox</a>&#x2197;), <a href="https://www.docker.com/products/docker-desktop/" target="_blank">Docker Desktop</a>&#x2197; and <a href="https://hub.docker.com/" target="_blank">Docker Hub</a>&#x2197;? You can use all of this in combination with <a href="https://code.visualstudio.com/download" target="_blank">MS-VSCode</a>&#x2197; (Microsoft Visual Studio Code).
</p>
<p>
Furthermore, I suggest to use the <a href="https://www.w3schools.com/w3css/default.asp" target="_blank">W3.CSS</a>&#x2197; for creating visually attractive web-pages.<br>
</p>

<p>Unless you have a strong motive, <b>I advise you <i>not</i> to work with any JS-based frameworks</b> like Angular, React, Vue, Ember, or any other. They usually have a steep learning curve and unless you are already proficient in these frameworks, you will waste valuable time configuring and learning than working on your FYP.</p>

<p>
If you want, access <a href="https://github.com/czekster/" target="_blank">my GitHub repository</a>&#x2197; (especially the <a href="https://github.com/czekster/cyberactive" target="_blank">cyberaCTIve</a>&#x2197; project, which is MySQL/PHP based) for pieces of code that you could incorporate into your project.
</p>

<p>As a final tip, work to improve your <b>Time Management</b> skills: reserve some 2 hours every two to three days (or more!) to work on this, and observe your deadlines!
</p>

<a name="FYP-kit"></a>
<h3>FYP - kit</h3>
<p><mark>First of all, look the <b><i>official documentation (and assessment criteria)</i></b> in the corresponding BlackBoard module information available to your access upon enrollment in FYP.</mark>
</p>

<p>Remember that there are videos explaining how to proper cite and work with the MS-Word and LaTeX templates in BlackBoard - it's strongly suggested that you look at those for learning how to do this in a proper manner.</p>

<p>Download the following items:</p>
<ul>
 <li><a href="materials/project-form.docx">Project form</a> (.docx) with help on items</li>
 <li><a href="materials/demo-template.pptx">Demonstration</a> template (.pptx)</li>
 <li><a href="materials/poster-template.pptx">Poster</a> template (.pptx)</li>
 <li><a href="materials/Report-Name-Year.dotx">Term Report or Report</a> template (.dotx) with observations</li>
 <li>LaTeX project for <a href="materials/Final-Report-Template-Aston.zip">Term Report or Report</a> <b>Overleaf</b> (.zip) with several comments
 <ul><li>Find out more about learning LaTeX in my <a href="FYP.php">FYP main page</a></li></ul>
 </li>
</ul>


<p>This is a general guide, it has to change and adapt to your own ideas. Check out other people's work and your peers as well for insights.</p>

<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>

<?php
include("postfooter.php");
?>

            </div> <!-- div main -->

          </div> <!-- div columns -->

        </div> <!-- div wrapper -->

<?php
include("footer.php");
?>

</div>
</body>
</html>