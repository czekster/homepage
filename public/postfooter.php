<div class="postfooter">

<h2>Social media</h2>
              
  <p>Follow me on <a href="https://www.linkedin.com/in/ricardoczekster/" target="_blank">LinkedIn</a>&#x2197;
    or check out my <a href="https://www.researchgate.net/profile/Ricardo-Czekster" target="_blank">ResearchGate profile</a>&#x2197;.
  </p>

</div>
