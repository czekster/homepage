<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="generator" content="GitLab Pages">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Ricardo M. Czekster's homepage</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="syntax.css">
	<link rel="stylesheet" href="main.css">
    <link rel="stylesheet" href="style.css">
  </head>

<body class="vsc-initialized">
  <div class="wrapper">
	<div class="columns">
<?php
include("sidebar.php");
?>
	  <div class="main">
<h1>Final Year Projects (FYP)</h1>

<p>Please, access more information <a href="FYP-info.php">here</a>.</p>

<p>Let me share one piece of advise that you must consider before embarking on the FYP journey with me:</p>

<p>First and foremost, check out my <a href="research-faq.php">Frequently Asked Questions</a> (FAQ) about <b>Research</b>.</p>

<table border="0" cellpadding="10">
 <tr>
  <td width="1%" bgcolor="#eeffee">&nbsp;</td>
  <td style = "border: 1px solid green;" bgcolor="#eeeeff"><p><b><i>"Is Ricardo M. Czekster a good fit for me??"</i></b></p>
Answer all of these questions (Yes|No): (i) Do you enjoy coding? (ii) Are you a curious independent investigative person? (iii) Do you like working with computing related technologies? (iv) Are you self-motivated to pursue your own objectives?<br/>
If you answer 'No' to <b>any</b> of those questions (especially (i)), then we are unfortunately <b>NOT</b> a good fit and you should find another supervisor.
   <ul>
    <li>What I <b><i>enjoy</i></b> supervising: software based projects, where you code a solution to a problem (stand-alone application or web-based application); dataset generation, processing (sanitising), analysing; working with external APIs to come up with an enriched application; Attack Modelling Techniques (AMT) - modelling, mapping, and evaluating.</li>
    <li>What I <b><i>will not</i></b> supervise: static web pages; anything unrelated to software development.</li>
   </ul>
</td>
 </tr>
</table>
<br/>

<table border="0" align="right"><tr><td><img align="right" width="200" src="images/helping-hand.png"><font size="-1"><br/>Source: <a href="https://commons.wikimedia.org/wiki/File:A_Helping_Hand.jpg"> (CC BY-SA 4.0)</a></font></td></tr></table>
<p>Feel free to propose a project on computing and send me an e-mail.
Check my <a href="FYP-faq.php">FAQ about how to conduct and engage in a successful FYP</a>.</p>

<h3>Supervision</h3>
<p>I enjoy <i>supervising</i> highly motivated, curious, and independent individuals that are self-guided learners and help them in their computing journey. If you are equipped or have a desire to <i>hone</i> these skills, please do get in touch with me.</p>

<p>I am interested in the following topics, mostly with respect to cyber security and dependability:</p>
<ul>
 <li>Combining cyber security, Attack Modelling Techniques (AMT), and risk assessment</li>
 <li>Advanced malware analysis and threat hunting</li>
 <li>Cyber Threat Intelligence (CTI), Threat Intelligence Platforms (TIP) and analysis</li>
 <li>Cyber security posture and risk assessment of varied size organisations and preparedness</li>
 <li>Intrusion detection and models for understanding cyber-attack progression</li>
 <li>A wide range of topics in system dependability and resilience</li>
</ul>

<h3>Writing</h3>
<p>I stress that you don't NEED to read all the below references, you SHOULD though. Writing is something to practice and learn over time. As you are starting to write down your own ideas, it is expected to stumble here and there, so, <i>don't worry!</i>
</p>
<p>I am a strong believer that communicating ideas clearly is <i>crucial</i> for impactful outcomes. Please, read some <a href="tips-for-better-writing.php">tips on writing</a>.</p>

<p>I recommend you to read this book: <a href="https://en.wikipedia.org/wiki/The_Elements_of_Style" target="_blank">The Elements of Style</a>&#x2197;, by William Strunk Jr. and E. B. White. This book will change the way you think and write. It costs about £1 (Amazon's Kindle version). <i>It will be the best £1 you will ever spend!</i></p>

<p><img align="right" width="120" src="images/on-writing.png">A marvellous book called "On Writing" by the master Stephen King is also another valuable source for good writing.</p>
<br><br><br><br><br>

<p>Learning LaTeX from the ground up:
  <ul>
   <li>LaTeX has a <mark><b>steep learning curve</b></mark> - be mindful of this.
    <ul><li>Access the <a href="https://www.overleaf.com/learn" target="_blank">Overleaf Learn</a>&#x2197; website</li></ul>
   </li>
   <li>It is <b>much more scalable</b> than MS-Word, and generates output files (pdf) with higher quality</li>
   <li>Create an account in <a href="https://www.overleaf.com/" target="_blank">Overleaf</a>&#x2197;, then add an empty project, delete the .tex file there, unzip this one here, and upload all files into there, as well as the folders, use CTRL-S to compile the pdf. Look at all .tex files in the project and fill with your information.</li>
   <li><a href="https://www.overleaf.com/learn/latex/Learn_LaTeX_in_30_minutes" target="_blank">Learn LaTeX in 30 min!</a>&#x2197;</li>
   <li>For more information about using tables in LaTeX, <a href=" https://www.overleaf.com/learn/latex/Tables" target="_blank">click here</a>&#x2197;.</li>
  </ul>
</p>

<br/>

<hr class="postfooter">

<h3>Previous FYP</h3>

<ol reversed>
 <li>Hassan Hassan. <b><i>Tool to generate Domain Driven Design artefacts automatically</i></b>, 23/04/2024.</li>
 <li>Adam Morris. <b><i>Automatic Hashing of Text for Quick Searching</i></b>, 23/04/2024.</li>
 <li>Sulaiman Hafiz. <b><i>TabADTool: Attack-Tree diagram generator</i></b>, 30/04/2024.</li>
 <li>Mudassar Suleman. <b><i>Ranking web pages using pagerank algorithms</i></b>, 30/04/2024.</li>
 <li>Syed Ali. <b><i>Facial Attendance System</i></b>, 03/05/2024.</li>
 <li>Soheer Amaan. <b><i>The Gamers LFG</i></b>, 03/05/2024.</li>
<hr>
 <li>Dainora Jakstaite. <b><i>Extracting Cybersecurity Newsworthy Pieces from Twitter API</i></b>, 27/04/2023.</li>
 <li>Deandale Balana. <b><i>Investigation into the Attack Vectors used by APT Groups</i></b>, 27/04/2023.</li>
 <li>Mansukh Basi. <b><i>CYTREND: a tool to convert attack-related data into threat intelligence models</i></b>, 27/04/2023.</li>
 <li>Chanveer Grewal. <b><i>Penetration Testing: a tool to model attacker behaviour for effective cybersecurity defence</i></b>, 27/04/2023.</li>
 <li>Mohammed Rahman. <b><i>CVEAlerts: quickly alerting users as new vulnerabilities surface</i></b>, 25/04/2023.</li>
 <li>Sukhraj Dhillon. <b><i>Risk assessment in Smart Infrastructure</i></b>, 25/04/2023.</li>
<hr>
 <li>Felipe Fritzen. <b><i>Identifying classification models of readmissions in Intensive Care Units (ICU) using data mining techniques</i></b>, 2018.</li>
 <li>Everton Sehnem dos Santos. <b><i>A system for monitoring and remote management of product lines using the OPC standard</i></b>, 2018.</li>
 <li>Felipe Augusto Hertzer. <b><i>System for blocking ads and access control using Mikrotik RouterOS</i></b>, 2018.</li>
 <li>Moisés Rodrigues Machado. <b><i>Detection and proactive anomaly analysis in wireless networks</i></b>, 2018.</li>
 <li>Éverton Nagel. <b><i>Framework prototype for developing multithread mechanisms in microcontrollers </i></b>, 2015.</li>
 <li>Jonathan Bonapace and Vinícius Ribeiro Garcia. <b><i>UCML Designer: a graphic tool for modelling UCML</i></b>, 2013.</li>
 <li>Bruno Franzoi Pilz and Clauter Gatti. <b><i>HAMDROID: A mobile tool for invasion control</i></b>, 2012.</li>
</ol>


<?php
include("postfooter.php");
?>

            </div> <!-- div main -->

          </div> <!-- div columns -->

        </div> <!-- div wrapper -->

<?php
include("footer.php");
?>

</div>
</body>
</html>