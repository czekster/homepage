<?php
session_start();
session_destroy();
$_SESSION['user'] = "";
$_SESSION['message'] = "Invalid or expired session.";
header("Location: index.php");
?>