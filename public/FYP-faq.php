<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="generator" content="GitLab Pages">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Ricardo M. Czekster's homepage</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="syntax.css">
	<link rel="stylesheet" href="main.css">
    <link rel="stylesheet" href="style.css">
  </head>

<body class="vsc-initialized">
  <div class="wrapper">
	<div class="columns">
<?php
include("sidebar.php");
?>
	  <div class="main">
		<h1>Final Year Project FAQ</h1>
<!--<table border="0" align="right"><tr><td><img align="right" width="200" src="images/faq.png"><font size="-1"><br/>Source: <a href="https://www.flickr.com/photos/42931449@N07/5397530925"> (CC BY 2.0)</a></font></td></tr></table>-->

<h3>Questions (Q) and Answers (A)</h3>
<a name="top"></a>
&bull;&nbsp;<a href="#idea">My idea of a FYP is -----------. Would you supervise me?</a><br/>
&bull;&nbsp;<a href="#fit">Is Ricardo a good fit for me?</a><br/>
&bull;&nbsp;<a href="#first">What should I do first?</a><br/>
&bull;&nbsp;<a href="#sdlc">Should I work with one or more software lifecycle phases or with Research?"</a><br/>
&bull;&nbsp;<a href="#citing">What to use for citing previous work in my report?</i></a><br/>
&bull;&nbsp;<a href="#modern">Should I use ----------- as software development process in FYP?</a><br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <font color="#558844">(put any 'modern' approach in the blanks)</font><br/>
&bull;&nbsp;<a href="#papers-cite">Where to find scientific papers to cite?</a><br/>
&bull;&nbsp;<a href="#datasets">Where to find good datasets?</a><br/>
&bull;&nbsp;<a href="#tips">Do you have any tips for me?</a><br/>
&bull;&nbsp;<a href="#no-idea">What to do if I have no idea what to do?</a><br/>
&bull;&nbsp;<a href="#mistakes">What are common mistakes people make in the course of the FYP?</a><br/>
&bull;&nbsp;<a href="#grades">About grades and marking - when and how to worry about it?</a><br/>
&bull;&nbsp;<a href="#writing-tips">Do you have any tips on writing?</a><br/>
&bull;&nbsp;<a href="#usability">Do I need to do <i>usability testing?</i></a><br/>
&bull;&nbsp;<a href="#final-thoughts">Any final thoughts?</a><br/>

<br/>
<hr width="100%" align="left" color="#91A3AB">
<br/>

<a name="idea"></a>
<p><b>Question: <i>"My idea of a FYP is -----------. Would you supervise me?"</i></b></p>
<b>Answer: </b> That's very nice, coming up with your own idea. I only advise you to come with a <i>substantial</i> idea, something that is a FYP. Sometimes students come to me with the idea of creating a static website and call it a FYP. <b>In my view and in accordance with the module requirements, this is *not* a FYP worth pursuing</b>. You will have to code something or research using a methodology. There is no turn around this (at least with me as supervisor). I prefer not to supervise static web pages as FYP - perhaps you could contact someone else? Check your options.<br/>
<br/><a href="#top">top</a><br/>

<br/>
<hr width="100%" align="left" color="#91A3AB">
<br/>

<a name="fit"></a>
<p><b>Question: <i>"Is Ricardo a good fit for me??"</i></b></p>
<b>Answer: </b> Answer these questions: (i) Do you enjoy coding? (ii) Are you a curious independent investigative person? (iii) Do you like working with computing related technologies? (iv) Are you self-motivated to pursue your own objectives?<br/>
If you answer NO to <b>any</b> of those questions (especially (i)), then we are unfortunately NOT a good fit and you should find another supervisor.<br>
What I <i>enjoy</i> supervising: software based projects, where you code a solution to a problem (stand-alone application or web-based application); dataset generation, processing (sanitising), analysing; working with external APIs to come up with an enriched application; Attack Modelling Techniques (AMT) - modelling, mapping, and evaluating.<br>
What I <b>WILL NOT</b> supervise: static web pages; anything unrelated to software development.<br/>
<br/><a href="#top">top</a><br/>

<br/>
<hr width="100%" align="left" color="#91A3AB">
<br/>

<a name="first"></a>
<p><b>Question: <i>"What should I do first?"</i></b></p>
<b>Answer: </b> First of all, think about having <a href="research-faq.php#SMART">S.M.A.R.T. goals</a> throughout. They will help you keeping you grounded to your deliverable.<br>

<br>
Start with small steps. Remember Sir Francis Drake's adage, that goes like <i>"Sic parvis magna"</i> ("From small beginnings come great things"). Work on your idea first of all. It should have the 'proper size', not too small that you can finish really quick, not too long or complex that looks like a M.Sc.<br/>
<br/><a href="#top">top</a><br/>

<br/>
<hr width="100%" align="left" color="#91A3AB">
<br/>

<a name="sdlc"></a>
<p><b>Question: <i>"Should I work with one or more software lifecycle phases or with Research?"</i></b></p>
<p>If you think that pursuing research will not entail programming, well, <b>think</b> again, as perhaps it will involve MORE PROGRAMMING than pursuing the alternative approach, which is to make a tool.</p>

<p>For RESEARCH you need to investigate a core aspect of something that is very new and demanding thorough investigations. Additionally, you need basic scientific training for i) conducting relevant state-of-the-art research (not search) on a topic; ii) work on a specific methodology to answer your Research Question; iii) devise experiments that will validate/refute your approach consistently, and iv) work on a comprehensive discussion about all lessons learned in the process.</p>

<p>Having said this, you need to have <b>strong</b> skills in programming and in research, combining them to reach a valid, useful scientific outcome that is at least publishable in scientific venues.</p>

<p>Over the undergraduate course we do not offer ways of qualifying these skills altogether, that is something you develop over a MSc or a PhD degree. So, I would strongly recommend you to pursuit the "making a tool for the FYP" approach.</p>

<p>Regardless of your choice:</p>
<ol>
 <li>Pick one <mark>EARLY</mark>;</li>
 <li>It is <mark>RESEARCH, not SEARCH</mark>: it's all about pushing the boundaries of <i>scientific knowledge</i> or making sound observations on particular phenomena of interest. It is <b>not</b> about looking Google, Yahoo!, or Wikipedia for answers (you could use, but not <i>only</i> that) in unverified or opinionated blogs, obscure sources, or other non-scientific venues.</li>
</ol>
If you are pursuing research, you will HAVE to follow a methodology and your references must be VERY consistent (both citing and discussing it). <br/>
Usually, research follows these steps (it depends on your chosen <i>methodology</i>):
<ol>
 <li>Hypothesis (finding a good <i>Research Question</i> to work with);</li>
 <li>Design of experiments;</li>
 <li>Execution;</li>
 <li>Results 'extraction';</li>
 <li>Analysis;</li>
 <li>Hypothesis acceptance/refutal documenting your methodology in a scientific report with all related information and data so others may reproduce, replicate, or repeat your research.</li>
</ol>
</p>

<p>Be aware of this and see if it matches your previous training on research.</p>

<p>
One thing you could do is writing a <i>Systematic Literature Review</i> (<a href="https://www.researchgate.net/publication/338037642_Checklist_on_carrying_out_Systematic_Literature_Reviews_in_computing" target="_blank">click here to find out more about it</a>&#x2197;) or following the PRISMA methodology (<a href="materials/PRISMA_2020_kit.zip">download here a PRISMA 2020 'kit'</a> with the workflow and the checklist). <i>Discuss this with your supervisor.</i></p>
<p>
Remind that you have significant training in SDLC and you have spent little time during your undergraduate studies seeking and pursuing research (thus little training). If it is research you want to pursue in your FYP, perhaps you will need to study about research's underpinnings first, and then frame your questions accordingly. <i>Talk to your supervisor.</i></p>

<br/><a href="#top">top</a><br/>

<br/>
<hr width="100%" align="left" color="#91A3AB">
<br/>

<a name="citing"></a>
<p><b>Question: <i>"What to use for citing previous work in my report?"</i></b></p>
<b>Answer: </b> You <i>can</i> have a few Internet links (preferably those whose content will remain for future referencing, ie, it will not 'disappear' over night). However, the best way is to use consistent references from scientific outlets (Google Scholar, Science Direct, Scopus, arXiv - read more about in <a href="#Q5">Q5</a> below). Papers with previous results are welcomed, or references to published books.
Tip: see how other researchers cite (in good venues), how they do, the frequency they cite, and where.<br/>
<br/><a href="#top">top</a><br/>

<br/>
<hr width="100%" align="left" color="#91A3AB">
<br/>

<a name="modern"></a>
<p><b>Question: <i>"Should I use ----------- as software development process in FYP?" (Put any 'modern' approach instead of 'X')</i></b></p>
<b>Answer: </b> It is not a crime to employ good o' Waterfall for delivering a project. Scrum/Kanban or even ScrumBan might be ok to organise ideas and deliverables in bi-weekly fashion, but you could 'adapt' it and have mini Waterfall cycles as well, with design/coding/testing/delivery phases.
Ask yourself: "does it make sense to use -----------?" If so, go along, otherwise, adapt!
<br/>
Reassess your contribution as time passes. If you are wasting too much time in one task and you need more tasks to finish by a deadline, re-evaluate what you are doing and try to move on or adjust. <i>Talk to your supervisor.</i><br/>
<br/><a href="#top">top</a><br/>

<br/>
<hr width="100%" align="left" color="#91A3AB">
<br/>

<a name="papers-cite"></a>
<p><b>Question: <i>"Where to find scientific papers to cite?"</i></b></p>
<b>Answer: </b> You can use the following scientific venues for this purpose (on many venues you may use your Aston credentials through OpenAthens):
<ul>
 <li>Google Scholar: <a href="https://scholar.google.com/" target="_blank">https://scholar.google.com/</a>&#x2197;</li>
 <li>Digital Library - ACM: <a href="https://dl.acm.org/" target="_blank">https://dl.acm.org/</a>&#x2197;</li>
 <li>IEEE Xplore - IEEE: <a href="https://ieeexplore.ieee.org/Xplore/home.jsp" target="_blank">https://ieeexplore.ieee.org/Xplore/home.jsp</a>&#x2197;</li>
 <li>Science Direct: <a href="https://www.sciencedirect.com/" target="_blank">https://www.sciencedirect.com/</a>&#x2197;</li>
 <li>arXiv: <a href="https://arxiv.org/" target="_blank">https://arxiv.org/</a>&#x2197;</li>
 <li>CiteSeer (Computing): <a href="http://citeseerx.ist.psu.edu/" target="_blank">http://citeseerx.ist.psu.edu/</a>&#x2197;</li>
 <li>Semantic Scholar (AI-powered research tool): <a href="https://www.semanticscholar.org/" target="_blank">https://www.semanticscholar.org/</a>&#x2197;</li>
</ul>

Another suggestion is to <b><i>use the library</i></b>. It contains several resources to help you in your FYP journey.<br/>

If you are having trouble citing and referencing other work, one suggestion is to check and see how authors have done in their work (pick a paper and see).<br/>
<br/><a href="#top">top</a><br/>

<br/>
<hr width="100%" align="left" color="#91A3AB">
<br/>

<a name="datasets"></a>
<p><b>Question: <i>"Where to find good datasets?"</i></b></p>
<b>Answer: </b> Look at this link at my <a href="research-faq.php#datasets">Research webpage</a>.<br/>

<br/><a href="#top">top</a><br/>

<br/>
<hr width="100%" align="left" color="#91A3AB">
<br/>

<a name="tips"></a>
<p><b>Question: <i>"Do you have any tips for me?"</i></b></p>
<b>Answer: </b> Yes. For example: 
<ul>
 <li>Do you need permission from <b><i>ethics committee</i></b> to move on? (mind that this process may take a considerable while to return favourably/unfavourably) - talk to your supervisor ASAP about this</li>
 <li>Decide on a <b>platform</b> early on: Desktop, Mobile (App), Web</li>
 <ul>
  <li>For Desktop applications, you will need to choose a GUI, eg, Qt or WX-Widgets (assuming you want a visual GUI, otherwise you could code a CLI - <i>Command Line Interface</i> application</li>
  <li>For Mobile, in MacOS you will have to know/learn Objective-C, for Android, Java</li>
  <li>For Web, a framework (the simplest one being <a href="https://www.w3schools.com/w3css/default.asp" target="_blank">W3.CSS</a>&#x2197;, a quality alternative to <i>Bootstrap</i> - check also their <i>tutorials</i> on CSS), also another, perhaps JS based like Angular</li>
 </ul>
  <li>Work to have a deliverable and <i>AT THE SAME TIME</i> work on the report (they go together). Remember the <b>weights</b> for each part (check <a href="#Q9">Q9</a>). Start drafting the structure and writing high level things you want to say in specific parts.</li>
  <li>Check your <b>progress</b> every once in a while to see if you are in line with the objectives you have set. <i>Ask yourself</i>:</li>
  <ul>
   <li>Have you finished what you proposed? (can you afford to add this new feature?)</li>
   <li>Do you have time for coding new, <i>unanticipated</i> things?</li>
   <li>What you have finished (main deliverable), is in good measure of the expected outcomes of a FYP?</li>
   <li>Has your tool been tested? What about <i>usability testing</i>?</li>
  </ul>
  <li>When you think you have a <b>stable version</b> (of both the <i>deliverable</i> and the <i>report</i>), give it to someone else to run (the application) and the report. They might find problem that you probably haven't.</li>
</ul>
<br/><a href="#top">top</a><br/>

<br/>
<hr width="100%" align="left" color="#91A3AB">
<br/>

<a name="no-idea"></a>
<p><b>Question: <i>"What to do if I have no idea what to do?"</i></b></p>
<b>Answer: </b> One suggestion is to look at opportunities for improving systems you deem need fixing.
Also, you can:
<ul>
 <li>Write a <b>plug-in</b> for Chrome/Firefox</li>
 <li>Continue a different venue of <b>previous FYP projects</b> (read their "Future Work" section)</li>
 <ul>
  <li><b>Combine</b> two ideas from other two FYPs</li>
  <li>Employ a different (but comparable) technology/approach from another FYP and analyse outputs</li>
 </ul>
 <li>Improve an <b>open source software project</b> or add a feature</li>
 <li>Work on something related to learning how to code, using visual approaches to teach programming</li>
 <li>You could work on a simple <b>simulation</b> of a reality, changing parameters to learn about interesting things. You could model a situation that you deem important/relevant. Create a virtual representation of something in reality worth studying</li>
 <li>You could <b>parallelise an application</b> using distributed computing primitives. Run it on a GPU, compare with the sequential/automatic parallel counterpart</li>
 <li>What about looking at some <b>characteristic</b> of Open Source Software (OSS)? Like the amount of repeated code in projects or how many times something was copied/pasted from StackOverflow?</li>
</ul>
<br/><a href="#top">top</a><br/>

<br/>
<hr width="100%" align="left" color="#91A3AB">
<br/>

<a name="mistakes"></a>
<p><b>Question: <i>"What are common mistakes people make in the course of the FYP?"</i></b></p>
<b>Answer: </b> Sadly, this happens more often than not. But you can learn from others and prevent them from happening to you. Examples are:
<ul>
 <li><b><i>Steep learning curve for technology X</i></b>: it takes a while to learn how to use a technology. Sometimes, the time it takes is overwhelmingly high. Start simple and work on fast prototypes before incorporating new technologies. Make things simple the first version, then add more complexities.</li>
 <li><b><i>Deciding unilateraly on big decisions concerning your FYP</i></b>: supervisors should not micromanage your every action, however, they must be in pair with big decisions that could impact your estimated delivery. For these instances, you should check with your supervisor so you decide how to best approach these new ideas.</li>
 <li><b><i>Failing to understand the difference between 'search' and 'research'</i></b>: Research is about discovery and exploration in a formal and robust manner. It is perceptible for assessors that usually work with research in their daily lives that your deliverable is not research-based, and that may reflect on your final marking. Just be aware of this throughout your FYP journey.</li>
 <li><b><i>Taking too long for dealing with ethics issues</i></b>: these processes take a while. Not having their agreement may cause you to postpone your scheduled report delivery and project demonstration.</li>
 <li><b><i>Disappearing</i></b>: only consulting your supervisor by the very end of the project, weeks before submitting the report/demonstration.</li>
<!-- <li><b><i>add</i></b>: </li> -->
</ul>
<br/><a href="#top">top</a><br/>


<br/>
<hr width="100%" align="left" color="#91A3AB">
<br/>

<a name="grades"></a>
<p><b>Question: <i>"About grades and marking - when and how to worry about it?"</i></b></p>
<b>Answer: </b> First, think about having a good idea and how to proper code, test and analyse a system the proper and effective way. Then think about marking afterwards. Start by accessing, reading, and overviewing the assessment criteria for the FYP. Follow it if you aim for having a good mark prospect. That's how you will be evaluated by the committee. You'll see that the REPORT corresponds to 70% of your grade (yes, it does). And 30% goes to your deliverable. Look for previous work online, see how they cite previous work, learn from others.<br/>
<br/><a href="#top">top</a><br/>


<br/>
<hr width="100%" align="left" color="#91A3AB">
<br/>

<a name="writing-tips"></a>
<p><b>Question: <i>"Do you have any tips on writing??"</i></b></p>
<b>Answer: </b> Look my <a href="tips-for-better-writing.php">writing tips</a> for a start.<br>
<br>
Writing is an art, one that you should not be afraid or think too much about. What you need to do to start writing well (or better) is to write often and assess the quality of your writing. Look at each individual sentence: does it look coherent? The concepts you are using, have they been introduced beforehand? How is your flow of ideas? Are they in logical order so the audience can grasp it?<br/>
<br/>
Good writing is about working on a good narrative to explain your ideas in a logical matter. The way you organise your thoughts to convey the things you want the audience to learn about. It's a bit like telling a joke: you have a setting, a situation you expose, a punch line (and perhaps laughs afterwards, depends on your skill). It's the same with writing a document, a report, a dissertation, a thesis. You need to have a good plan for how you will flow everything throughout your sentences, your paragraphs, your sections, your chapters.<br/>
<br/>
One technique is to start with the broad structure (major sections), then adding items that you need to write about (each item will perhaps become a paragraph, full of sentences). This will give you an idea of the things you want to address in the work. Look at each sentence trying to see whether it's too long (or too short) - they must be the right size, one that conveys a single idea clearly and precisely.<br/>
<br/>
The power of editing: editing is re-reading all over again (perhaps after some time). By editing, you find problems in your previous thinking. You should read your work like an assessor going through your material, with an analytical eye. Use a red marker to highlight <i>odd</i> parts needing improvement.<br/>
<br/><a href="#top">top</a><br/>

<br/>
<hr width="100%" align="left" color="#91A3AB">
<br/>


<a name="usability"></a>
<p><b>Question: <i>"Do I need to do <i>usability</i> testing?"</i></b></p>
<b>Answer: </b> We are building software applications and tackling the SDLC. Our aim is to eventually have users working on these applications. So, yes, if you are building an application and you want end-user feedback, you will require a usability testing. Remind that for this you will need to engage with <b>Ethics Committee</b> for outlining and documenting the interaction with users (and filling forms and authorisations and so on). Plan for this as soon as you can because it takes a while to set everything up properly.<br/>

<p>In terms of <i>participants number</i> you must know for a fact that is not easy to recruit participants and that there is no <i>good answer</i> for this question. You can do a remarkable and insightful usability test with about five users and sometimes, even doing for 100 users, the feedback quality is not desirable. My recommendation is to gage the responses and their quality and make an assessment as to what you have learned to improve your tool and decide whether to keep recruiting or stop altogether.</p>
<br/><a href="#top">top</a><br/>

<br/>
<hr width="100%" align="left" color="#91A3AB">
<br/>


<a name="final-thoughts"></a>
<p><b>Question: <i>"Any final thoughts?"</i></b></p>
<b>Answer: </b> Have fun.<br/>
<br/><a href="#top">top</a><br/>

<br/>
<hr width="100%" align="left" color="#91A3AB">
<br/>

<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>

<?php
include("postfooter.php");
?>

            </div> <!-- div main -->

          </div> <!-- div columns -->

        </div> <!-- div wrapper -->

<?php
include("footer.php");
?>

</div>
</body>
</html>