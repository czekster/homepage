<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="generator" content="GitLab Pages">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Ricardo M. Czekster's homepage</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="syntax.css">
	<link rel="stylesheet" href="main.css">
    <link rel="stylesheet" href="style.css">
  </head>

<body class="vsc-initialized">
  <div class="wrapper">
	<div class="columns">
<?php
include("sidebar.php");
?>
	  <div class="main">
		<h1>Interests</h1>
<p>First and foremost, I like to read various subjects. To get things started here, I will recommend <a href="https://standardebooks.org/" target="_blank">the Standard E-books</a>&#x2197; website and <a href="https://www.goodreads.com/" target="_blank">Goodreads</a>&#x2197; to find good books to put in the reading pipeline.</p>


<table border="0" cellpadding="10">
 <tr>
  <td width="1%" bgcolor="#eeffee">&nbsp;</td>
  <td style = "border: 1px dashed green;" bgcolor="#eeeeff"><p>I am interested in improving my writing skills continuously, so do check out some <a href="tips-for-better-writing.php">tips for writing better</a>.</p></td>
 </tr>
</table><br/>

<table border="0" cellpadding="10">
 <tr>
  <td style = "border: 1px dashed green;" bgcolor="#eeeeff"><p>Then, I will recommend you to play a little with programming in the <a href="https://www.hackerrank.com/" target="_blank">Hacker Rank</a>&#x2197; website. It has a wealth of projects to improve one's programming skills and software design capabilities.</p></td>
  <td width="1%" bgcolor="#eeffee">&nbsp;</td>
 </tr>
</table><br/>

<table border="0" cellpadding="10">
 <tr>
  <td width="1%" bgcolor="#eeffee">&nbsp;</td>
  <td style = "border: 1px dashed green;" bgcolor="#eeeeff"><p>In terms of news commentary related to technology, I usually read <a href="https://news.ycombinator.com/" target="_blank">HackerNews</a>&#x2197;.</p></td>
 </tr>
</table><br/>

<table border="0" cellpadding="10">
 <tr>
  <td style = "border: 1px dashed green;" bgcolor="#eeeeff"><p>Have you checked <a href="https://thelifeengine.net/" target="_blank">"The Life Engine"</a>&#x2197; or <a href="https://boredhumans.com/falling_sand.php" target="_blank">The Falling Sand Game</a>&#x2197;? - <i>disclaimer:</i> completely addictive!</p></td>
  <td width="1%" bgcolor="#eeffee">&nbsp;</td>
 </tr>
</table><br/>


<p>I enjoy studying <i>basic</i> (yet powerful) Markov Chains (CTMC/DTMC) modelling and solution methods. I have <a href="https://github.com/czekster/markov" target="_blank">written a <i>short</i> book about this subject</a>&#x2197;, I urge you to take a look and let me know what you think. The GitHub repository has the book for (free) download, coding challenges, and models (in C, MATLAB and PRISM) for validation.</p>

<h3>Hobbies</h3>
<p>I am a professional <i><a href="https://www.dictionary.com/browse/putter" target="_blank">Putterer</a>&#x2197;</i>.<br/><br />

<img src="images/puttering.png" align="right" width="300"></p>

<i>"Do what you can, with what you have, where you are."</i>
- Theodore Roosevelt<br />

<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>

<?php
include("postfooter.php");
?>

            </div> <!-- div main -->

          </div> <!-- div columns -->

        </div> <!-- div wrapper -->

<?php
include("footer.php");
?>

</div>
</body>
</html>