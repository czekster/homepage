<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="generator" content="GitLab Pages">
    <meta name="keywords" content="ricardo melo czekster, czekster, cyber security"/>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Ricardo M. Czekster's homepage</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="syntax.css">
	<link rel="stylesheet" href="main.css">
    <link rel="stylesheet" href="style.css">
  </head>

<body class="vsc-initialized">
  <div class="wrapper">
	<div class="columns">
<?php
include("sidebar.php");
?>
	  <div class="main">
		<h1>Contact</h1>

 <table border="0" width="100%" cellspacing="0" cellpadding="0">
  <tr>
   <td width="10%">
    <img src="images/rmc2.jpg" width="150" border="0"><br/>
   </td>
   <td width="90%">
Poeminho do Contra<br>
<br>
<i>
Todos esses que aí estão<br>
Atravancando meu caminho,<br>
Eles passarão...<br>
Eu passarinho!<br>
</i>
<b>Mario Quintana</b><br>
In: Caderno H, Mario Quintana: Poesia Completa, Editora Nova Aguilar, p. 257.<br>
   </td>
  </tr>
  <tr>
   <td colspan="2">
      <table border="0" cellpadding="5" cellspacing="5" width="100%">
       <tr>
        <td width="1%" bgcolor="#eeffee">&nbsp;</td>
        <td style = "border: 1px solid green;" bgcolor="#eeeeff">

         <table border="0">
          <tr>
           <td>
            <span style="white-space: pre-line"><p id="info">Ricardo Melo Czekster
College of Engineering and Physical Sciences (EPS)
School of Computer Science and Digital Technologies
Department of Software Engineering and Cybersecurity
Aston University
Room MB211D
Phone +44 (0) 121 204 4544
B4 7ET
Birmingham, UK</p></span>
           </td>
           <td valign="bottom">
            <button style="width: 70px" onclick="copyContent()">Copy!</button>
           </td>
          </tr>
         </table>

      </td>
       </tr>
      </table>
   </td>
  </tr>
 </table>

<p>
<img src="images/contact.png" width="400">
</p>




<?php
include("postfooter.php");
?>

          </div>

        </div>

<?php
include("footer.php");
?>

</div>

<script>
  let text = document.getElementById('info').innerHTML;
  const copyContent = async () => {
    try {
      await navigator.clipboard.writeText(text);
      console.log('Content copied to clipboard');
    } catch (err) {
      console.error('Failed to copy: ', err);
    }
  }
</script>
</body>
</html>