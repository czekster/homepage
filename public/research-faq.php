<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="generator" content="GitLab Pages">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Ricardo M. Czekster's homepage</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="syntax.css">
	<link rel="stylesheet" href="main.css">
    <link rel="stylesheet" href="style.css">
  </head>

<body class="vsc-initialized">
  <div class="wrapper">
	<div class="columns">
<?php
include("sidebar.php");
?>
	  <div class="main">
		<h1>Research FAQ</h1>

<a name="top"></a>
<h2>Questions</h2>
&bull;&nbsp;<a href="#begin">Where to begin?</a><br>
&bull;&nbsp;<a href="#SMART">What are S.M.A.R.T. goals?</a><br>
&bull;&nbsp;<a href="#scientific-method">How to do Science the proper way?</a><br>
&bull;&nbsp;<a href="#research-methods">What are Research Methods?</a><br>
&bull;&nbsp;<a href="#writing-doc">How to write my document?</a><br>
&bull;&nbsp;<a href="#datasets"><mark>I don't have any datasets to work, what can I do?</mark></a><br>
&bull;&nbsp;<a href="#ethics">Do I need to fill out the Ethics forms?</a><br>
&bull;&nbsp;<a href="#llm">Can I use LLM/AI-generators (eg, ChatGPT)?</a><br>
&bull;&nbsp;<a href="#meetings">How to prepare for meetings?</a><br>
&bull;&nbsp;<a href="#ai-ml-algo">Which AI/ML algorithm should I employ in my research?</a><br>
<!--&bull;&nbsp;<a href="#"></a><br>-->


<!-- question -->

<br><br><hr style="width:50%;text-align:left;margin-left:0;color:#ddeeff">
<a name="begin"></a>
<h2>Where to begin?</h2>
<p>
FIRST of all, to work with research and answer "where to begin?", start by finding a research topic or theme that motivates and interests you.
</p>
<p>
THEN, you should start by having a 'guiding question', or what we call, a 'research question'. What <b><i>specifically</i></b> interests you in this field?
</p>
<p>
What question are you trying to answer here with supporting scientific literature (based on <a href="https://scholar.google.com.br/" target="_blank">Google Scholar</a>&#x2197;, <a href="https://www.scopus.com/" target="_blank">Scopus</a>, <a href="https://www.sciencedirect.com/" target="_blank">Science Direct</a>&#x2197;, <a href="https://dl.acm.org/" target="_blank">Portal ACM - Digital Library</a>&#x2197;, or <a href="https://ieeexplore.ieee.org/" target="_blank">IEEExplore</a>&#x2197;, and other scientific venues)? 
</p>
<p>
You should think really hard about this and then let's reconvene. 
</p>
<p>
It was very good that you find the topic, the theme interests you. Now, WITHIN the theme, we need to further clarify what we are trying to discover/unveil/understand, and then how to substantiate with evidence (related work).
</p>

<p>Check out <a href="FYP-faq.php#first">this answer</a> at my FYP FAQ.</p>

<p>Read materials about <a href="research-highlights.php">Scientific Research</a> principles.</p>

<a href="#top">Go back to questions</a><br>

<!-- question -->

<br><br><hr style="width:50%;text-align:left;margin-left:0;color:#ddeeff">
<a name="SMART"></a>
<h2>What are S.M.A.R.T. goals?</h2>
<p>
This is used when you are setting your objectives. More information <a href="https://www.atlassian.com/blog/productivity/how-to-write-smart-goals" target="_blank">here</a>&#x2197;.
</p>
<p>
They are meant to be:
<ul>
  <li><font size="=1"><b>S</b>pecific.</font>
   <ul>
    <li>What needs to be accomplished?</li>
    <li>Who’s responsible for it?</li>
    <li>What steps need to be taken to achieve it?</li>
   </ul>
  </li>
  <li><font size="=1"><b>M</b>easurable.</font>
   <ul>
    <li>Quantify your goal.</li>
   </ul>
  </li>
  <li><font size="=1"><b>A</b>chievable.</font>
   <ul>
    <li>Is your objective something you can reasonably accomplish?</li>
   </ul>
  </li>
  <li><font size="=1"><b>R</b>elevant.</font>
   <ul>
    <li>Why are you setting the goal that you're setting?</li>
   </ul>
  </li>
  <li><font size="=1"><b>T</b>ime-bound.</font>
   <ul>
    <li>What's your time horizon?</li>
    <li>When will you start creating and implementing the tasks you've identified?</li>
    <li>When will you finish?</li>
   </ul>
  </li>
</ul>
</p>

<a href="#top">Go back to questions</a><br>

<!-- question -->

<br><br><hr style="width:50%;text-align:left;margin-left:0;color:#ddeeff">
<a name="scientific-method"></a>
<h2>How to do Science the proper way?</h2>
<p>
In a nutshell, you will follow the <a href="https://en.wikipedia.org/wiki/Scientific_method" target="_blank">Scientific Method</a>&#x2197;.
</p>
<table border="0" cellpadding="10">
 <tr>
  <td width="1%" bgcolor="#eeffee">&nbsp;</td>
  <td style = "border: 1px dashed green;" bgcolor="#eeeeff"><p><i>The term "scientific method" emerged in the 19th century, as a result of significant institutional development of science, and terminologies establishing clear boundaries between science and non-science, such as "scientist" and "pseudoscience", appearing.</i></p></td>
 </tr>
</table><br/>
<p align="center">
<a href="images/scientific-method.png"><img src="images/scientific-method.png"></a><br>
<font size="-2">Image credit: <a href="https://www.sciencebuddies.org/image-credit?id=5084" target="_blank">created by Amy Cowen for Science Buddies / Science Buddies</a></font>

</p>
<p>
The idea is to work on defining a problem, looking at what other researchers have done, devising your hypothesis, working on experiments that validate or refuse your hypothesis, keeping on a loop case answers do not satisfy you, until you are satisfied where you communicate your findings to the scientific community or other audiences.</p>
<p align="center">
<img src="images/scientific-method-loop.png" width="50%"><br>
</p>
<p>Ideally, your quest will discover things, in other cases, your experimentation might help other researchers from taking the steps you took, and devising new ways of reaching out the <b><i>objective truth</i></b>.
</p>

<a href="#top">Go back to questions</a><br>


<!-- question -->

<br><br><hr style="width:50%;text-align:left;margin-left:0;color:#ddeeff">
<a name="research-methods"></a>
<h2>What are Research Methods?</h2>
<p>
Here's a list of some Research Methods you could use:
</p>

<a href="images/research-methods.png"><img src="images/research-methods.png" width="50%"></a><br/>
<font size="-1">[click to enlarge]</font><br/>
<br>
<a href="#top">Go back to questions</a><br>



<!-- question -->

<br><br><hr style="width:50%;text-align:left;margin-left:0;color:#ddeeff">
<a name="writing-doc"></a>
<h2>How to write my document?</h2>
<p>
You will have the following basic structure:
</p>

<p align="center">
<a href="images/writing-process.jpg"><img src="images/writing-process.jpg" width="50%"></a><br/>
<font size="-1">[click to enlarge]</font><br/>
</p>
<p>
This is not <b><i>written in stone</i></b>, you can change here and there. For instance, instead of methods, you could call "Methodology", and also put "Discussions" in a sub-section within "Results", and so on.</p>
<p>Note that in Computer Science, it is common to have a section called "State-of-the-Art" or "Related work" (not in figure), where you put previous related research with comments on limitations/strengths.</p>

<p><mark>Get inspiration from other researcher's work.</mark></p>
<p>
Tips:
<ul>
 <li>Argue your point, showing limitations and strengths;</li>
 <li>Discuss in depth, not superficially;</li>
 <li>Refrain from using/citing blogs, commentary on the Internet, links that won't sustain the test of time;</li>
</ul>

<a href="#top">Go back to questions</a><br>

<!-- question -->

<br><br><hr style="width:50%;text-align:left;margin-left:0;color:#ddeeff">
<a name="datasets"></a>
<h2>I don't have any datasets to work, what can I do?</h2>

<p>First things first: you need to get familiar with licensing models (and if they involve human related data, application to Ethics - <a href="#ethics">see related question about this topic</a>).</p>
<ul>
 <li>Licenses (more information at <a href="https://www.imperial.ac.uk/research-and-innovation/support-for-staff/scholarly-communication/research-data-management/sharing-data/licensing-your-data/" target="_blank">this link at Imperial</a>&#x2197;):</li>
 <ul>
  <li><a href="https://creativecommons.org/share-your-work/cclicenses/" target="_blank">Creative Commons (CC)</a>&#x2197;: There are six main license types. The most permissive license - and therefore the license most suited for enabling data sharing and reuse - is the Creative Commons Attribution (CC BY Attribution).</li>
  <li><a href="https://opensource.org/license/mit" target="_blank">MIT</a>&#x2197;: permits any person to use, copy, modify, merge, publish distribute, sublicense, and/or sell copies of the software as long as a copy of the license notification is included with any reuse.</li>
  <li><a href="https://www.gnu.org/licenses/gpl-3.0.en.html" target="_blank">GNU General Public License</a>&#x2197;: users can copy, distribute, and modify the software as long as any modifications are also licensed under the GPL.</li>
  <li><a href="https://www.apache.org/licenses/" target="_blank">Apache</a>&#x2197;: allows users to use the software for any purpose, to distribute it, to modify it, and to distribute modified versions of the software as long as a copy of the license is redistributed with any modified software.</li>
  <li><a href="https://opendatacommons.org/licenses/dbcl/1-0/" target="_blank">Database Contents License (DbCL) v1.0</a/>&#x2197;</li>
 </ul>
</ul>

<p>You will find next a non-comprehensive list of datasets for research (or other purposes, like learning how to tackle large datasets, and so on).</p>
<ul>
 <li>General purpose</li>
 <ul>
  <li><a href="https://www.kaggle.com/datasets" target="_blank">Kaggle</a>&#x2197; (general link for datasets)</li>
  <li><a href="https://www.data.gov.uk/" target="_blank">Find open data</a>&#x2197; (UK)</li>
  <li><a href="https://data.worldbank.org/" target="_blank">World Bank Open Data</a>&#x2197;</li>
  <li><a href="https://registry.opendata.aws/" target="_blank">Registry of Open Data on AWS</a>&#x2197; (Amazon Web Services)</li>
 </ul>
 <li>Cyber security related</li>
 <ul>
  <li><a href="https://github.com/gfek/Real-CyberSecurity-Datasets" target="_blank">Real CyberSecurity-Datasets</a>&#x2197;</li>
  <li><a href="https://github.com/shramos/Awesome-Cybersecurity-Datasets" target="_blank">Awesome-Cybersecurity-Datasets</a>&#x2197;</li>
  <li><a href="https://github.com/rshipp/awesome-malware-analysis" target="blank">Awesome Malware Analysis</a>&#x2197;</li>
  <li><a href="https://github.com/hslatman/awesome-threat-intelligence" target="_blank">Awesome Threat Intelligence</a>&#x2197; (curated datasets)</li>
  <li><a href="http://www.sysnet.ucsd.edu/projects/url/" target="_blank">Detecting Malicious URLs</a>&#x2197;</li>
  <li><a href="https://csr.lanl.gov/data/cyber1/" target="_blank">Comprehensive, Multi-Source Cyber-Security Events</a>&#x2197; (requires explanations on how it will be used)</li>
 </ul>
</ul>

<p>Previous MSc students have (successfully) worked with the following <b>secondary</b> datasets:</p> 
<ul>
 <li><a href="https://zenodo.org/records/6463389" target="_blank">HIKARI-2021 dataset</a>&#x2197;</li>
 <ul>
  <li>Paper: <a href="" target="_blank">https://www.mdpi.com/2076-3417/11/17/7868</a>&#x2197;</li>
  <li>License: CC-Attribution 4.0</li>
 </ul>
 <li><a href="https://www.kaggle.com/datasets/mlg-ulb/creditcardfraud" target="_blank">Credit Card Fraud Detection</a>&#x2197;</li>
  <ul>
   <li>Papers: find a list of papers in the link</li>
   <li>License: dbcl-1.0</li>
 </ul>
 <li><a href="https://github.com/BunsenFeng/TwiBot-20" target="_blank">TwiBot-20 Dataset</a>&#x2197;</li>
  <ul>
   <li>Paper: <a href="https://arxiv.org/abs/2106.13088" target="_blank">https://arxiv.org/abs/2106.13088</a>&#x2197;</li>
   <li>License: MIT</li>
 </ul>
 <li><a href="https://www.kaggle.com/datasets/chethuhn/network-intrusion-dataset" target="_blank">Network Intrusion dataset (CIC-IDS- 2017)</a>&#x2197; @ Kaggle</li>
  <ul>
   <li>Link: <a href="https://www.unb.ca/cic/datasets/ids-2017.html" target="_blank">https://www.unb.ca/cic/datasets/ids-2017.html</a>&#x2197;</li>
   <li>License: CC0</li>
 </ul>
 <li><a href="https://github.com/boschresearch/anno-ctr-lrec-coling-2024" target="_blank">AnnoCTR: Annotated Cyber Threat Reports dataset</a>&#x2197;</li>
  <ul>
   <li>Paper: <a href="https://arxiv.org/abs/2404.07765" target="_blank">https://arxiv.org/abs/2404.07765</a>&#x2197;</li>
   <li>License: CC-Attribution 4.0</li>
 </ul>
</ul>


<a href="#top">Go back to questions</a><br>

<!-- question -->

<br><br><hr style="width:50%;text-align:left;margin-left:0;color:#ddeeff">
<a name="ethics"></a>
<h2>Do I need to fill out the Ethics forms?</h2>
<p>First of all, a clarification of terminology:</p>
<ul>
  <li><b>Primary dataset</b> refers to the first hand data gathered by the researcher himself.</li>
  <li><b>Secondary dataset</b> is data that a researcher has not collected or created themselves. </li>
</ul>

<p>There are (at least) three situations where you <b>must</b> fill out the forms:</p>
<ol>
  <li>You are <i>generating</i> a dataset involving humans, ie, containing human subject data.</li>
  <li>You are <i>working</i> with a dataset involving humans, idem.</li>
  <li>You would like to do <i>usability testing</i> with humans.</li>
</ol>

<p>At <b>any</b> case, <mark>you must always check the dataset's <b>license</b></mark> to see whether or not you can use it.</p>

<p>Doing research without proper ethical approval could result in disciplinary action, and even invalidate your research altogether, so please, take it very seriously.</p>

<a href="#top">Go back to questions</a><br>

<!-- question -->

<br><br><hr style="width:50%;text-align:left;margin-left:0;color:#ddeeff">
<a name="llm"></a>
<h2>Can I use LLM/AI-generators (eg, ChatGPT)?</h2>
<p>
Yes, to <b><i>help</i></b> you (just as a calculator helps you), not to do the whole writing process <i>for</i> you.
</p>
<p><mark>If you get caught, it can have harmful consequences, just as any <b>plagiarism</b>.</mark></p>
<p>
These LLMs are helpful to help you clarify your ideas, and so on, however, the <i><b>original thinking</b></i> should be done <b>by you</b>.
</p>
<p>
Remember, it is YOU that will have to defend your ideas, so you might as well actually engage with your topic in a serious manner.
</p>


<a href="#top">Go back to questions</a><br>

<!-- question -->

<br><br><hr style="width:50%;text-align:left;margin-left:0;color:#ddeeff">
<a name="meetings"></a>
<h2>How to prepare for meetings?</h2>
<p>
First and foremost, ask yourself:
<ul>
  <li><i>"Is this meeting <b>really necessary</b>?"</i></li>
  <li><i>"Can I try to work on my doubts <b>before</b> asking for a meeting?"</i></li>
</ul>
</p>
<p>Only then ask for a meeting, either <mark>face-to-face</mark> or <mark>on-line</mark> (be especific on your request).
</p>
<p><mark>Send your availability by e-mail beforehand</mark>: select up to three day/time slots over the working week (Mon-Fri) during business hours (9am-5pm).
</p>
<p><b>Time</b> is a limited resource for anyone, so before the meeting, <b>prepare</b> by adhering to the following:
</p>
<ul>
   <li>Be on time.</li>
   <li>If you can't make it, notify <b>before</b> the meeting, not <i>afterwards</i>.</li>
   <li>Have with you <i>specific</i> questions or send them by e-mail before the meeting.</li>
   <li>Make notes during the meeting with action items, ie, things to do.</li>
</ul>


<a href="#top">Go back to questions</a><br>

<br><br><hr style="width:50%;text-align:left;margin-left:0;color:#ddeeff">
<a name="ai-ml-algo"></a>
<h2>Which AI/ML algorithm should I employ in my research?</h2>
<p>Look at this self-explanatory image:</p>
<a href="images/ML-algorithms.png" target="_blank"><img src="images/ML-algorithms.png" width="60%"></a><br>

<br>
<a href="#top">Go back to questions</a><br>


<!-- question -->

<!--
<br><br><hr style="width:50%;text-align:left;margin-left:0;color:#ddeeff">
<a name="name"></a>
<h2>question?</h2>
<p>
</p>

<a href="#top">Go back to questions</a><br>

-->

<br><br><br><br><br><br><br><br><br><br><br><br><br>
<?php
include("postfooter.php");
?>

            </div> <!-- div main -->

          </div> <!-- div columns -->

        </div> <!-- div wrapper -->

<?php
include("footer.php");
?>

</div>
</body>
</html>
