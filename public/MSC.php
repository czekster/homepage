<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="generator" content="GitLab Pages">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Ricardo M. Czekster's homepage</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="syntax.css">
	<link rel="stylesheet" href="main.css">
    <link rel="stylesheet" href="style.css">
  </head>

<body class="vsc-initialized">
  <div class="wrapper">
	<div class="columns">
<?php
include("sidebar.php");
?>
	  <div class="main">
<h1>MSc. in Computer Science</h1>

<p>
Here is all the information about the pursuit of a Master degree in Computer Science (various degrees).</p>
<p>
The idea is to develop a <i>deliverable</i> that has implications (ie, is <b>useful</b>) in industry or in academia.</p>
<br>
<p>Please, do contact me for more information on how we could engage.</p>

<p>If you have questions, why don't you consult my <a href="research-faq.php">Frequently Asked Questions</a> (FAQ) about Research?</p>

<hr class="postfooter">

<h3>Previous MSc. Dissertations</h3>

Role: <b>supervisor</b>
<ol reversed>
 <li>Rohith K. Sasidharan. <b><i>Evaluating the effectiveness of NLP-based techniques for Named Entity Recognition in Cyber Threat Intelligence</i></b>, Aston University, 24/01/2025.</li>
<hr>
 <li>Deekshitha Reddy Banuri. <b><i>Using AI/ML to filter out internet chatter for outlining impending cyber attacks</i></b>, Aston University, 24/10/2024.</li>
 <li>Kevin Ntim. <b><i>Leveraging AI/ML and social media for Cyber Threat Detection</i></b>, Aston University, 24/10/2024.</li>
 <li>Prathmesh Thakare. <b><i>Anomaly Detection in Wi-Fi Networks for SMEs Using Generative Adversarial Networks</i></b>, Aston University, 10/10/2024.</li>
<hr>
 <li>Bhavya Botta. <b><i>Model extraction and automation using Advanced ML/DL Models on the HIKARI-2021 Dataset</i></b>, Aston University, 29/12/2023.</li>
 <li>Vijay Ram Dayalan Raguram. <b><i>AI-Driven Fraud Detection and Prevention in e-commerce businesses</i></b>, Aston University, 09/10/2023.</li>
<hr>
 <li><font color="#110011">Amanda Brum de Carvalho. <i>Knowledge retention in computational environment to promote creativity and innovation</i>, 2018.</font></li>
 <li><font color="#110011">Júlio Eduardo Forster. <i>Proposal of monitoring system for low capacity airplanes</i>, 2017.</font></li>
 <li><font color="#110011">Eduardo Fernando Watte. <i>Global competitivity rate for hospitalar care</i>, 2017.</font></li>
 <li><font color="#110011">Daniel Assmann. <i>Risk map recognition system using computer graphics</i>, 2016.</font></li>
</ol>


<?php
include("postfooter.php");
?>

            </div> <!-- div main -->

          </div> <!-- div columns -->

        </div> <!-- div wrapper -->

<?php
include("footer.php");
?>

</div>
</body>
</html>