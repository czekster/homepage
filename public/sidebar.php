<?php
// bread crumb
// header: type, script, text, hidden (0|1)

$arr = array("item;index.php;Home;1",
             "subitem;tips-for-better-writing.php;Writing tips;1",
             "subitem;personal-tutee-faq.php;PT-FAQ;1",
             "item;about.php;About;1",
             "item;research.php;Research;1",
             "subitem;research-faq.php;FAQ;1",
             "subitem;research-highlights.php;Highlights;1",
             "item;FYP.php;FYP;1",
             "subitem;FYP-info.php;Info;1",
             "subitem;FYP-projects.php;Projects;1",
             "subitem;FYP-faq.php;FYP-FAQ;1",
             "subitem;FYP-info.php#FYP-kit;Kit;1",
             "subitem;FYP-timeline.php;Timeline;1",
             "item;MSC.php;MSc.;1",
             "subitem;MSC-projects.php;Projects;1",
             "item;programming.php;Programming;1",
             "subitem;programming-useful.php;Useful;1",
             "subitem;code-snippets.php;Snippets;1",
             "item;interests.php;Interests;1",
             "item;projects.php;Projects;1",
             "item;reading.php;Reading list;1",
             "item;cybersecurity.php;Cybersecurity;0",
             "item;rants.php;Rants;1",
             "item;contact.php;Contact;1"
      );

$serverscript = basename($_SERVER['PHP_SELF']);
?>
	  <div class="sidebar">
		<header>Ricardo M. Czekster</header>
		<div class="menu">
<?php
for ($i=0; $i<sizeof($arr); $i++) {
   $item = $arr[$i];
   list($type,$script,$text,$show) = explode(';', $item);
   if ($show) {
?>
		  <div class="menu-<?php echo($type);?><?php echo($serverscript == $script ? "-active" : "");?>">
         <?php echo($type == "subitem" ? "&rarr;&nbsp;" : "");?>
			<a href="<?php echo($script); ?>"><?php echo($text); ?></a>
		  </div>
<?php
   }
   if ($i == 19) { echo("<br><br><hr size=\"1\" width=\"75%\" align=\"left\">"); }
}
?>
		</div>
	  </div>