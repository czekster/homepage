<html>
  <head>
    <meta charset="utf-8">
    <meta name="generator" content="GitLab Pages">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Ricardo M. Czekster's homepage</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="syntax.css">
	<link rel="stylesheet" href="main.css">
    <link rel="stylesheet" href="style.css">
    <script src="https://platform.linkedin.com/badges/js/profile.js" async defer type="text/javascript"></script>
  </head>

<body class="vsc-initialized">
  <div class="wrapper">
	<div class="columns">
<?php
include("sidebar.php");
?>
	  <div class="main">
		<h1>Home</h1>
<img align="right" width="200" src="images/rmc.png">
<p>Welcome! I have been working for the past years on the general topic of systems modelling and quantitative issues in Cyber-Physical Systems (CPS).</p>
<p>My focus is on tackling dependability, i.e., availability, reliability, maintenance, performance, safety and security.</p>
<p>
I am currently working at <a href="https://www.aston.ac.uk/" target="_blank">Aston University</a>&#x2197; as a Lecturer in Software Engineering & Cybersecurity within the School of Computer Science and Digital Technologies.<br>
</p>
<p>
I chiefly investigate dependability + cyber security, modelling and simulation, and Cyber Threat Intelligence (CTI) in <i>smart</i> and <i>critical</i> infrastructure.<br>
</p>

<table border="0" cellpadding="10" width="100%">
 <tr><td colspan="2" align="left"><b><font size="-1">Quote:</font></td></tr>
 <tr>
  <td width="5%" bgcolor="#AABBCC">&nbsp;</td>
  <td width="90%" style = "border: 2px solid gray;" bgcolor="#EEEEEE"><p>
"If you look for an excuse, you'll always find one."<br>
- Anthony Liccione
</p>
</td>
  <td width="5%" bgcolor="#AABBCC">&nbsp;</td>
 </tr>
</table>

<br><hr>

<p>Check my <a href="https://research.aston.ac.uk/en/persons/ricardo-melo-czekster" target="_blank">Pure profile at Aston</a>&#x2197; for my latest research outcomes.</p>

<p>Look the <a href="research-faq.php">Frequently Asked Questions</a> (FAQ) about <b>Research</b>.</p>

<p>Read some <a href="tips-for-better-writing.php">tips for writing better</a>.</p>

<table border="0" cellpadding="10">
 <tr>
  <td width="1%" bgcolor="#eeffee">&nbsp;</td>
  <td style = "border: 1px solid green;" bgcolor="#eeeeff"><p>
One question I get a lot is <i>"How to start working with cyber security right away?"</i>.<br />
So, to answer this the best I can, please check out this <a href="https://www.sans.org/blog/unlock-your-cybersecurity-potential-a-look-at-whats-new-in-the-updated-new-to-cyber-field-manual/" target="_blank"><i>"New to Cyber Field Manual: The Ultimate Guide to Getting Into Cyber security"</i></a>&#x2197; or <a href="https://tryhackme.com/" target="_blank">TryHackMe</a>&#x2197;, a free online platform or learning cyber security.
</p>
<p>You may also find quite interesting NCSC's (UK) page on <a href="https://www.ncsc.gov.uk/section/advice-guidance/all-topics" target="_blank">cybersecurity topics</a>.</p>
</td>
 </tr>
</table>


<h2 id="research">Research</h2>
<p>This is what currently interests me in research:</p>
<img src="images/research-2023.png" width="600">
<br/><br/>

<a name="past-engagements"></a>
<h2>Engagements</h2>
<ul>
 <li>Book reviewer and work on conferences and workshops</li>
 <ul>
  <li><b><i>General Co-Chair</i></b> (with Dr Paolo Milazzo, University of Pisa, Italy) in <a href="https://datamod-symposium.github.io/DataMod-2024/" target="_blank">12<sup>th</sup> International Symposium DataMod 2024: From Data to Models and Back</a>&#x2197; - Joint event with <a href="https://sefm-conference.github.io/2024/" target="_blank">SEFM 2024</a>&#x2197;</li>
  <li><b><i>Publicity Chair</i></b>, <a href="https://www.thec3ai.com/home" target="_blank">1<sup>st</sup> International Conference on Computing, Communication, Cybersecurity & AI</a>&#x2197; (The C3AI 2023)</li>
  <li><b><i>PC Member</i></b>, <a href="https://www.wosar.net/" target="_blank">15<sup>th</sup> International Workshop on Software Aging and Rejuvenation 2023</a> - Joint event with <a href="https://issre.github.io/2023/" target="_blank">ISSRE 2023</a>&#x2197;</li>
  <li><b><i>PC Member</i></b>, <a href="https://datamod-symposium.github.io/DataMod-2023/" target="_blank">11<sup>th</sup> International Symposium DataMod 2023: From Data to Models and Back</a>&#x2197; - Joint event with <a href="https://sefm-conference.github.io/2023/" target="_blank">SEFM 2023</a>&#x2197;</li>
  <li><b><i>Technical reviewer</i></b>, <i><a href="https://nostarch.com/math-security" target="_blank"><img src="images/book-math-sec.png" align="right">Math for Security</a>&#x2197;: From Graphs and Geometry to Spatial Analysis</i>, by Daniel Reilly (NoStarch press - September 2023)</li>
  <li><b><i>Technical Program Committee</i></b>, <a href="https://www.uk-ci.org/" target="_blank">UKCI'2023</a>&#x2197;, The 22<sup>nd</sup> UK Workshop on Computational Intelligence</li>
  <li><b><i>Co-Program Chair</i></b>, <a href="https://sites.google.com/view/wosar2022" target="_blank">WoSAR'2022</a>&#x2197;, The 14<sup>th</sup> Workshop on Software Aging and Rejuvenation</li>
  <li><b><i>Program Committee</i></b>, WPerformance within SBC - Brazilian Computing Society, 2013-2019</li>
  <li><b><i>Finance Chair</i></b>, <a href="https://icgse2012.serandp.com/" target="_blank">ICGSE'2012</a>&#x2197;, The 7<sup>th</sup> IEEE International Conference on Global Software Engineering</li>
 </ul>
 <li>Journals</li>
 <ul>
  <li><b><i>Associate editor</i></b>, <a href="https://link.springer.com/journal/42044/editorial-board" target="_blank">Iran Journal of Computer Science</a>&#x2197; (Springer), since 2017</li>
  <li><b><i>Editorial board member</i></b>, <a href="https://www.inderscience.com/jhome.php?jcode=ijcat" target="_blank">International Journal of Computer Applications in Technology</a>&#x2197; (IJCAT), 2015-2019
 </ul>
 <li>Reviewer (check also my <a href="https://www.webofscience.com/wos/author/record/D-2854-2009" target="_blank">Web of Science</a>&#x2197; or <a href="https://orcid.org/0000-0002-6636-4398" target="_blank">ORCID</a>&#x2197; profile under "Peer Review" section)</li>
 <ul>
  <li><a href="https://www.computer.org/csdl/magazine/so" target="_blank">IEEE Software</a>&#x2197;</li>
  <li><a href="https://www.emeraldgrouppublishing.com/journal/bpmj" target="_blank">Business Process Management Journal</a>&#x2197; (Emerald Publishing)</li>
  <li><a href="https://www.cwejournal.org/" target="_blank">Current World Environment</a>&#x2197;</li>
  <li><a href="https://www.tandfonline.com/toc/teis20/current" target="_blank">Enterprise Information Systems</a>&#x2197; (Taylor & Francis)</li>
  <li><a href="https://www.sciencedirect.com/journal/information-processing-and-management" target="_blank">Information Processing & Management</a>&#x2197; (Elsevier)</li>
 </ul>
</ul>
<br/>

<h2>Professional memberships</h2>
<table border="0">
 <tr>
  <td valign="center" align="center" bgcolor="#eeffee"><a href="https://member.acm.org/~czekster" target="_blank"><img align="left" src="images/acm.png"></a></td>
  <td style = "border: 1px solid green;" bgcolor="#eeeeff" valign="top"><p>I'm a member of <a href="https://www.acm.org/" target="_blank">Association for Computing Machinery (ACM)</a>&#x2197;. Please find <a href="https://member.acm.org/~czekster" target="_blank">my profile information here</a>&#x2197;.</p>
  </td>
 </tr>
</table>
<br/>
<table border="0">
 <tr>
  <td style = "border: 1px solid green;" bgcolor="#eeeeff" valign="top" align="right">
   <p>I am also an active member of <a href="https://owasp.org/" target="_blank">The Open Worldwide Application Security Project (OWASP)</a>&#x2197;.</p>
  </td>
  <td valign="center" bgcolor="#eeffee"><a href="https://owasp.org/" target="_blank"><img align="left" src="images/owasp-logo.png"></a></td>
 </tr>
</table>
<br/>

<?php
include("postfooter.php");
?>

            </div> <!-- div main -->

          </div> <!-- div columns -->

        </div> <!-- div wrapper -->

<?php
include("footer.php");
?>

</div>
</body>
</html>