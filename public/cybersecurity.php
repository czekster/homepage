<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="generator" content="GitLab Pages">
    <meta name="keywords" content="cyber security quotes"/>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Ricardo M. Czekster's homepage</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="syntax.css">
	<link rel="stylesheet" href="main.css">
    <link rel="stylesheet" href="style.css">
    <script src="https://platform.linkedin.com/badges/js/profile.js" async defer type="text/javascript"></script>
  </head>

<body class="vsc-initialized">
  <div class="wrapper">
	<div class="columns">
<?php
include("sidebar.php");
?>
	  <div class="main">
		<h1>Cyber security</h1>
<img align="right" width="300" src="images/hacked.png">
<p>Cyber security is about catering for CIA (Confidentiality, Integrity, Availability) plus Authenticity, Non-Repudiation, and Privacy.</p>

<p>Some <i>quotes</i> on cyber security worth mentioning:</p>
<img src="images/sec-quotes.png" width="120%">
<br/><br/>
<p>Modern digitisation and on-line presence mandates citizens to learn to protect themselves against malicious threat actors. </p>

<?php
include("postfooter.php");
?>

            </div> <!-- div main -->

          </div> <!-- div columns -->

        </div> <!-- div wrapper -->

<?php
include("footer.php");
?>

</div>
</body>
</html>